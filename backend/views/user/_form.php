<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary ">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="fas fa-user-plus"></i>  <?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close text-white" onclick='closeForm("userForm")'>×</button>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'userForm']]); ?>
            <div class="user-form card-body">
                <?= $form->field($model,'firstName',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput(['readonly'=>true,'maxlength' => true])->label('Nombre(s)') ?>
                <?= $form->field($model,'lastName',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput(['readonly'=>true,'maxlength' => true])->label('Apellidos') ?>
                <div class="clearfix"></div>
                <?= $form->field($model,'email',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput(['readonly'=>true,'maxlength' => true]) ?>
                <div class="clearfix"></div>
                <?= $form->field($model, 'cliente_id',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput(['readonly'=>true])?>
                <?= $form->field($model, 'empresa_id',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput(['readonly'=>true])?>
                <div class="clearfix"></div>
                <?= $form->field($model,'username',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true,'autocomplete'=>'off']) ?>
                <?= $form->field($model,'password',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->passwordInput(['autocomplete'=>'off']) ?>
                <div class="clearfix"></div>
            </div>
            <div class=" card-footer" align="right">
                <?= Html::input('hidden', $name = 'skuPaquete', $value = $paqueteSku, ['option' => 'value']); ?>
                <?= Html::Button('Cancelar', ['class' => 'btn btn-default','id'=>'btnCloseForm','onClick'=>'closeForm("userForm")']) ?>
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
            </div>
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>
