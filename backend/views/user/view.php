<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->firstName." ".$model->lastName;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="fas fa-user-check"></i> <?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close cancelView text-white">×</button>
            </div>
            <div class="user-view card-body">
                 <div class="col-12" align="right">
                    <!--<?//= Html::a('<i class="fas fa-edit"></i>',$url=Url::to('index.php?r=user/update&id='.$model->id),['class' => 'btn btn-success btnUpdateView','title'=>'Editar Registro']); ?>-->
                
                    <?= Html::a('<i class="fas fa-trash-alt"></i>', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'title' => 'Eliminar',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
                <div class="col-12 pt-3">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'firstName',
                            'lastName',
                            'username',
                            'auth_key',
                            'password_hash',
                            //'password_reset_token',
                            'email:email',
                            'status',
                            //'created_at',
                            //'updated_at',
                            //'verification_token',
                            'cliente_id',
                            'empresa_id',
                        ],
                    ]) ?>
                </div>
            </div>
            <div align="center">
                <div class="card-footer">
                    <?= Html::button('Aceptar',['value'=>'','class'=>'btn btn-primary cancelView']) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /*** Action Button Edit View ***/
    $(".btnUpdateView").on("click",function(e){
        $("#divEditForm").hide(function(e){});
        $("#btnAddForm").show(function(e){});
        $("#divEditForm").load($(this).attr('value'),function(e){
            $("#divEditForm").slideDown('slow');
        });
    });

    /*** Action Button Cancel-Close View ***/
    $(".cancelView").on("click",function(e){
         $("#divEditForm").slideUp(function(e){});
    });
</script>
