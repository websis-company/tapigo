<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Modulos */

$this->title = 'Agregar Módulo';
$this->params['breadcrumbs'][] = ['label' => 'Modulos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modulos-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
