<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Contratos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contratos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cliente_id')->textInput() ?>

    <?= $form->field($model, 'paquete_id')->textInput() ?>

    <?= $form->field($model, 'fechaContrato')->textInput() ?>

    <?= $form->field($model, 'fechaRenovacion')->textInput() ?>

    <?= $form->field($model, 'fechaCancelacion')->textInput() ?>

    <?= $form->field($model, 'tipoContrato')->dropDownList([ 'Paquete' => 'Paquete', 'Modulos' => 'Modulos', 'Impresos' => 'Impresos', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'renovado')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'estatus')->dropDownList([ 'Activo' => 'Activo', 'Inactivo' => 'Inactivo', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
