<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Contratos */

$this->title = 'Update Contratos: ' . $model->contrato_id;
$this->params['breadcrumbs'][] = ['label' => 'Contratos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->contrato_id, 'url' => ['view', 'id' => $model->contrato_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contratos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
