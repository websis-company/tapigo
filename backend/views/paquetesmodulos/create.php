<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Paquetesmodulos */

$this->title = 'Create Paquetesmodulos';
$this->params['breadcrumbs'][] = ['label' => 'Paquetesmodulos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquetesmodulos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
