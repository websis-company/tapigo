<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Paquetesmodulos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paquetesmodulos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'modulo_id')->textInput() ?>

    <?= $form->field($model, 'paquete_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
