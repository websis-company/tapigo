<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PaquetesmodulosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Paquetesmodulos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquetesmodulos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Paquetesmodulos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'paqueteModulo_id',
            'modulo_id',
            'paquete_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
