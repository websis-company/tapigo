<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Paquetesmodulos */

$this->title = 'Update Paquetesmodulos: ' . $model->paqueteModulo_id;
$this->params['breadcrumbs'][] = ['label' => 'Paquetesmodulos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->paqueteModulo_id, 'url' => ['view', 'id' => $model->paqueteModulo_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paquetesmodulos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
