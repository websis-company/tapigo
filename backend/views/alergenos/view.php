<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Alergenos */

$this->title = $this->title = 'Editar Alérgenos: '.$model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alergenos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-skull-crossbones"></i> <?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close cancelView text-white">×</button>
            </div>
            <div class="alergenos-view card-body">
                <div class="col-12" align="right">
                    <?= Html::button('<i class="fas fa-edit"></i>',['value'=>Url::to(['update','id'=>$model->alergeno_id]),'class' => 'btn btn-success btnUpdateView','title'=>'Editar Registro']); ?>

                    <?= Html::a('<i class="fas fa-trash-alt"></i>', ['delete', 'id' => $model->alergeno_id], [
                        'class' => 'btn btn-danger',
                        'title' => 'Eliminar',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
                <div class="col-12 pt-3">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'alergeno_id',
                            'nombre',
                            'descripcion:ntext',
                            //'logo',
                            [
                                'attribute' => 'Logo',
                                'format'    => 'html',
                                'headerOptions' =>['style'=>'text-align:center'],
                                'value' => function($model){
                                    return Html::img(Url::base()."/".$model->logo, ['class' => 'img-thumbnail','width'=>'100px']);
                                }
                            ],
                            'estatus',
                        ],
                    ]) ?>
                </div>
            </div>
            <div align="center">
                <div class="card-footer">
                    <?= Html::button('Aceptar',['value'=>'','class'=>'btn btn-primary cancelView']) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /*** Action Button Edit View ***/
    $(".btnUpdateView").on("click",function(e){
        $("#divEditForm").hide(function(e){});
        $("#btnAddForm").show(function(e){});
        $("#divEditForm").load($(this).attr('value'),function(e){
            $("#divEditForm").slideDown('slow');
        });
    });

    /*** Action Button Cancel-Close View ***/
    $(".cancelView").on("click",function(e){
         $("#divEditForm").slideUp(function(e){});
    });
</script>
