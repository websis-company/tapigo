<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Alergenos */

$this->title = Yii::t('app', 'Update Alergenos {name}',['name'=>$model->nombre]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alergenos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->alergeno_id, 'url' => ['view', 'id' => $model->alergeno_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="alergenos-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
