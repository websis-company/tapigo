<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AlergenosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alérgenos';
$this->params['breadcrumbs'][] = $this->title;
echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox',
    'config' => [
        'image' => [      
            // Wait for images to load before displaying
            // Requires predefined image dimensions
            // If 'auto' - will zoom in thumbnail if 'width' and 'height' attributes are found
            'preload'      => "auto",
        ],
        'clickSlide'      => false,
        'clickOutside'    => false,
        'dblclickContent' => false,
        'dblclickSlide'   => false,
        'dblclickOutside' => false,


        //'dblclickOutside' => 'false',
    ]
]);
?>

<div class="container-fluid">
    <div id="divEditForm" class="col-sm-12 col-md-12 col-lg-6 offset-lg-3" style="display: none;"></div>
</div>

<div class="alergenos-index">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-secondary card-outline">
                    <div class="card-header">
                        <div class="col-6 float-right pb-3">
                            <?php echo Html::button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Agregar Alérgeno', ['value'=>Url::to('create'),'class' => 'btn bg-gradient-primary float-right','id'=>'btnAddForm']);
                            ?>
                        </div>
                    </div>
                    <div class="card-body pad table-responsive">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                //'alergeno_id',
                                'nombre',
                                //'descripcion:ntext',
                                [
                                    'label'     =>'Logo',
                                    'attribute' =>'logo',
                                    'format'    =>'html',
                                    'value'     =>function($model){
                                        return Html::a(Html::img(Url::base()."/".$model->logo,['width'=>'45']), $url = Url::base()."/".$model->logo, ['class' => 'data-fancybox']);
                                    }
                                ],
                                'estatus',

                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'header'=> 'Actions',
                                    'headerOptions'=>['style'=>'text-align:center'],
                                    'template'=>'{view} {update} {delete}',
                                    'buttons'=>[
                                        'view'=>function($url,$model){
                                            return Html::button('<span class="glyphicon glyphicon-search"></span>',['value'=>Url::to(['view','id'=>$model->alergeno_id]),'class' => 'btn btnViewForm']);
                                        },
                                        'update'=>function ($url, $model) {
                                            return Html::button('<span class="glyphicon glyphicon-pencil"></span>',['value'=>Url::to(['update','id'=>$model->alergeno_id]),'class' => 'btn btnUpdateForm']);
                                        },
                                    ]

                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
