<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Alergenos */

$this->title = 'Agregar Alérgeno';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alergenos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alergenos-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
