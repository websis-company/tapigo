<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Alergenos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-skull-crossbones"></i> <?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close text-white" onclick='closeForm("alergenosForm")'>×</button>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'alergenosForm']]); ?>
			<div class="alergenos-form card-body">
			    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
			    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>
			    <!-- <?//= $form->field($model, 'logo')->textInput(['maxlength' => true]) ?> -->
                <?= $form->field($model, 'file')->fileInput(['class'=>'form-control']) ?>
			    <?= $form->field($model, 'estatus')->dropDownList([ 'Activo' => 'Activo', 'Inactivo' => 'Inactivo', ], ['prompt' => '']) ?>
			</div>
			<div class=" card-footer" align="right">
                <?= Html::Button('Cancelar', ['class' => 'btn btn-default','id'=>'btnCloseForm','onClick'=>'closeForm("alergenosForm")']) ?>
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
            </div>
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>
