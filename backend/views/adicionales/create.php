<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Adicionales */

$this->title = 'Create Adicionales';
$this->params['breadcrumbs'][] = ['label' => 'Adicionales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adicionales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
