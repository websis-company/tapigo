<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Adicionales */

$this->title = $model->adicional_id;
$this->params['breadcrumbs'][] = ['label' => 'Adicionales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="adicionales-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->adicional_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->adicional_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'adicional_id',
            'contrato_id',
            'modulo_id',
            'impreso_id',
            'fechaContrato',
            'fechaRenovacion',
            'fechaCancelacion',
            'renovado',
            'cantidad',
            'estatus',
        ],
    ]) ?>

</div>
