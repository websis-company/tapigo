<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\AdicionalesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adicionales-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'adicional_id') ?>

    <?= $form->field($model, 'contrato_id') ?>

    <?= $form->field($model, 'modulo_id') ?>

    <?= $form->field($model, 'impreso_id') ?>

    <?= $form->field($model, 'fechaContrato') ?>

    <?php // echo $form->field($model, 'fechaRenovacion') ?>

    <?php // echo $form->field($model, 'fechaCancelacion') ?>

    <?php // echo $form->field($model, 'renovado') ?>

    <?php // echo $form->field($model, 'cantidad') ?>

    <?php // echo $form->field($model, 'estatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
