<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Adicionales */

$this->title = 'Update Adicionales: ' . $model->adicional_id;
$this->params['breadcrumbs'][] = ['label' => 'Adicionales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->adicional_id, 'url' => ['view', 'id' => $model->adicional_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="adicionales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
