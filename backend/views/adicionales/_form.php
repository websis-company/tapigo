<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Adicionales */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adicionales-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contrato_id')->textInput() ?>

    <?= $form->field($model, 'modulo_id')->textInput() ?>

    <?= $form->field($model, 'impreso_id')->textInput() ?>

    <?= $form->field($model, 'fechaContrato')->textInput() ?>

    <?= $form->field($model, 'fechaRenovacion')->textInput() ?>

    <?= $form->field($model, 'fechaCancelacion')->textInput() ?>

    <?= $form->field($model, 'renovado')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'estatus')->dropDownList([ 'Activo' => 'Activo', 'Inactivo' => 'Inactivo', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
