<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Clientes */

$this->title = 'Agregar Cliente';
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientes-create">
    <?= $this->render('_form', [
		'model'              => $model,
		'modelEmpresa'       => $modelEmpresa,
		'modulosAdicionales' => $modulosAdicionales,
		'modelAdicionales'   => $modelAdicionales,
    ]) ?>

</div>
