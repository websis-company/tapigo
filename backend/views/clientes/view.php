<?php
use backend\models\Adicionales;

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Clientes */

$this->title = "Cliente :".$model->nombres." ".$model->apellidoPaterno." ".$model->apellidoMaterno;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-users"></i> <?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close cancelView text-white">×</button>
            </div>
            <div class="clientes-view card-body">
                <div class="col-12" align="right">
                    <?= Html::button('<i class="fas fa-edit"></i>',['value'=>Url::to(['update','id'=>$model->cliente_id]),'class' => 'btn btn-success btnUpdateView','title'=>'Editar Registro']); ?>

                    <?= Html::a('<i class="fas fa-trash-alt"></i>', ['delete', 'id' => $model->cliente_id], [
                        'class' => 'btn btn-danger',
                        'title' => 'Eliminar',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
                <div class="col-12 pt-3">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'cliente_id',
                            [
                               'attribute' => 'Nombre Completo',
                               'format'    => 'text',
                               'value'     => function($model){
                                    return $model->nombres." ".$model->apellidoPaterno." ".$model->apellidoMaterno;
                               },
                            ],
                            'email:email',
                            'telefono',
                            'movil',
                            [
                               'attribute' => 'Empresa',
                               'format'    => 'text',
                               'value'     => function($model){
                                    $empresas = $model->empresas;
                                    foreach ($empresas as $empresa) {
                                        return $empresa->nombre;
                                    }//end foreach
                               },
                            ],
                            [
                               'attribute' => 'Paquete contratado',
                               'format'    => 'text',
                               'value'     => function($model){
                                    $contratos = $model->contratos;
                                    foreach ($contratos as $contrato) {
                                        if(!is_null($contrato->paquete)){
                                            return $contrato->paquete->nombre;
                                        }
                                    }//end foreach
                               },
                            ],
                            [
                               'attribute' => 'Modulos Adicionales',
                               'format'    => 'text',
                               'value'     => function($model){
                                    $modulosAdicionales = Adicionales::find()
                                                            ->where(['contrato_id'=>$model->contratos[0]->contrato_id])
                                                            ->andWhere(['tipo'=>'Modulo'])
                                                            ->andWhere(['estatus'=>'Activo'])
                                                            ->all();
                                    $moduloStr = "";
                                    foreach ($modulosAdicionales as $moduloAdicional) {
                                        $moduloStr .= $moduloAdicional->modulo->nombre.", ";
                                    }
                                    return $moduloStr;
                               },
                            ],
                            [
                               'attribute' => 'Impresos Adicionales',
                               'format'    => 'text',
                               'value'     => function($model){
                                    $impresosAdicionales = Adicionales::find()
                                                                ->where(['contrato_id'=>$model->contratos[0]->contrato_id])
                                                                ->andWhere(['tipo'=>'Impreso'])
                                                                ->andWhere(['estatus'=>'Activo'])
                                                                ->all();
                                    $impresoStr = "";
                                    foreach ($impresosAdicionales as $impresoAdicional) {
                                        if(!is_null($impresoAdicional->cantidad)){
                                            $impresoStr .= "(".$impresoAdicional->cantidad.") ".$impresoAdicional->impreso->nombre.", ";
                                        }
                                    }
                                    return $impresoStr;

                               },
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
            <div align="center">
                <div class="card-footer">
                    <?= Html::button('Aceptar',['value'=>'','class'=>'btn btn-primary cancelView']) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /*** Action Button Edit View ***/
    $(".btnUpdateView").on("click",function(e){
        $("#divEditForm").hide(function(e){});
        $("#btnAddForm").show(function(e){});
        $("#divEditForm").load($(this).attr('value'),function(e){
            $("#divEditForm").slideDown('slow');
        });
    });

    /*** Action Button Cancel-Close View ***/
    $(".cancelView").on("click",function(e){
         $("#divEditForm").slideUp(function(e){});
    });
</script>
