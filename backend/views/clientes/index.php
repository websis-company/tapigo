<?php
use backend\models\Empresa;
use backend\models\User;

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ClientesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div id="divEditForm" class="col-sm-12 col-md-12 col-lg-6 offset-lg-3" style="display: none;"></div>
</div>

<div class="clientes-index">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-secondary card-outline">
                    <div class="card-header">
                        <div class="col-6 float-right pb-3">
                            <?php echo Html::button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Agregar Cliente', ['value'=>Url::to('create'),'class' => 'btn bg-gradient-primary float-right','id'=>'btnAddForm']);
                            ?>
                        </div>
                    </div>
                    <div class="card-body pad table-responsive">
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                //'cliente_id',
                                'nombres',
                                'apellidoPaterno',
                                'apellidoMaterno',
                                'email:email',
                                'telefono',
                                'movil',
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'header'=> 'Actions',
                                    'headerOptions'=>['style'=>'text-align:center'],
                                    //'template'=>'{empresa} {contratos} {view} {update} {delete}',
                                    'template'=>'{usuario} {view} {update} {delete}',
                                    'buttons'=>[
                                        'view'=>function($url,$model){
                                            return Html::button('<span class="glyphicon glyphicon-search"></span>',['value'=>Url::to(['view',"id"=>$model->cliente_id]),'title'=>'Ver Cliente','class' => 'btn btnViewForm']);
                                        },
                                        'usuario'=>function($url,$model){
                                            $modelUser = User::find()->where(['cliente_id'=>$model->cliente_id])->andWhere(['status'=>10])->orderBy(['id'=>SORT_DESC])->one();

                                            if(is_null($modelUser)){
                                                $modelEmpresa =  Empresa::find()
                                                                ->where(['cliente_id'=>$model->cliente_id])
                                                                ->andWhere(['estatus'=>'Activo'])
                                                                ->orderBy(['empresa_id'=>SORT_DESC])
                                                                ->one();
                                                if(!empty($modelEmpresa)){
                                                    return Html::a('<i class="fas fa-user-plus"></i>',$url = Url::to(['user/create','cliente_id'=>$model->cliente_id,'empresa_id'=>$modelEmpresa->empresa_id]),['title'=>'Asignar Usuario','class' => 'btn text-warning btnViewForm']);
                                                }//end if
                                            }else{
                                                return Html::button('<i class="fas fa-user-check"></i>',['value'=>Url::to(['user/view','id'=>$modelUser->id]),'title'=>'Ver Cliente','class' => 'btn text-teal btnViewForm']);
                                            }//end if
                                        },
                                        /*'contratos'=>function($url,$model){
                                            $contratos_url = 'index.php?r=clientes/contratos&id='.$model->cliente_id;
                                            return Html::a('<i class="fas fa-file-invoice-dollar"></i>',$url = Url::to($contratos_url),['title'=>'Ver Contratos','class' => 'btn btnContrato']);  
                                        },*/
                                        'update'=>function ($url, $model) {
                                            return Html::button('<span class="glyphicon glyphicon-pencil"></span>',['value'=>Url::to(['update','id'=>$model->cliente_id]),'title'=>'Editar Cliente','class' => 'btn btnUpdateForm']);
                                        },
                                    ]
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
