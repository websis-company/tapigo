<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Clientes */

$this->title = 'Editar Cliente: ' . $model->nombres." ".$model->apellidoPaterno." ".$model->apellidoMaterno;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cliente_id, 'url' => ['view', 'id' => $model->cliente_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="clientes-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
		'model'              => $model,
		'modelEmpresa'       => $modelEmpresa,
		'modulosAdicionales' => $modulosAdicionales,
		'modelAdicionales'   => $modelAdicionales,
    ]) ?>

</div>
