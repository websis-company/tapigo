<?php
use backend\models\Paquetes;
use backend\models\Paquetesmodulos;
use backend\models\Modulos;
use backend\models\Impresos;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use wbraganca\dynamicform\DynamicFormWidget; /*** Dymanic Form (@view Componser.json for implementation)***/

/* @var $this yii\web\View */
/* @var $model backend\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-users"></i> <?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close cancelView text-white" onClick='closeForm("clientesForm")'>×</button>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>"clientesForm"]]); ?>
            <div class="clientes-form card-body">
                <!-- Datos Cliente -->
                <div class="row mt-2">
                    <div class="col-12">
                        <div class="card border border-dark">
                            <div class="card-header bg-dark text-white ">
                                <h5><strong><i class="nav-icon fas fa-fas fa-users"></i> Información del cliente</strong></h5>
                            </div>
                            <div class="card-body">
                                <?= $form->field($model, 'nombres',['options'=>['class'=>'col-sm-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true,'autocomplete'=>'off']) ?>
                                <?= $form->field($model, 'apellidoPaterno',['options'=>['class'=>'col-sm-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true,'autocomplete'=>'off']) ?>
                                <div class="clearfix"></div>
                                <?= $form->field($model, 'apellidoMaterno',['options'=>['class'=>'col-sm-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true,'autocomplete'=>'off']) ?>
                                <?= $form->field($model, 'email',['options'=>['class'=>'col-sm-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true,'autocomplete'=>'off']) ?>
                                <div class="clearfix"></div>
                                <?= $form->field($model, 'telefono',['options'=>['class'=>'col-sm-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true,'autocomplete'=>'off']) ?>
                                <?= $form->field($model, 'movil',['options'=>['class'=>'col-sm-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true,'autocomplete'=>'off']) ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Empresa Asignada -->
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="card border border-dark">
                            <div class="card-header bg-dark text-white ">
                                <h5><strong><i class="fas fa-store-alt"></i> Asignar Empresa <small>(Nombre del Restaurante,Bar,Pub, etc.)</small></strong></h5>
                            </div>
                            <div class="card-body">
                                <?= $form->field($modelEmpresa, 'nombre',['options'=>['class'=>'col-12','style'=>'float: left;']])->textInput(['maxlength' => true,'autocomplete'=>'off']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Section Paquetes -->
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="card border border-dark">
                            <div class="card-header bg-dark text-white ">
                                <h5><strong><i class="nav-icon fas fa-fad fa-cubes"></i> Paquete Contratado</strong></h5>
                            </div>
                            <div class="card-body">
                                <?php
                                // Normal select with ActiveForm & model [Select2]
                                echo $form->field($model, 'paqueteId',['options'=>['class'=>'col-12']])->label('<span class="text-primary">Seleccione un paquete</span>')->widget(Select2::classname(), [
                                    'data' =>  ArrayHelper::map(Paquetes::find()->all(),'paquete_id','nombre'),
                                    'language' => 'es',
                                    'theme' =>Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'value' => $model->paqueteId,//Initial values or selected values
                                        'placeholder' => 'Seleccione un paquete'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                    'pluginEvents' => [
                                        "select2:select" => "function() { 
                                            $('select#multipleModulos').val(null).trigger('change'); //Clean select2 asociate
                                            var id_    = $(this).val();
                                            var url_to = '".Url::to(['paquetesmodulos/list'])."';
                                            $.get(url_to,{\"id\":id_},function(data){
                                                $(\"select#multipleModulos\").html(data);
                                            });
                                        }",
                                        "select2:unselect" => "function() { 
                                            var id_ = $(this).val();
                                            var url_to = '".Url::to(['paquetesmodulos/list'])."';
                                            $.get(url_to,{\"id\":id_},function(data){
                                                $(\"select#multipleModulos\").html(data);
                                            });

                                        }",
                                    ]
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Section -->

                <div class="row mt-4">
                    <div class="col-12">
                        <div class="card border border-dark">
                            <div class="card-header bg-dark text-white ">
                                <h5><strong><i class="fas fa-fas fa-puzzle-piece"></i> Adicionales</strong></h5>
                            </div>
                            <div class="card-body">
                                <!-- Section Modulos -->
                                <?php
                                /*** Show "Modulos" that not include in "Paquete" selected Only for Edit register***/
                                if(!empty($model->paqueteId)){
                                    #-- Get "Modulos" related to register "Paquete" selected
                                    $modulosPaquetes = Paquetesmodulos::find()
                                                            ->with('modulo') //table @Modulos related
                                                            ->where(['paquete_id'=>$model->paqueteId])
                                                            ->all();
                                    #-- Get All registers in table "Modulos"
                                    $modelModulos = Modulos::find()->select('modulo_id,nombre')->all();

                                    $arrModulos = [];
                                    foreach ($modelModulos as $modulo) {
                                        $arrModulos[] = $modulo->modulo_id;
                                    }
                                    $arrModulosPaquetes = [];
                                    foreach ($modulosPaquetes as $moduloPaquetes) {
                                        $arrModulosPaquetes[] = $moduloPaquetes->modulo_id;
                                    }

                                    #-- Get Diff in arrays
                                    $resArray = array_diff($arrModulos, $arrModulosPaquetes);
                                    #-- Ceate array to show "Modulos" not includes in "Paquete"
                                    $data = [];
                                    foreach ($resArray as $res) {
                                        $modelModulos = Modulos::find()
                                                            ->select('modulo_id,nombre')
                                                            ->where(['modulo_id'=>$res])
                                                            ->one();
                                        $data[$modelModulos->modulo_id] = $modelModulos->nombre;
                                    }//end foreach
                                }else{
                                    $data = ArrayHelper::map(Modulos::find()->all(),'modulo_id','nombre');
                                }//end if

                                // Multi select with ActiveForm & model [Select2]
                                echo $form->field($model, 'moduloId',['options'=>['class'=>'col-12']])->label('<span class="text-primary">Seleccione uno o varios módulos</span>')->widget(Select2::classname(), [
                                    'data' => $data,
                                    'language' => 'es',
                                    //'theme' =>'classic',
                                    'options' => [
                                        'value' => $modulosAdicionales,//Initial values or selected values
                                        'placeholder' => 'Seleccione modulos',
                                        'id'=>'multipleModulos',
                                    ],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'allowClear' => true,
                                        'multiple' => true,
                                    ],
                                ]);
                                ?>
                                <div class="clearfix"></div>
                                <!-- End Section -->

                                <!-- Section Impresos -->
                                <div class="row-fluid mt-4">
                                    <label>
                                        <span class="text-primary">Agrege uno o más productos impresos</span>
                                    </label>
                                </div>
                                <?php DynamicFormWidget::begin([
                                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                    'widgetBody' => '.container-items', // required: css class selector
                                    'widgetItem' => '.item', // required: css class
                                    'limit' => 10, // the maximum times, an element can be cloned (default 999)
                                    'min' => 1, // 0 or 1 (default 1)
                                    'insertButton' => '.add-item', // css class
                                    'deleteButton' => '.remove-item', // css class
                                    'model' => $modelAdicionales[0],
                                    'formId' => 'clientesForm',
                                    'formFields' => [
                                        'impreso_id',
                                        'cantidad',
                                    ],
                                ]); ?>
                                <div class="container-items"><!-- widgetContainer -->
                                    <?php foreach ($modelAdicionales as $i => $modelAdicionales): ?>
                                        <div class="item card"><!-- widgetBody -->
                                            <div class="card-header">
                                                <div class="float-right">
                                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="fas fa-plus-square"></i></button>
                                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="fas fa-minus-square"></i></button>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12">
                                                        <?= $form->field($modelAdicionales, "[{$i}]impreso_id")->label('Impresos')->dropDownList(
                                                            ArrayHelper::map(Impresos::find()->all(),'impreso_id','nombre'),
                                                            ['prompt'=>'Seleccione una opción']
                                                        ); ?>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <?= $form->field($modelAdicionales, "[{$i}]cantidad")->textInput(['maxlength' => true]) ?>
                                                    </div>
                                                </div><!-- .row -->
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div> 
                                <?php DynamicFormWidget::end(); ?>
                                <!-- End Section -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=" card-footer" align="right">
                <?= Html::Button('Cancelar', ['class' => 'btn btn-default','id'=>'btnCloseForm','onClick'=>'closeForm("clientesForm")']) ?>
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
            </div>
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>
