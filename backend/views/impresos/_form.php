<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Impresos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary ">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fa fa-print"></i>  <?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close text-white" onclick='closeForm("impresosForm")'>×</button>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'impresosForm']]); ?>
            <div class="impresos-form card-body">
                <?= $form->field($model, 'sku',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'nombre',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                <div class="clearfix"></div>
                <?= $form->field($model, 'descripcion',['options'=>['class'=>'col-12']])->textarea(['rows' => 6]) ?>
                <div class="clearfix"></div>
                <?= $form->field($model, 'precio',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput() ?>
                <?= $form->field($model, 'estatus',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->dropDownList([ 'Activo' => 'Activo', 'Inactivo' => 'Inactivo', ], ['prompt' => 'Seleccione una opción']) ?>
            </div>
            <div class=" card-footer" align="right">
                <?= Html::Button('Cancelar', ['class' => 'btn btn-default','id'=>'btnCloseForm','onClick'=>'closeForm("impresosForm")']) ?>
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
            </div>
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>
