<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Impresos */

$this->title = 'Agregar Impreso';
$this->params['breadcrumbs'][] = ['label' => 'Impresos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="impresos-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
