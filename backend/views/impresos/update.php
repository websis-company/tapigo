<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Impresos */

$this->title = 'Editar: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Impresos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->impreso_id, 'url' => ['view', 'id' => $model->impreso_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="impresos-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
