<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IdiomasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Idiomas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div id="divEditForm" class="col-sm-12 col-md-12 col-lg-6 offset-lg-3" style="display: none;"></div>
</div>

<div class="idiomas-index">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-secondary card-outline">
                    <div class="card-header">
                         <div class="col-6 float-right pb-3">
                            <?php echo Html::button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Agregar Idioma', ['value'=>Url::to('create'),'class' => 'btn bg-gradient-primary float-right','id'=>'btnAddForm']);
                            ?>
                            <!-- <?//= Html::a('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Agregar Idioma', ['create'], ['class' => 'btn bg-gradient-primary float-right']) ?> -->
                        </div>
                    </div>
                    <div class="card-body pad table-responsive">
                        <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'options' => ['class'=>''],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'idioma_id',
                            [
                                'label'     =>'Logo',
                                'attribute' =>'bandera',
                                'format'    =>'html',
                                'value'     =>function($model){
                                    return yii\bootstrap4\Html::img(Url::base()."/".$model->bandera,['width'=>'32']);
                                }

                            ],
                            'nombre',
                            'estatus',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'header'=> 'Actions',
                                'headerOptions'=>['style'=>'text-align:center'],
                                'template'=>'{update} {delete}',
                                'buttons'=>[
                                    'update'=>function ($url, $model) {
                                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>',['value'=>Url::to(['update','id'=>$model->idioma_id]),'class' => 'btn btnUpdateForm']);
                                    },
                                ]
                            ],
                        ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
