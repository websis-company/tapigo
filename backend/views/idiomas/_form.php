<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model backend\models\Idiomas */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row-fluid">
	<div class="col-xs-12">
		<div class="card card-primary">
			<div class="card-header">
            	<h1 class="card-title"><?= Html::encode($this->title) ?></h1>
          	</div>
			<?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'idiomasForm']]); ?>
			<div class="idiomas-form card-body">
			    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
			    <!-- <?//= $form->field($model, 'bandera')->textInput(['maxlength' => true]) ?> -->
			    <?= $form->field($model, 'file')->fileInput(['class'=>'form-control']) ?>
			    <?= $form->field($model, 'estatus')->dropDownList([ 'activo' => 'Activo', 'inactivo' => 'Inactivo', ], ['prompt' => 'Seleccione una opción']) ?>    
			</div>
		    <div class=" card-footer" align="right">
		        <?= Html::Button('Cancelar', ['class' => 'btn btn-default','id'=>'btnCloseForm','onClick'=>'closeForm("idiomasForm")']) ?>
		        <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
		    </div>
			<?php \yii\bootstrap4\ActiveForm::end(); ?>
		</div>
	</div>
</div>