<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Idiomas */

$this->title = 'Update Idiomas: ' . $model->idioma_id;
$this->params['breadcrumbs'][] = ['label' => 'Idiomas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idioma_id, 'url' => ['view', 'id' => $model->idioma_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="idiomas-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
