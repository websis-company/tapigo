<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PaquetesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paquetes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'paquete_id') ?>

    <?= $form->field($model, 'sku') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'descripción') ?>

    <?= $form->field($model, 'precio') ?>

    <?php // echo $form->field($model, 'estatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
