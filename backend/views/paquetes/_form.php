<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\Modulos;
use backend\models\Impresos;
use kartik\select2\Select2;
use wbraganca\dynamicform\DynamicFormWidget; /*** Dymanic Form (@view Componser.json for implementation)***/

/* @var $this yii\web\View */
/* @var $model backend\models\Paquetes */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary ">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fad fa-cubes"></i> <?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close text-white" onclick='closeForm("paquetesForm")'>×</button>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'paquetesForm']]); ?>
            <div class="paquetes-form card-body">
                <?= $form->field($model, 'sku',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'nombre',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                <div class="clearfix"></div>    
                <?= $form->field($model, 'descripción',['options'=>['class'=>'col-12']])->textarea(['rows' => 6]) ?>
                <div class="clearfix"></div>
                <?= $form->field($model, 'precio',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->textInput() ?>
                <?= $form->field($model, 'estatus',['options'=>['class'=>'col-md-6 col-xs-12','style'=>'float: left;']])->dropDownList([ 'Activo' => 'Activo', 'Inactivo' => 'Inactivo', ], ['prompt' => 'Seleccione una opción']) ?>

                <div class="clearfix"></div>

                <div class="row-fluid mt-4">
                    <div class="col-12">
                        <div class="card border border-dark">
                            <div class="card-header bg-dark text-white ">
                                <h5><strong><i class="fas fa-fas fa-puzzle-piece"></i> Módulos</strong></h5>
                            </div>
                             <div class="card-body">
                                <?php
                                /***** Foreach for "Modulos" Selected ****/
                                $arrModulos = []; 
                                if(isset($paquetes_modulos)){
                                    foreach ($paquetes_modulos as $value) {
                                        $arrModulos[] = $value->modulo_id;
                                    }//end foreach
                                }//end if
                                /****************************************/
                                // Multi select with ActiveForm & model [Select2]
                                echo $form->field($modelPaquetesModulos, 'modulo_id',['options'=>['class'=>'col-12']])->label('')->widget(Select2::classname(), [
                                    'data' =>  ArrayHelper::map(Modulos::find()->all(),'modulo_id','nombre'),
                                    'language' => 'es',
                                    'theme' =>'material',
                                    'options' => [
                                        'value' => $arrModulos,//Initial values or selected values
                                        'placeholder' => 'Selecciones los módulos'
                                    ],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'allowClear' => true,
                                        'multiple' => true,
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="clearfix mt-4"></div>
                <div class="row-fluid">
                    <div class="col-12">
                        <div class="card border border-dark">
                            <div class="card-header bg-dark text-white ">
                                <h5><strong><i class="fas fa-print"></i> Impresos</strong></h5>
                            </div>
                            <div class="card-body">
                                <?php DynamicFormWidget::begin([
                                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                    'widgetBody' => '.container-items', // required: css class selector
                                    'widgetItem' => '.item', // required: css class
                                    'limit' => 10, // the maximum times, an element can be cloned (default 999)
                                    'min' => 1, // 0 or 1 (default 1)
                                    'insertButton' => '.add-item', // css class
                                    'deleteButton' => '.remove-item', // css class
                                    'model' => $modelPaquetesImpresos[0],
                                    'formId' => 'paquetesForm',
                                    'formFields' => [
                                        'po_item_no',
                                        'quantity',
                                    ],
                                ]); ?>
                                <div class="container-items"><!-- widgetContainer -->
                                    <?php foreach ($modelPaquetesImpresos as $i => $modelPaquetesImpresos): ?>
                                        <div class="item card"><!-- widgetBody -->
                                            <div class="card-header">
                                                <div class="float-right">
                                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="fas fa-plus-square"></i></button>
                                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="fas fa-minus-square"></i></button>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="card-body">
                                                <?php
                                                    // necessary for update action.
                                                    /*if (!$modelPaquetesImpresos->isNewRecord) {
                                                        echo Html::activeHiddenInput($modelPaquetesImpresos, "[{$i}]impreso_id");
                                                    }*/
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12">
                                                        <?= $form->field($modelPaquetesImpresos, "[{$i}]impreso_id")->dropDownList(
                                                            ArrayHelper::map(Impresos::find()->all(),'impreso_id','nombre'),
                                                            ['prompt'=>'Seleccione una opción']
                                                        ); ?>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <?= $form->field($modelPaquetesImpresos, "[{$i}]cantidad")->textInput(['maxlength' => true]) ?>
                                                    </div>
                                                </div><!-- .row -->
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <?php DynamicFormWidget::end(); ?>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="clearfix mt-3"></div>
                
            </div>
            <div class=" card-footer" align="right">
                <?= Html::Button('Cancelar', ['class' => 'btn btn-default','id'=>'btnCloseForm','onClick'=>'closeForm("paquetesForm")']) ?>
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
            </div>
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>
