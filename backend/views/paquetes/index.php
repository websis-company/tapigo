<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PaquetesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Paquetes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div id="divEditForm" class="col-sm-12 col-md-12 col-lg-6 offset-lg-3" style="display: none;"></div>
</div>

<div class="paquetes-index">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-secondary card-outline">
                    <div class="card-header">
                        <div class="col-6 float-right pb-3">
                            <?php echo Html::button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Agregar Paquete', ['value'=>Url::to('create'),'class' => 'btn bg-gradient-primary float-right','id'=>'btnAddForm']);
                            ?>
                            <!-- <?= Html::a('Create Paquetes', ['create'], ['class' => 'btn btn-success']) ?> -->
                        </div>
                    </div>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    <div class="card-body pad table-responsive">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                //'paquete_id',
                                'sku',
                                'nombre',
                                //'descripción:ntext',
                                'precio',
                                //'estatus',
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'header'=> 'Actions',
                                    'headerOptions'=>['style'=>'text-align:center'],
                                    'template'=>'{view} {update} {delete}',
                                    'buttons'=>[
                                        'view'=>function($url,$model){
                                            return Html::button('<span class="glyphicon glyphicon-search"></span>',['value'=>Url::to(['view','id'=>$model->paquete_id]),'class' => 'btn btnViewForm']);
                                        },
                                        'update'=>function ($url, $model) {
                                            return Html::button('<span class="glyphicon glyphicon-pencil"></span>',['value'=>Url::to(['update','id'=>$model->paquete_id]),'class' => 'btn btnUpdateForm']);
                                        },
                                    ]
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
