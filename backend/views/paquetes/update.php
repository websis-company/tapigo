<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Paquetes */

$this->title = 'Editar: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Paquetes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->paquete_id, 'url' => ['view', 'id' => $model->paquete_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paquetes-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
		'model'                 => $model,
		'modelPaquetesModulos'  => $modelPaquetesModulos,
		'paquetes_modulos'      => $paquetes_modulos,
		'modelPaquetesImpresos' => $modelPaquetesImpresos
    ]) ?>

</div>
