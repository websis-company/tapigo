<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Paquetes */

$this->title = 'Agregar Paquete';
$this->params['breadcrumbs'][] = ['label' => 'Paquetes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquetes-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
		'model'                 => $model,
		'modelPaquetesModulos'  => $modelPaquetesModulos,
		'modelPaquetesImpresos' => $modelPaquetesImpresos
    ]) ?>

</div>
