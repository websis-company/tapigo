<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Paquetesimpresos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paquetesimpresos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'impreso_id')->textInput() ?>

    <?= $form->field($model, 'paquete_id')->textInput() ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
