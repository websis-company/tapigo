<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Paquetesimpresos */

$this->title = 'Create Paquetesimpresos';
$this->params['breadcrumbs'][] = ['label' => 'Paquetesimpresos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquetesimpresos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
