<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Paquetesimpresos */

$this->title = 'Update Paquetesimpresos: ' . $model->paqueteImpresio;
$this->params['breadcrumbs'][] = ['label' => 'Paquetesimpresos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->paqueteImpresio, 'url' => ['view', 'id' => $model->paqueteImpresio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paquetesimpresos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
