<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use backend\models\Empresa;
use backend\models\Idiomas;
use backend\modules\menus\platillos\models\Productosidiomas;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\menus\platillos\models\ProductosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Platillos';
$this->params['breadcrumbs'][] = $this->title;
echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox',
]);

echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox-idiomas',
    'config' => [
        'image' => [      
            // Wait for images to load before displaying
            // Requires predefined image dimensions
            // If 'auto' - will zoom in thumbnail if 'width' and 'height' attributes are found
            'preload'      => "auto",
        ],
        //'dblclickOutside' => 'false',
    ]
]);
?>

<div class="container-fluid">
    <div class="text-center col-12"><div class="loading" style="display: none;"></div></div>
    <div id="divEditForm" class="col-sm-12 col-md-12 col-lg-6 offset-lg-3" style="display: none;"></div>
</div>

<div class="productos-index">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-secondary card-outline">
                    <div class="card-header">
                        <div class="col-12 text-center"> 
                            <?php echo Html::button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Agregar Platillo', ['value'=>Url::to(['create']),'class' => 'btn bg-gradient-primary float-right','id'=>'btnAddForm']);
                            ?>
                        </div>
                    </div>

                    <?php if (Yii::$app->session->hasFlash('success')): ?>
                        <div class="row-fluid mt-2" align="center">
                            <div class="col-sm-12">
                                <div class="alert bg-teal alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                   <i class="icon fa fa-check"></i> <?= Yii::$app->session->getFlash('success') ?>
                               </div>
                            </div>
                        </div>
                   <?php endif; ?>

                   <?php if (Yii::$app->session->hasFlash('error')): ?>
                        <div class="row-fluid mt-2" align="center">
                            <div class="col-sm-12">
                                <div class="alert bg-maroon alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                   <i class="icon fas fa-times"></i> <?= Yii::$app->session->getFlash('error') ?>
                               </div>
                            </div>
                        </div>
                   <?php endif; ?>

                    <div class="card-body pad table-responsive">
                        <?php 
                        /* Validación para mostrar columna idiomas **/
                        if(is_null($idiomas)){
                            $columnIdiomasVisible = false;
                            $arr_idiomas          = [];
                        }else{
                            $arr_idiomas          = explode(',',$idiomas);
                            $columnIdiomasVisible = true;
                        }//end if
                        ?>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            /*** Show Class in GridView ***/
                            'rowOptions' => function($model){
                                if($model->estatus == "Inactivo"){
                                    return ['class'=>'tableDanger'];
                                }elseif($model->estatus == "Activo"){
                                    return ['class'=>'tableSuccess'];
                                }
                            },
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                //'producto_id',
                                //'empresa_id',
                                //'cliente_id',
                                [
                                    'label'     => 'Imagen Principal',
                                    'attribute' => 'imagenPrincipal',
                                    'format'    => 'html',
                                    'value'     =>function($model){
                                        return yii\bootstrap4\Html::a(yii\bootstrap4\Html::img(Url::base()."/".$model->imagenPricipal,['height'=>'75']),Url::base()."/".$model->imagenPricipal,['title'=>'Ver Imagen','class' => 'data-fancybox']);
                                    }
                                ],
                                [
                                    'attribute' => 'categoriaId',
                                    'format'    =>'html',
                                    'value'     => function($model){
                                        $categorias   = $model->categoriasproductos;
                                        $categoriaStr = "<div align='left'>";
                                        foreach ($categorias as $categoria) {
                                            $categoriaStr .= "- ".$categoria->categoria->nombre."<br>";
                                        }//end foreach

                                        return $categoriaStr."</div>";
                                    },
                                ],
                                [
                                    'attribute' => 'alergenosId',
                                    'format'    => 'html',
                                    'value'     => function($model){
                                        $alergenos    = $model->productosalergenos;
                                        $alergenosStr = "<div align='center'>";
                                        foreach ($alergenos as $alergeno) {
                                            $alergenosStr .= "- ".$alergeno->alergeno->nombre."<br>";
                                        }//end foreach
                                        return $alergenosStr."</div>";
                                    }
                                ],
                                'nombre',
                                [
                                    'attribute' => 'Nombre (Inglés)',
                                    'headerOptions' =>['style'=>'text-align:center'],
                                    'visible'   => in_array(1,$arr_idiomas) ? true : false,
                                    'value'     => function($model){
                                        $id_idioma   = 1; //constante
                                        $modelIdioma = Productosidiomas::find()->select('nombre')->where(["idioma_id"=>$id_idioma,'producto_id'=>$model->producto_id])->one();
                                        if(!empty($modelIdioma)){
                                            return $modelIdioma->nombre;
                                        }else{
                                            return "--";
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'Nombre (Alemán)',
                                    'headerOptions' =>['style'=>'text-align:center'],
                                    'visible'   => in_array(2,$arr_idiomas) ? true : false,
                                    'value'     => function($model){
                                        $id_idioma   = 2; //constante
                                        $modelIdioma = Productosidiomas::find()->select('nombre')->where(["idioma_id"=>$id_idioma,'producto_id'=>$model->producto_id])->one();
                                        if(!empty($modelIdioma)){
                                            return $modelIdioma->nombre;
                                        }else{
                                            return "--";
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'Nombre (Francés)',
                                    'headerOptions' =>['style'=>'text-align:center'],
                                    'visible'   => in_array(3,$arr_idiomas) ? true : false,
                                    'value'     => function($model){
                                        $id_idioma   = 3; //constante
                                        $modelIdioma = Productosidiomas::find()->select('nombre')->where(["idioma_id"=>$id_idioma,'producto_id'=>$model->producto_id])->one();
                                        if(!empty($modelIdioma)){
                                            return $modelIdioma->nombre;
                                        }else{
                                            return "--";
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'Nombre (Italiano)',
                                    'headerOptions' =>['style'=>'text-align:center'],
                                    'visible'   => in_array(4,$arr_idiomas) ? true : false,
                                    'value'     => function($model){
                                        $id_idioma   = 4; //constante
                                        $modelIdioma = Productosidiomas::find()->select('nombre')->where(["idioma_id"=>$id_idioma,'producto_id'=>$model->producto_id])->one();
                                        if(!empty($modelIdioma)){
                                            return $modelIdioma->nombre;
                                        }else{
                                            return "--";
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'Nombre (Portugués)',
                                    'headerOptions' =>['style'=>'text-align:center'],
                                    'visible'   => in_array(5,$arr_idiomas) ? true : false,
                                    'value'     => function($model){
                                        $id_idioma   = 5; //constante
                                        $modelIdioma = Productosidiomas::find()->select('nombre')->where(["idioma_id"=>$id_idioma,'producto_id'=>$model->producto_id])->one();
                                        if(!empty($modelIdioma)){
                                            return $modelIdioma->nombre;
                                        }else{
                                            return "--";
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'precio',
                                    'value' => function($model){
                                        return "$ ".number_format($model->precio,2,'.',',');
                                    }
                                ],
                                [
                                    'attribute' =>'tipo',
                                    'value'     => 'tipo',
                                    'filter'    => HTML::activeDropDownList(
                                        $searchModel,
                                        'tipo',
                                        ['General'=>'General','Infantil'=>'Infantil'],
                                        ['class'=>'form-control','prompt'=>'Todos']
                                    ),
                                ],
                                //'calificar',
                                [
                                    'attribute' =>'estatus',
                                    'value'=> 'estatus',
                                    'filter' => HTML::activeDropDownList(
                                        $searchModel,
                                        'estatus',
                                        ['Activo'=>'Activo','Inactivo'=>'Inactivo'],
                                        ['class'=>'form-control','prompt'=>'Todos']
                                    ),
                                ],
                                [
                                    'class'         =>'yii\grid\ActionColumn',
                                    'header'        =>'Idiomas',
                                    'headerOptions' =>['style'=>'text-align:center'],
                                    'template'      =>'{en} {de} {fr} {it} {pr}',
                                    'visible'       =>$columnIdiomasVisible,
                                    'buttons'=>[
                                        'en'=>function($url_,$model,$id_platillo){
                                            $id_idioma    = 1; //constante
                                            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
                                            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
                                            $arr_idiomas  = explode(',',$idiomas);
                                            if (in_array($id_idioma, $arr_idiomas)) {
                                                $modelIdioma = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
                                                return  Html::a(
                                                            Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','width'=>'25']), 
                                                            $url = 'javascript:;', 
                                                            [
                                                                'title'      => $modelIdioma->nombre,
                                                                'class'      => 'data-fancybox-idiomas',
                                                                'data-type'  => 'ajax',
                                                                'data-touch' => false,
                                                                'data-src' => Url::to(['platilloslanguage','id_idioma'=>$id_idioma,'id_platillo'=>$id_platillo])
                                                                //'data-src'   => 'index.php?r=platillos/default/platilloslanguage&id_idioma='.$id_idioma."&id_platillo=".$id_platillo,
                                                            ]
                                                        );
                                            }else{
                                                return false;
                                            }//end if
                                        },
                                        'de'=>function($url,$model,$id_platillo){
                                            $id_idioma    = 2; //constante
                                            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
                                            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
                                            $arr_idiomas  = explode(',',$idiomas);
                                            if (in_array($id_idioma, $arr_idiomas)) {
                                                $modelIdioma = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
                                                return  Html::a(
                                                            Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','width'=>'25']), 
                                                            $url = 'javascript:;', 
                                                            [
                                                                'title'      => $modelIdioma->nombre,
                                                                'class'      => 'data-fancybox-idiomas',
                                                                'data-type'  => 'ajax',
                                                                'data-touch' => false,
                                                                'data-src' => Url::to(['platilloslanguage','id_idioma'=>$id_idioma,'id_platillo'=>$id_platillo])
                                                                //'data-src'   => 'index.php?r=platillos/default/platilloslanguage&id_idioma='.$id_idioma."&id_platillo=".$id_platillo,
                                                            ]
                                                        );
                                            }else{
                                                return false;
                                            }//end if
                                        },
                                        'fr'=>function($url,$model,$id_platillo){
                                            $id_idioma    = 3; //constante
                                            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
                                            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
                                            $arr_idiomas  = explode(',',$idiomas);
                                            if (in_array($id_idioma, $arr_idiomas)) {
                                                $modelIdioma = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
                                                return  Html::a(
                                                            Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','width'=>'25']), 
                                                            $url = 'javascript:;',
                                                            [
                                                                'title'      => $modelIdioma->nombre,
                                                                'class'      => 'data-fancybox-idiomas',
                                                                'data-type'  => 'ajax',
                                                                'data-touch' => false,
                                                                'data-src' => Url::to(['platilloslanguage','id_idioma'=>$id_idioma,'id_platillo'=>$id_platillo])
                                                                //'data-src'   => 'index.php?r=platillos/default/platilloslanguage&id_idioma='.$id_idioma."&id_platillo=".$id_platillo,
                                                            ]
                                                        );
                                            }else{
                                                return false;
                                            }//end if
                                        },
                                        'it'=>function($url,$model,$id_platillo){
                                            $id_idioma    = 4; //constante
                                            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
                                            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
                                            $arr_idiomas  = explode(',',$idiomas);
                                            if (in_array($id_idioma, $arr_idiomas)) {
                                                $modelIdioma = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
                                                return  Html::a(
                                                            Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','width'=>'25']),
                                                            $url = 'javascript:;',
                                                            [
                                                                'title'      => $modelIdioma->nombre,
                                                                'class'      => 'data-fancybox-idiomas',
                                                                'data-type'  => 'ajax',
                                                                'data-touch' => false,
                                                                'data-src' => Url::to(['platilloslanguage','id_idioma'=>$id_idioma,'id_platillo'=>$id_platillo])
                                                                //'data-src'   => 'index.php?r=platillos/default/platilloslanguage&id_idioma='.$id_idioma."&id_platillo=".$id_platillo,
                                                            ]
                                                        );
                                            }else{
                                                return false;
                                            }//end if
                                        },
                                        'pr'=>function($url,$model,$id_platillo){
                                            $id_idioma    = 5; //constante
                                            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
                                            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
                                            $arr_idiomas  = explode(',',$idiomas);
                                            if (in_array($id_idioma, $arr_idiomas)) {
                                                $modelIdioma = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
                                                return  Html::a(
                                                            Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','width'=>'25']),
                                                            $url = 'javascript:;',
                                                            [
                                                                'title'      => $modelIdioma->nombre,
                                                                'class'      => 'data-fancybox-idiomas',
                                                                'data-type'  => 'ajax',
                                                                'data-touch' => false,
                                                                'data-src' => Url::to(['platilloslanguage','id_idioma'=>$id_idioma,'id_platillo'=>$id_platillo])
                                                                //'data-src'   => 'index.php?r=platillos/default/platilloslanguage&id_idioma='.$id_idioma."&id_platillo=".$id_platillo,
                                                            ]
                                                        );
                                            }else{
                                                return false;
                                            }//end if
                                        },

                                    ]
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'header'=> 'Actions',
                                    'headerOptions'=>['style'=>'text-align:center'],
                                    'template'=>'{view} {update} {delete}',
                                    'buttons'=>[
                                        'view'=>function($url,$model){
                                            return Html::button('<span class="glyphicon glyphicon-search"></span>',['value'=>Url::to(['view','id'=>$model->producto_id]),'class' => 'btn btnViewForm']);
                                        },
                                        'update'=>function ($url, $model) {
                                            return Html::button('<span class="glyphicon glyphicon-pencil"></span>',['value'=>Url::to(['update','id'=>$model->producto_id]),'class' => 'btn btnUpdateForm']);
                                        },
                                    ]
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
