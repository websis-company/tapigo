<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\menus\platillos\models\ProductosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'producto_id') ?>

    <?= $form->field($model, 'empresa_id') ?>

    <?= $form->field($model, 'cliente_id') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'calorias') ?>

    <?php // echo $form->field($model, 'precio') ?>

    <?php // echo $form->field($model, 'tipo') ?>

    <?php // echo $form->field($model, 'imagenPricipal') ?>

    <?php // echo $form->field($model, 'imagenSecundaria') ?>

    <?php // echo $form->field($model, 'imagenSecundaria2') ?>

    <?php // echo $form->field($model, 'imagenSecundaria3') ?>

    <?php // echo $form->field($model, 'imagenSecundaria4') ?>

    <?php // echo $form->field($model, 'imagenSecundaria5') ?>

    <?php // echo $form->field($model, 'calificar') ?>

    <?php // echo $form->field($model, 'estatus') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
