<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;

use backend\models\Idiomas;
use backend\modules\menus\platillos\models\Productos;

$modelIdioma   = Idiomas::find()->where(['idioma_id'=>$model->idioma_id])->one();
$modelProducto = Productos::find()->where(['producto_id'=>$model->producto_id])->one();

$this->title = Yii::t('app', 'Información Platillos - '.$modelIdioma->nombre);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Platillos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="categorias-create col-xl-4 col-lg-7 col-md-8 col-sm-12 col-12">
	<div class="row-fluid">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
	                <h1 class="card-title">
	                	<strong>
	                		<i class="nav-icon fas fa-globe-americas"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title); ?>  <?= Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','style'=>'max-width: 40px;']); ?>
	                	</strong>
	                </h1>
	            </div>
	            <?php 
	            $form = ActiveForm::begin([
            			'id'=>"idiomasplatillosform",
            			'action' => ['default/addplatilloslanguage'],
            			'options'=>[
            				'enctype'=>'multipart/form-data',
            			]
            		]); 
	            ?>
				<div class="div-form card-body">
					<h6>- Agregar información para platillo : <strong><?php echo $modelProducto->nombre; ?></strong></h6>
					<?= $form->field($model, 'nombre',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true])->label('Nombre en '.$modelIdioma->nombre) ?>

					<?= $form->field($model, 'descripcion',['options'=>['class'=>'col-12 mt-4','style'=>'float: left;']])->textArea(['rows'=>6,'maxlength' => true])->label('Descripción en '.$modelIdioma->nombre) ?>
					<?= $form->field($model, 'idioma_id')->hiddenInput(['maxlength' => true])->label(''); ?>
					<?= $form->field($model, 'producto_id')->hiddenInput(['maxlength' => true])->label(''); ?>
					<?= $form->field($model, 'productoIdioma_id')->hiddenInput(['maxlength' => true])->label(''); ?>
				</div>
				<div class=" card-footer" align="center">
					<?= Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm','data-fancybox-close'=>true]) ?>
                	<?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
