<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\menus\platillos\models\Productos */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Platillos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox-view',
]);
?>

<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-list-alt"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
            </div>
            <div class="productos-view card-body">
                <div class="col-12" align="right">
                    <?php echo Html::button('<i class="fas fa-edit"></i>',['value'=>Url::to(['update','id'=>$model->producto_id]),'class' => 'btn btn-success btnUpdateView']); ?>

                    <?php echo Html::a('<i class="fas fa-trash-alt"></i>', ['delete', 'id' => $model->producto_id], [
                        'class' => 'btn btn-danger',
                        'title' => 'Eliminar',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
                <div class="col-12 pt-3">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'producto_id',
                            //'empresa_id',
                            //'cliente_id',
                            [
                                'attribute' => 'categoriaId',
                                'format'    =>'html',
                                'value'     => function($model){
                                    $categorias   = $model->categoriasproductos;
                                    $categoriaStr = "<div align='center'>";
                                    foreach ($categorias as $categoria) {
                                        $categoriaStr .= "- ".$categoria->categoria->nombre."<br>";
                                    }//end foreach

                                    return $categoriaStr."</div>";
                                },
                            ],
                            [
                                'attribute' => 'alergenosId',
                                'format'    => 'html',
                                'value'     => function($model){
                                    $alergenos    = $model->productosalergenos;
                                    $alergenosStr = "<div align='center'>";
                                    foreach ($alergenos as $alergeno) {
                                        $alergenosStr .= "- ".$alergeno->alergeno->nombre."<br>";
                                    }//end foreach
                                    return $alergenosStr."</div>";
                                }
                            ],
                            'nombre',
                            'descripcion:ntext',
                            'calorias',
                            [
                                'attribute' => 'precio',
                                'value' => function($model){
                                    return "$ ".number_format($model->precio,2,'.',',');
                                }
                            ],
                            'tipo',
                            [
                                'label'     => 'Imagen Principal',
                                'attribute' => 'imagenPricipal',
                                'format'    => 'html',
                                'value'     =>function($model){
                                    return yii\bootstrap4\Html::a(yii\bootstrap4\Html::img(Url::base()."/".$model->imagenPricipal,['height'=>'75']),Url::base()."/".$model->imagenPricipal,['title'=>'Ver Imagen','class' => 'data-fancybox-view']);
                                }
                            ],
                            //'imagenPricipal',
                            /*'imagenSecundaria',
                            'imagenSecundaria2',
                            'imagenSecundaria3',
                            'imagenSecundaria4',
                            'imagenSecundaria5',*/
                            //'calificar',
                            'estatus',
                        ],
                    ]) ?>
                </div>
            </div>
            <div align="center">
                <div class="card-footer">
                    <?= Html::button('Aceptar',['value'=>'','class'=>'btn btn-primary cancelView']) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /*** Action Button Edit View ***/
    $(".btnUpdateView").on("click",function(e){
        $("#divEditForm").hide(function(e){});
        $("#btnAddForm").show(function(e){});
        $("#divEditForm").load($(this).attr('value'),function(e){
            $("#divEditForm").slideDown('slow');
        });
    });

    /*** Action Button Cancel-Close View ***/
    $(".cancelView").on("click",function(e){
         $("#divEditForm").slideUp(function(e){});
    });
</script>