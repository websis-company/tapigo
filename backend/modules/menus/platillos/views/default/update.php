<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\menus\platillos\models\Productos */

$this->title = Yii::t('app', 'Editar Platillo: {name}', [
    'name' => $model->nombre,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->producto_id, 'url' => ['view', 'id' => $model->producto_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="productos-update">

    <?= $this->render('_form', [
		'model'                   => $model,
		'categorias'              => $categorias,
		'modelCategorias'         => $modelCategorias,
		'modelAlergenos'          => $modelAlergenos,
		'modelProductosAlergenos' => $modelProductosAlergenos,
    ]) ?>

</div>
