<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\modules\menus\platillos\models\Productos */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css" media="screen">
    .field-productos-empresa_id, .field-productos-cliente_id{
        display: none;
    }

    .select2-selection__rendered > li > input{
        min-width: inherit;
        width: 160% !important;
    }
</style>
<div class="row-fluid">
    <div class="col-sm-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-pizza-slice"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close text-white" onclick='closeForm("platillosForm")'>×</button>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'platillosForm']]); ?>
            <div class="productos-form card-body">
                <?= $form->field($model, 'nombre',['options'=>['class'=>'col-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'descripcion',['options'=>['class'=>'col-12 mt-2','style'=>'float: left;']])->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'precio',['options'=>['class'=>'col-md-4 col-sm-12 mt-2','style'=>'float: left;']])->textInput(['placeholder'=>'000000.00']) ?>
                <?= $form->field($model, 'calorias',['options'=>['class'=>'col-md-4 col-sm-12 mt-2','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'tipo',['options'=>['class'=>'col-md-4 col-sm-12 mt-2','style'=>'float: left;']])->dropDownList([ 'General' => 'General', 'Infantil' => 'Infantil', ], ['prompt' => 'Seleccione una opción']) ?>

                <!-- <?= $form->field($model, 'imagenSecundaria')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'imagenSecundaria2')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'imagenSecundaria3')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'imagenSecundaria4')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'imagenSecundaria5')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'calificar')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => '']) ?> -->
                <div class="clearfix"></div>
                <?php
                    $arrCategorias = [];
                    if(isset($modelCategorias)){
                        foreach ($modelCategorias as $value) {
                            $arrCategorias[] = $value->categoria_id;
                        }//end foreach
                    }//end if

                    echo $form->field($model, 'categoriaId',['options'=>['class'=>'col-12 mt-3']])->label('Categoría')->widget(Select2::classname(), [
                                'data' =>  $categorias,
                                'language' => 'es',
                                'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
                                'options' => [
                                    'value' => $arrCategorias,//Initial values or selected values
                                    'placeholder' => 'Relacione el platillo a una o más categorías'
                                ],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'allowClear' => true,
                                    'multiple' => true,
                                ],
                            ]);
                ?>

                <?php 
                    $arrAlergenos = [];
                    if(isset($modelProductosAlergenos)){
                        foreach ($modelProductosAlergenos as $value) {
                            $arrAlergenos[] = $value->alergeno_id;
                        }//end foreach
                    }//end if
                    echo $form->field($model, 'alergenosId',['options'=>['class'=>'col-12 mt-3']])->label('Alérgenos')->widget(Select2::classname(), [
                                'data' =>  ArrayHelper::map($modelAlergenos,'alergeno_id','nombre'),
                                'language' => 'es',
                                'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
                                'options' => [
                                    'value' => $arrAlergenos,//Initial values or selected values
                                    'placeholder' => 'Seleccione los alérgenos para el platillo'
                                ],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'allowClear' => true,
                                    'multiple' => true,
                                ],
                            ]);
                ?>

                <!-- <?//= $form->field($model, 'imagenPricipal',['options'=>['class'=>'col-sm-12 mt-2 bg-light']])->fileInput()->label('<div>Imagen Principal</div> <div class=" alert-info" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 1000px X 1000px (Ancho x Alto)</small></div>'); ?> -->

                <?= $form->field($model, 'fileMainImage',['options'=>['class'=>'col-sm-12 mt-2 bg-light']])->fileInput()->label('<div>Imagen Principal</div> <div class=" alert-primary" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 1000px X 1000px (Ancho x Alto)</small></div>'); ?>

                <?= $form->field($model, 'estatus',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->dropDownList([ 'Activo' => 'Activo', 'Inactivo' => 'Inactivo', ], ['prompt' => 'Seleccione una opción']) ?>

                <?= $form->field($model, 'empresa_id')->hiddenInput() ?>
                <?= $form->field($model, 'cliente_id')->hiddenInput() ?>
            </div>
            <div class=" card-footer" align="right">
                <?= Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm','onClick'=>'closeForm("platillosForm")']) ?>
                <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
            </div>
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>
