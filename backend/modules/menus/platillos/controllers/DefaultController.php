<?php

namespace backend\modules\menus\platillos\controllers;

use Yii;
use backend\modules\menus\platillos\models\Productos;
use backend\modules\menus\platillos\models\ProductosSearch;
use backend\modules\menus\categorias\models\Categorias;
use backend\modules\menus\categorias\models\Categoriasproductos;
use backend\models\Alergenos;
use backend\models\Empresa;
use backend\modules\menus\platillos\models\Productosalergenos;
use backend\modules\menus\platillos\models\Productosidiomas;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\web\ForbiddenHttpException;
use yii\helpers\Html;

/**
 * DefaultController implements the CRUD actions for Productos model.
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index','lists','view','create','update','delete'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'lists' => ['GET','POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Productos models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('generalProducto')){
            $searchModel             = new ProductosSearch();
            $searchModel->empresa_id = Yii::$app->user->identity->empresa_id;
            $searchModel->cliente_id = Yii::$app->user->identity->cliente_id;
            $dataProvider            = $searchModel->search(Yii::$app->request->queryParams);

            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;

            return $this->render('index', [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
                'idiomas'      => $idiomas
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Displays a single Productos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('generalProducto')){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    public function searchLevels($categorias_){
        $model   = new Categorias();
        foreach ($categorias_ as $key => $value) {
            $childrens = $model::find()
                        ->where(['empresa_id'=>Yii::$app->user->identity->empresa_id])
                        ->andWhere(['cliente_id'=>Yii::$app->user->identity->cliente_id])
                        ->andWhere(['categoriaPadre_id'=>$key])
                        ->all();
            foreach ($childrens as $children) {
                $categorias_[$key]["childrens"][$children->categoria_id] = $children->nombre;
            }//end foreach
        }//end foreach
        return $categorias_;
    }//end function

    public function getPrincipalCategory(){
        $model   = new Categorias();
        $parents = $model::find()
                        ->where(['empresa_id'=>Yii::$app->user->identity->empresa_id])
                        ->andWhere(['cliente_id'=>Yii::$app->user->identity->cliente_id])
                        ->andWhere(['categoriaPadre_id'=>NULL])
                        ->all();

        $categories = [];
        foreach ($parents as $parent) {
            $categories[$parent->categoria_id]['nombre'] = $parent->nombre;
            $arrCategories = $this->searchLevels($categories);
        }//end foreach
        return $arrCategories;
    }//end function

    /** Obtiene listado de alergenos **/
    private function getAlergenos(){
        $alergenos      = new Alergenos;
        return $modelAlergenos = $alergenos::find()->where(['estatus'=>'Activo'])->all();
    }//End function

    /**
     * Creates a new Productos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('generalProducto')){
            $model = new Productos();
            $model->empresa_id     = Yii::$app->user->identity->empresa_id;
            $model->cliente_id     = Yii::$app->user->identity->cliente_id;
            $categorias = $this->getPrincipalCategory();
            $selectCategorias = [];
            foreach ($categorias as $key => $attr) {
                $selectCategorias[$key] = $attr["nombre"];
                if(isset($attr["childrens"])){
                    foreach ($attr["childrens"] as $key => $children) {
                        $selectCategorias[$key] = $attr["nombre"]." - ".$children;
                    }//end foreach
                }//end if
            }//end foreach

            /*** Obtiene lista de Alérgenos ***/
            $modelAlergenos =  $this->getAlergenos();

            if ($model->load(Yii::$app->request->post())) {
                //get the instance of the upload file
                $model->fileMainImage = UploadedFile::getInstance($model,'fileMainImage');
                if(!empty($model->fileMainImage)){
                    $imageName             = str_replace(' ','_',$model->fileMainImage->name);
                    $imageName             = str_replace('(','',$imageName);
                    $imageName             = str_replace(')','',$imageName);
                    $model->fileMainImage->saveAs('uploads/platillos/'.$imageName);
                    $model->imagenPricipal = 'uploads/platillos/'.$imageName;    
                }else{
                    $model->imagenPricipal = NULL;    
                }//end if

                if($model->save(false)){
                    $relCategorias = Yii::$app->request->post()["Productos"]["categoriaId"];
                    foreach ($relCategorias as $relation) {
                        $modelRelCategorias               = new Categoriasproductos;
                        $modelRelCategorias->categoria_id = $relation;
                        $modelRelCategorias->producto_id  = $model->producto_id;
                        $modelRelCategorias->save();
                    }//end foreach

                    $relAlergenos = Yii::$app->request->post()["Productos"]['alergenosId'];
                    if(!empty($relAlergenos)){
                        foreach ($relAlergenos as $relation) {
                            $modelRelAlergenos              = new Productosalergenos;
                            $modelRelAlergenos->producto_id = $model->producto_id;
                            $modelRelAlergenos->alergeno_id = $relation;
                            $modelRelAlergenos->save();
                        }//end foreach
                    }


                    Yii::$app->session->setFlash('success', "Se registro correctamente el platillo :  <strong>".$model->nombre."</strong>");
                }else{
                    Yii::$app->session->setFlash('error', "Lo sentimos la categoría :  <strong>".$model->nombre."</strong> no se guardo.");
                }//end if
                return $this->redirect(['index']);
            }//end if

            return $this->renderAjax('create', [
                'model'          => $model,
                'categorias'     => $selectCategorias,
                'modelAlergenos' => $modelAlergenos,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Updates an existing Productos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('generalProducto')){
            $model      = $this->findModel($id);
            $categorias = $this->getPrincipalCategory();
            $selectCategorias = [];
            foreach ($categorias as $key => $attr) {
                $selectCategorias[$key] = $attr["nombre"];
                if(isset($attr["childrens"])){
                    foreach ($attr["childrens"] as $key => $children) {
                        $selectCategorias[$key] = $attr["nombre"]." - ".$children;
                    }//end foreach
                }//end if
            }//end foreach

            /** Obtiene Categorias relacionadas al producto **/
            $modelCat        = new Categoriasproductos;
            $modelCategorias = $modelCat::find()
                            ->where(['producto_id'=>$id])
                            ->all();

            /*** Obtiene lista de Alérgenos ***/
            $modelAlergenos          = $this->getAlergenos();
            $modelProductosAlergenos = Productosalergenos::find()
                                        ->where(['producto_id'=>$id])
                                        ->all();

            if ($model->load(Yii::$app->request->post())) {
                /*** Diff Array Categorias ***/
                $arrActualCategorias = [];
                foreach ($modelCategorias as $categoria) {
                    $arrActualCategorias[] = (string) $categoria->categoria_id;
                }//end foreach
                $arrNewsCategorias = Yii::$app->request->post()["Productos"]["categoriaId"];

                $countAC = count($arrActualCategorias);
                $countNC = count($arrNewsCategorias);

                if($countAC != $countNC){
                    $categoriasDiff = true;
                }else{
                    $categoriasDiff = false;   
                }//end if
                /*** Diff Array Categorias ***/

                /*** Diff Array Alergenos ***/
                if(count($modelProductosAlergenos) >0 ){
                    $arrActualAlergenos = [];
                    foreach ($modelProductosAlergenos as $alergeno) {
                        $arrActualAlergenos[] = (string) $alergeno->alergeno_id;
                    }//end foreach
                    $arrNewsAlergenos = Yii::$app->request->post()["Productos"]['alergenosId'];

                    $countAA = count($arrActualAlergenos);
                    $countNA = count($arrNewsAlergenos);
                     if($countAA != $countNA){
                        $alergenosDiff = true;
                    }else{
                        $alergenosDiff = false;   
                    }//end if
                }else{
                    $alergenosDiff = false;
                }//end if
                /*** Diff Array Alergenos ***/


                //get the instance of the upload file
                $model->fileMainImage = UploadedFile::getInstance($model,'fileMainImage');
                if(!empty($model->fileMainImage)){
                    $imageName             = str_replace(' ','_',$model->fileMainImage->name);
                    $imageName             = str_replace('(','',$imageName);
                    $imageName             = str_replace(')','',$imageName);
                    $model->fileMainImage->saveAs('uploads/platillos/'.$imageName);
                    $model->imagenPricipal = 'uploads/platillos/'.$imageName;    
                }//end if

                if($model->save(false)){
                    if($categoriasDiff == true){
                        /*** Delete ralation Platillo to Category ***/
                        foreach ($modelCategorias as $modelCategoria) {
                            $modelCategoria->delete();
                        }//end foreach
                        /**** Save new relation Platillo to Category ****/
                        $relCategorias = Yii::$app->request->post()["Productos"]["categoriaId"];
                        foreach ($relCategorias as $relation) {
                            $modelRelCategorias = new Categoriasproductos();
                            $modelRelCategorias->categoria_id = $relation;
                            $modelRelCategorias->producto_id  = $model->producto_id;
                            $modelRelCategorias->save();
                        }//end foreach
                    }//end if

                    if($alergenosDiff == true){
                        /*** Delete relation Alergenos Platillos ***/
                        foreach ($modelProductosAlergenos as $modelProductoAlergeno) {
                            $modelProductoAlergeno->delete();
                        }//end foreach
                        /**** Save new relation Platillo to Alergenos ****/
                        $relAlergenos = Yii::$app->request->post()["Productos"]['alergenosId'];
                        foreach ($relAlergenos as $relation) {
                            $modelRelAlergenos              = new Productosalergenos;
                            $modelRelAlergenos->producto_id = $model->producto_id;
                            $modelRelAlergenos->alergeno_id = $relation;
                            $modelRelAlergenos->save();
                        }//end foreach
                    }//end if

                    Yii::$app->session->setFlash('success', "Se actualizo correctamente el platillo :  <strong>".$model->nombre."</strong>");
                }else{
                    Yii::$app->session->setFlash('error', "Lo sentimos la categoría :  <strong>".$model->nombre."</strong> no se actualizo.");
                }//end if

                return $this->redirect(['index']);
            }

            return $this->renderAjax('update', [
                'model'                   => $model,
                'categorias'              => $selectCategorias,
                'modelCategorias'         => $modelCategorias,
                'modelAlergenos'          => $modelAlergenos,
                'modelProductosAlergenos' => $modelProductosAlergenos,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Deletes an existing Productos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('generalProducto')){
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Finds the Productos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Productos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Productos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }//end function

    public function actionLists($parent_id,$id){
        $modelCatProd = new Categoriasproductos;

        if($id != "all"){
            $platillos    = $modelCatProd::find()->where(['categoria_id'=>$id])->all();
            if(count($platillos)>0){
                $html = "";
                foreach ($platillos as $platillo) {
                    $html = '
                        <div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-12" style="float: left;">
                            <div class="col-2" style="float:left;">';
                                $html.= Html::checkbox('platillos[]', $checked = false, ['id'=>'platillo_'.$platillo->producto->producto_id.'_'.$id,'value' => $platillo->producto->producto_id,'class'=>'']);
                    $html .= '
                            </div>
                            <div class="col-10" style="float:left;">
                            ';
                                $html.= Html::label($platillo->producto->nombre, $for = 'platillo_'.$platillo->producto->producto_id.'_'.$id, ['class' => 'pl-1 text-primary']); 
                    $html.= '
                            </div>
                        </div>
                    ';
                    echo $html;
                }//end foreach
            }else{
                 echo '<div class="callout callout-danger">
                                <p>Sin Platillos Asignados</p>
                            </div>';
            }//end if
        }else{
            $modelCategorias = new Categorias;
            $categorias      = $modelCategorias::find()->where(['categoriaPadre_id'=>$parent_id])->all();
            $categoriaPadre  = $modelCategorias::find()->where(['categoria_id'=>$parent_id])->one();

            if(count($categorias) > 0){
                $html = '';

                $platillos    = $modelCatProd::find()->where(['categoria_id'=>$parent_id])->all();
                if(count($platillos)>0){
                    $html .= '<fieldset><legend class="bg-gray-dark" style="border-radius: 0px; padding-left: 15px;">'.$categoriaPadre->nombre.'</legend> ';
                    foreach ($platillos as $platillo) {
                        $html .= '
                                    <div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-12" style="float: left;">
                                        <div class="col-2" style="float:left;">';
                                    $html.= Html::checkbox('platillos['.$parent_id.'][]', $checked = true, ['id'=>'platillo_'.$platillo->producto->producto_id.'_'.$id,'value' => $platillo->producto->producto_id,'class'=>'']);
                        $html .= '      </div>
                                        <div class="col-10" style="float:left;">';
                                    $html.= Html::label($platillo->producto->nombre, $for = 'platillo_'.$platillo->producto->producto_id.'_'.$id, ['class' => 'pl-1 text-primary']); 
                        $html.= '        </div>
                                    </div>
                        ';
                    }//end foreach
                    $html .= '</fieldset>';
                }//end if


                foreach ($categorias as $categoria) {
                    $platillos = $categoria->categoriasproductos;
                    if(count($platillos) > 0){
                        $html .= '<fieldset><legend class="bg-gray-dark" style="border-radius: 0px; padding-left: 15px;">'.$categoria->nombre.'</legend> ';
                        foreach ($platillos as $platillo) {
                            $html .= ' 
                                        <div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-12" style="float: left;">
                                            <div class="col-2" style="float:left;">';
                                        $html.= Html::checkbox('platillos['.$categoria->categoria_id.'][]', $checked = true, ['id'=>'platillo_'.$platillo->producto->producto_id.'_'.$categoria->categoria_id,'value' => $platillo->producto->producto_id,'class'=>'']);
                            $html .= '      </div>
                                            <div class="col-10" style="float:left;">';
                                        $html.= Html::label($platillo->producto->nombre, $for = 'platillo_'.$platillo->producto->producto_id.'_'.$categoria->categoria_id, ['class' => 'pl-1 text-primary']); 
                            $html.= '       </div>
                                        </div>';
                        }//end foreach
                        $html .= '</fieldset>';
                    }else{
                        //echo "<div class='text-danger'>Sin Platillos Asignados</div>";
                        $html .= '<fieldset><legend class="bg-gray-dark" style="border-radius: 0px; padding-left: 15px;">'.$categoria->nombre.'</legend>';
                        $html .= '<div class="callout callout-danger"><p>Sin Platillos Asignados</p></div>';
                        $html .= '</fieldset>';
                    }//end if
                }//end foreach
                echo $html;
                die();
            }else{
                $platillos    = $modelCatProd::find()->where(['categoria_id'=>$parent_id])->all();
                if(count($platillos)>0){
                    $html = "";
                    foreach ($platillos as $platillo) {
                        $html .= '
                            <div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-12" style="float: left;">
                                <div class="form-check">';
                                    $html.= Html::checkbox('platillos['.$parent_id.'][]', $checked = true, ['id'=>'platillo_'.$platillo->producto->producto_id.'_'.$id,'value' => $platillo->producto->producto_id,'class'=>'']);
                                    $html.= Html::label($platillo->producto->nombre, $for = 'platillo_'.$platillo->producto->producto_id.'_'.$id, ['class' => 'pl-1 text-primary']); 
                        $html.= '</div>
                            </div>
                        ';
                    }//end foreach
                }else{
                    $html .='<div class="callout callout-danger"><p>Sin Platillos Asignados</p></div>';
                }
                echo $html;
                die();
            }//end if
        }//end if
    }//end function

    public function actionPlatilloslanguage($id_idioma,$id_platillo){
        if(Yii::$app->user->can('interIdiomas') || Yii::$app->user->can('premiumIdiomas')){
            $model = Productosidiomas::find()->where(['idioma_id'=>$id_idioma,'producto_id'=>$id_platillo])->one();
            if(empty($model)){
                $model              = new Productosidiomas;
                $model->idioma_id   = $id_idioma;
                $model->producto_id = $id_platillo;
            }//end if

            return  $this->renderAjax('_formplatillos', [
                        'model'                 => $model,
                    ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    public function actionAddplatilloslanguage(){
        $productoIdioma_id = Yii::$app->request->post()["Productosidiomas"]["productoIdioma_id"];
        if(!empty($productoIdioma_id)){
            $model = Productosidiomas::find()->where(['productoIdioma_id'=>$productoIdioma_id])->one();
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', "Se actulizo la información correctamente.");
                return $this->redirect(['index']);
            }
        }else{
            $model = new Productosidiomas();
            Yii::$app->session->setFlash('success', "Se guardo la información correctamente.");
            if($model->load(Yii::$app->request->post()) && $model->save()){
                return $this->redirect(['index']);
            }//end if
        };
    }


}
