<?php

namespace backend\modules\menus\platillos\models;

use Yii;
use backend\models\Clientes;
use backend\models\Empresa;
use backend\modules\menus\categorias\models\Categoriasproductos;


/**
 * This is the model class for table "productos".
 *
 * @property int $producto_id
 * @property int $empresa_id
 * @property int $cliente_id
 * @property string $nombre
 * @property string|null $descripcion
 * @property string|null $calorias
 * @property float $precio
 * @property string $tipo
 * @property string|null $imagenPricipal
 * @property string|null $imagenSecundaria
 * @property string|null $imagenSecundaria2
 * @property string|null $imagenSecundaria3
 * @property string|null $imagenSecundaria4
 * @property string|null $imagenSecundaria5
 * @property string|null $calificar
 * @property string|null $estatus
 *
 * @property Calificacionesplatillos[] $calificacionesplatillos
 * @property Categoriasproductos[] $categoriasproductos
 * @property Clientes $cliente
 * @property Empresa $empresa
 * @property Productosalergenos[] $productosalergenos
 * @property Productosidiomas[] $productosidiomas
 */
class Productos extends \yii\db\ActiveRecord
{
    public $categoriaId;
    public $alergenosId;
    public $fileMainImage;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id', 'cliente_id', 'nombre', 'precio', 'tipo','categoriaId','estatus'], 'required'],
            [['empresa_id', 'cliente_id'], 'integer'],
            [['descripcion', 'tipo', 'calificar', 'estatus'], 'string'],
            [['precio'], 'number'],
            [['nombre'], 'string', 'max' => 255],
            [['calorias', 'imagenPricipal'], 'string', 'max' => 250],
            [['imagenSecundaria', 'imagenSecundaria2', 'imagenSecundaria3', 'imagenSecundaria4', 'imagenSecundaria5','alergenosId'],'safe'],
            [['fileMainImage'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 250,'maxWidth'=>1000,'minHeight'=>250,'maxHeight'=>1000,'maxSize'=>1024 * 1024 * 2],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente_id' => 'cliente_id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'empresa_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'producto_id' => 'Producto ID',
            'empresa_id' => 'Empresa ID',
            'cliente_id' => 'Cliente ID',
            'categoriaId' => 'Categoría',
            'alergenosId' => 'Alérgenos',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'calorias' => 'Calorías',
            'precio' => 'Precio',
            'tipo' => 'Tipo',
            'fileMainImage'=>'Imagen Principal',
            'imagenPricipal' => 'Imagen Pricipal',
            'imagenSecundaria' => 'Imagen Secundaria',
            'imagenSecundaria2' => 'Imagen Secundaria2',
            'imagenSecundaria3' => 'Imagen Secundaria3',
            'imagenSecundaria4' => 'Imagen Secundaria4',
            'imagenSecundaria5' => 'Imagen Secundaria5',
            'calificar' => 'Calificar',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Calificacionesplatillos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCalificacionesplatillos()
    {
        return $this->hasMany(Calificacionesplatillos::className(), ['producto_id' => 'producto_id']);
    }

    /**
     * Gets query for [[Categoriasproductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriasproductos()
    {
        return $this->hasMany(Categoriasproductos::className(), ['producto_id' => 'producto_id']);
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Empresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['empresa_id' => 'empresa_id']);
    }

    /**
     * Gets query for [[Productosalergenos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosalergenos()
    {
        return $this->hasMany(Productosalergenos::className(), ['producto_id' => 'producto_id']);
    }

    /**
     * Gets query for [[Productosidiomas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosidiomas()
    {
        return $this->hasMany(Productosidiomas::className(), ['producto_id' => 'producto_id']);
    }
}
