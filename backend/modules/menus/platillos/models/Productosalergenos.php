<?php

namespace backend\modules\menus\platillos\models;

use Yii;
use backend\models\Alergenos;
/**
 * This is the model class for table "productosalergenos".
 *
 * @property int $productoAlergeno_id
 * @property int $producto_id
 * @property int $alergeno_id
 *
 * @property Productos $producto
 * @property Alergenos $alergeno
 */
class Productosalergenos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productosalergenos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto_id', 'alergeno_id'], 'required'],
            [['producto_id', 'alergeno_id'], 'integer'],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['producto_id' => 'producto_id']],
            [['alergeno_id'], 'exist', 'skipOnError' => true, 'targetClass' => Alergenos::className(), 'targetAttribute' => ['alergeno_id' => 'alergeno_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'productoAlergeno_id' => 'Producto Alergeno ID',
            'producto_id' => 'Producto ID',
            'alergeno_id' => 'Alergeno ID',
        ];
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Productos::className(), ['producto_id' => 'producto_id']);
    }

    /**
     * Gets query for [[Alergeno]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlergeno()
    {
        return $this->hasOne(Alergenos::className(), ['alergeno_id' => 'alergeno_id']);
    }
}
