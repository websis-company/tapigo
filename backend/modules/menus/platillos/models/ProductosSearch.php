<?php

namespace backend\modules\menus\platillos\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\menus\platillos\models\Productos;

/**
 * ProductosSearch represents the model behind the search form of `backend\modules\menus\platillos\models\Productos`.
 */
class ProductosSearch extends Productos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto_id', 'empresa_id', 'cliente_id'], 'integer'],
            [['nombre', 'descripcion', 'calorias', 'tipo', 'calificar', 'estatus'], 'safe'],
            [['precio'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Productos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'producto_id' => $this->producto_id,
            'empresa_id' => $this->empresa_id,
            'cliente_id' => $this->cliente_id,
            'precio' => $this->precio,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'calorias', $this->calorias])
            ->andFilterWhere(['=', 'tipo', $this->tipo])
            /*->andFilterWhere(['like', 'imagenPricipal', $this->imagenPricipal])
            ->andFilterWhere(['like', 'imagenSecundaria', $this->imagenSecundaria])
            ->andFilterWhere(['like', 'imagenSecundaria2', $this->imagenSecundaria2])
            ->andFilterWhere(['like', 'imagenSecundaria3', $this->imagenSecundaria3])
            ->andFilterWhere(['like', 'imagenSecundaria4', $this->imagenSecundaria4])
            ->andFilterWhere(['like', 'imagenSecundaria5', $this->imagenSecundaria5])*/
            ->andFilterWhere(['like', 'calificar', $this->calificar])
            ->andFilterWhere(['=', 'estatus', $this->estatus]);

        return $dataProvider;
    }
}
