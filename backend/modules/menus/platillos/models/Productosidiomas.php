<?php

namespace backend\modules\menus\platillos\models;

use Yii;

use backend\modules\menus\platillos\models\Productos;
use backend\models\Idiomas;
/**
 * This is the model class for table "productosidiomas".
 *
 * @property int $productoIdioma
 * @property int $idioma_id
 * @property int $producto_id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $calorias
 *
 * @property Productos $producto
 * @property Idiomas $idioma
 */
class Productosidiomas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productosidiomas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idioma_id', 'producto_id','nombre'], 'required'],
            [['productoIdioma_id','idioma_id', 'producto_id'], 'integer'],
            [['descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 250],
            [['calorias'], 'string', 'max' => 255],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['producto_id' => 'producto_id']],
            [['idioma_id'], 'exist', 'skipOnError' => true, 'targetClass' => Idiomas::className(), 'targetAttribute' => ['idioma_id' => 'idioma_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'productoIdioma_id' => 'Producto Idioma ID',
            'idioma_id'      => 'Idioma ID',
            'producto_id'    => 'Producto ID',
            'nombre'         => 'Nombre',
            'descripcion'    => 'Descripción',
            'calorias'       => 'Calorias',
        ];
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Productos::className(), ['producto_id' => 'producto_id']);
    }

    /**
     * Gets query for [[Idioma]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdioma()
    {
        return $this->hasOne(Idiomas::className(), ['idioma_id' => 'idioma_id']);
    }
}
