<?php

namespace backend\modules\menus\menus\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

use backend\modules\menus\categorias\models\Categorias;
use backend\modules\menus\categorias\models\Categoriasproductos;
use backend\modules\menus\menus\models\Menus;
use backend\modules\menus\menus\models\MenusSearch;
use backend\modules\menus\menus\models\Articulos;
use backend\modules\menus\platillos\models\Productos;
use backend\models\Empresa;

/**
 * Default controller for the `menus` module
 */
class DefaultController extends Controller
{
	 /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index'                   => ['GET','POST'],
                    'delcategoria'            => ['GET','POST'],
                    'delsubcategoria'         => ['GET','POST'],
                    'delproducto'             => ['GET','POST'],
                    'editsumordensubcategory' => ['GET','POST'],
                    'editsubordensubcategory' => ['GET','POST'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($tabactive = NULL){
    	if(Yii::$app->user->can('basicMenu') || Yii::$app->user->can('premiumMenus')){
            #-- Busca Menú
            $menu = Menus::find()
                        ->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id,'flag'=>'principal'])
                        ->count();

            if($menu > 0){
                $modelMenu = Menus::find()
                        ->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id,'flag'=>'principal'])
                        ->one();
            }else{
                $modelMenu = false;
            }//end if

            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id])->one();
            
            if(!is_null($tabactive)){
                return $this->render('index',['modelMenu' => $modelMenu,'tabactive'=>$tabactive,'modelEmpresa'=>$modelEmpresa]);
            }else{
                return $this->render('index',['modelMenu' => $modelMenu,'modelEmpresa'=>$modelEmpresa]);
            }//end if
    	}else{
    		throw new ForbiddenHttpException;
    	}//end if
    }//end function

    public function actionAddcategory(){
        //$modelMenus     = new Menus;
        //$modelArticulos = new Articulos;
        //$modelCategorias = new Categorias;
        $modelCat = Categorias::find()
                        ->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])
                        ->andWhere(['categoriaPadre_id' => NULL])
                        ->all();

        return $this->renderAjax('addRegister', [
            'modelCat' => $modelCat
        ]);
    }//end function

    public function actionEditsubcategory($menuId,$idParent,$id,$tabactive){
        $platillos     = Categoriasproductos::find()->where(['categoria_id'=>$id])->all();
        $seleccionados = Articulos::find('producto_id')->where(['categoriaPadre_id'=>$idParent,'categoria_id'=>$id])->all();

        $arr_platillos = [];
        foreach ($seleccionados as $seleccionado) {
            $arr_platillos[] = $seleccionado->producto_id;
        }//end foreach

        return $this->renderAjax('editRegister',[
            'menuId'        => $menuId,
            'idParent'      => $idParent,
            'idCategory'    => $id,
            'platillos'     => $platillos,
            'seleccionados' => $arr_platillos,
            'tabactive'     => $tabactive,
        ]);
    }//end function

    public function actionSaveform(){
        $menus = Menus::find()
                        ->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])
                        ->count();

        /**No encuentra Ménú, genera un registro***/
        if($menus == 0){
            $modelMenu             =  new Menus;
            $modelMenu->empresa_id = Yii::$app->user->identity->empresa_id;
            $modelMenu->cliente_id = Yii::$app->user->identity->cliente_id;
            $modelMenu->nombre     = 'TAPIGO'.Yii::$app->user->identity->empresa_id."-".Yii::$app->user->identity->cliente_id;
            $modelMenu->flag       = 'principal';
            $modelMenu->estatus    = 'Activo';
            $modelMenu->save();
        }else{
            $modelMenu = Menus::find()
                        ->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id,'flag'=>'principal'])
                        ->one();
        }//end if

        //Seleccina agregar todo
        if(Yii::$app->request->post()["categoria_id"] == "all"){
            $ordenCatHijos  = 1;
            $ordenPlatillos = 1;
            /** Busca incrementable Categoria Padre **/
            $ordenPadre = Articulos::find()->where(['menu_id'=>$modelMenu->menu_id])->orderBy(['ordencatpadre'=>SORT_DESC])->one();
            if(is_null($ordenPadre)){
                $ordenCatPadres = 1;
            }else{
                $ordenCatPadres = ($ordenPadre->ordencatpadre + 1);
            }//end if

            $parentId       = Yii::$app->request->post()["categoriaPadre_id"];
            if(isset(Yii::$app->request->post()["platillos"])){
                $platillos = Yii::$app->request->post()["platillos"];
                #-- Busca categorias de los platillos seleccionados
                foreach ($platillos as $categoria_id => $platillos_id) {
                    $ordenPlatillos = 1;
                    foreach ($platillos_id as $platillo_id) {
                        /** Busa si el platillo ya esta agreado al menú **/
                        $searchArticle = Articulos::find()->where(['menu_id'=>$modelMenu->menu_id,'categoriaPadre_id'=>$parentId,'categoria_id'=>$categoria_id,'producto_id'=>$platillo_id])->count();

                        if($searchArticle == 0){
                            $modelArticulos                    = new Articulos;
                            $modelArticulos->menu_id           = $modelMenu->menu_id;
                            $modelArticulos->categoriaPadre_id = $parentId;
                            $modelArticulos->categoria_id      = $categoria_id;
                            $modelArticulos->producto_id       = $platillo_id;
                            $modelArticulos->ordencatpadre     = $ordenCatPadres;
                            $modelArticulos->ordencathijo      = $ordenCatHijos;
                            $modelArticulos->ordenproducto     = $ordenPlatillos;
                            $modelArticulos->estatus           = "Activo";
                            $modelArticulos->save();
                        }//end if
                        $ordenPlatillos++;
                    }//end foreach
                    $ordenCatHijos++;
                }//end foreach

                Yii::$app->session->setFlash('success', "Los platillos se cargaron correctamente.");
            }else{
                Yii::$app->session->setFlash('error', "Lo sentimos no hay platillos para cargar al menú.");
            }//end if
        }else{ //Selecciona agregar subcategoría en especifico
            $parentId    = Yii::$app->request->post()["categoriaPadre_id"];
            $categoriaId = Yii::$app->request->post()["categoria_id"];

            /*** Busca orden (Categoria padre) ***/
            $ordenPadre    = Articulos::find()->where(['menu_id'=>$modelMenu->menu_id,'categoriaPadre_id'=>$parentId])->orderBy(['ordencatpadre'=>SORT_DESC])->one();
            $ordenHijo     = Articulos::find()->where(['menu_id'=>$modelMenu->menu_id,'categoriaPadre_id'=>$parentId])->orderBy(['ordencathijo'=>SORT_DESC])->one();
            $ordenPlatillo = Articulos::find()->where(['menu_id'=>$modelMenu->menu_id,'categoriaPadre_id'=>$parentId])->orderBy(['ordenproducto'=>SORT_DESC])->one();

            if(!is_null($ordenPadre)){
                $ordenCatPadres = $ordenPadre->ordencatpadre;
            }else{
                $ordenCatPadres = 1;
            }//end if

            if(!is_null($ordenHijo)){
                $ordenCatHijos = ($ordenHijo->ordencathijo + 1);
            }else{
                $ordenCatHijos = 1;
            }//end if
            
            $ordenPlatillos = 1;

            if(isset(Yii::$app->request->post()["platillos"])){
                $platillos   = Yii::$app->request->post()["platillos"];
                foreach ($platillos as $platillo_id) {
                    /** Busa si el platillo ya esta agreado al menú **/
                    $searchArticle = Articulos::find()->where(['menu_id'=>$modelMenu->menu_id,'categoriaPadre_id'=>$parentId,'categoria_id'=>$categoriaId,'producto_id'=>$platillo_id])->count();
                    if($searchArticle == 0){
                        $modelArticulos                    = new Articulos;
                        $modelArticulos->menu_id           = $modelMenu->menu_id;
                        $modelArticulos->categoriaPadre_id = $parentId;
                        $modelArticulos->categoria_id      = $categoriaId;
                        $modelArticulos->producto_id       = $platillo_id;
                        $modelArticulos->ordencatpadre     = $ordenCatPadres;
                        $modelArticulos->ordencathijo      = $ordenCatHijos;
                        $modelArticulos->ordenproducto     = $ordenPlatillos;
                        $modelArticulos->estatus           = "Activo";
                        $modelArticulos->save();
                    }//end if
                    $ordenPlatillos++;
                }//end foreach
                Yii::$app->session->setFlash('success', "Los platillos se cargaron correctamente.");
            }else{
                Yii::$app->session->setFlash('error', "No se selecciono platillo para cargar al menú.");
            }//end if
        }//end if

        return $this->redirect(['index']);
    }//end function

    public function actionSaveeditform(){
        $menuId      = Yii::$app->request->post()["menu_id"];
        $platillos   = !isset(Yii::$app->request->post()["platillos"]) ? NULL : Yii::$app->request->post()["platillos"];
        $parentCatId = Yii::$app->request->post()["parent_id"];
        $categoryId  = Yii::$app->request->post()["category_id"];
        $tabactive   = Yii::$app->request->post()["tabactive"];
        
        $articulos   = Articulos::find()->where(['menu_id'=>$menuId,'categoriaPadre_id'=>$parentCatId,'categoria_id'=>$categoryId])->all();

        /** Busca orden categoria Padre **/
        $ordenCatPadre = Articulos::find()->select('ordencatpadre')->where(['menu_id'=>$menuId,'categoriaPadre_id'=>$parentCatId])->one();
        /** Busca orden categorias Hijos **/
        $ordenCatHijo = Articulos::find()->select('ordencathijo')->where(['menu_id'=>$menuId,'categoriaPadre_id'=>$parentCatId,'categoria_id'=>$categoryId])->one();

        foreach ($articulos as $articulo) {
            $articulo->delete();
        }//end foreach

        if(isset(Yii::$app->request->post()["platillos"])){
            $c = 1;
            foreach ($platillos as $platillo) {
                $modelArticulos                    = new Articulos;
                $modelArticulos->menu_id           = $menuId;
                $modelArticulos->categoriaPadre_id = $parentCatId;
                $modelArticulos->categoria_id      = $categoryId;
                $modelArticulos->producto_id       = $platillo;
                $modelArticulos->ordencatpadre     = $ordenCatPadre->ordencatpadre;
                $modelArticulos->ordencathijo      = $ordenCatHijo->ordencathijo;
                $modelArticulos->ordenproducto     = $c;
                $modelArticulos->estatus           = "Activo";
                $modelArticulos->save();
                $c++;
            }//end foreach
        }//end if*/

        return $this->redirect(['index','tabactive'=>$tabactive]);
    }//end function

    public function actionEditordencategory($menu_id,$categoriaPadre_id){
        if(Yii::$app->user->can('basicMenu') || Yii::$app->user->can('premiumMenus')){
            /** Busca orden categoria Padre **/
            $modelCategoria = Articulos::find()->where(['menu_id'=>$menu_id,'categoriaPadre_id'=>$categoriaPadre_id])->one();
            return $this->renderAjax('editOrden',['modelCategoria' => $modelCategoria]);
        }else{
            throw new ForbiddenHttpException;
        }   
    }//end function

    public function actionSaveeditordencategory(){
        $newOrderCatPadre  = Yii::$app->request->post()["ordenCatPadre"];
        $menu_id           = Yii::$app->request->post()["menu_id"];
        $categoriaPadre_id = Yii::$app->request->post()["parent_id"];

        $articulos = Articulos::find()->where(['menu_id'=>$menu_id,'categoriaPadre_id'=>$categoriaPadre_id])->all();
        foreach ($articulos as $articulo) {
            $articulo->ordencatpadre = $newOrderCatPadre;
            $articulo->save();
        }//end foreach
        return $this->redirect(['index']);
    }//end function

    /*public function actionSaveeditordensubcategory(){
        $newOrderCatHijo   = Yii::$app->request->post()["ordenCatHijo"];
        $menu_id           = Yii::$app->request->post()["menu_id"];
        $categoriaPadre_id = Yii::$app->request->post()["parent_id"];
        $categoria_id      = Yii::$app->request->post()["chield_id"];
        $tabactive         = Yii::$app->request->post()["tabactive"];

        $articulos = Articulos::find()->where(['menu_id'=>$menu_id,'categoriaPadre_id'=>$categoriaPadre_id,'categoria_id'=>$categoria_id])->all();
        foreach ($articulos as $articulo) {
            $articulo->ordencathijo = $newOrderCatHijo;
            $articulo->save();
        }//end foreach
        return $this->redirect(['index','tabactive'=>$tabactive]);
    }//end function*/

    public function actionDelcategoria($id){
        if(Yii::$app->user->can('basicMenu') || Yii::$app->user->can('premiumMenus')){
            $modelArticulos = Articulos::find()->where(["categoriaPadre_id"=>$id])->all();
            foreach ($modelArticulos as $modelArticulo) {
                $modelArticulo->delete();
            }//end foreach
            return true;
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    public function actionDelsubcategoria($id,$tabactive){
        if(Yii::$app->user->can('basicMenu') || Yii::$app->user->can('premiumMenus')){
            $modelArticulos = Articulos::find()->where(["categoria_id"=>$id])->all();
            foreach ($modelArticulos as $modelArticulo) {
                $modelArticulo->delete();
            }//end foreach
            return $this->redirect(['index','tabactive'=>$tabactive]);
            //return true;
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    public function actionDelproducto($id,$tabactive){
        if(Yii::$app->user->can('basicMenu') || Yii::$app->user->can('premiumMenus')){
            $modelArticulos = Articulos::find()->where(["articulo_id"=>$id])->one();
            $modelArticulos->delete();
            return $this->redirect(['index','tabactive'=>$tabactive]);
            //return true;
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    public function actionEditsumordensubcategory($menu_id,$categoriaPadre_id,$categoria_id,$tabactive){
        if(Yii::$app->user->can('basicMenu') || Yii::$app->user->can('premiumMenus')){
            /** Busca orden categoria Padre **/
            $modelCategoria  = Articulos::find()->where(['menu_id'=>$menu_id,'categoriaPadre_id'=>$categoriaPadre_id,'categoria_id'=>$categoria_id])->one();
            $newOrdenCatHijoMenos = ($modelCategoria->ordencathijo-1);

            $articulosDesc       = Articulos::find()->where(['menu_id'=>$menu_id,'categoriaPadre_id'=>$categoriaPadre_id,'ordencathijo'=>$newOrdenCatHijoMenos])->all();
            $newOrdenCatHijoMas = ($newOrdenCatHijoMenos + 1);
            foreach ($articulosDesc as $articulo_desc) {
                $articulo_desc->ordencathijo = $newOrdenCatHijoMas;
                $articulo_desc->save();
            }//end foreach

            $articulosAsc = Articulos::find()->where(['menu_id'=>$menu_id,'categoriaPadre_id'=>$categoriaPadre_id,'categoria_id'=>$categoria_id])->all();
            foreach ($articulosAsc as $articulo_asc) {
                $articulo_asc->ordencathijo = $newOrdenCatHijoMenos;
                $articulo_asc->save();
            }//end foreach

            return $this->redirect(['index','tabactive'=>$tabactive]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    public function actionEditsubordensubcategory($menu_id,$categoriaPadre_id,$categoria_id,$tabactive){
        if(Yii::$app->user->can('basicMenu') || Yii::$app->user->can('premiumMenus')){
            /** Busca orden categoria Padre **/
            $modelCategoria     = Articulos::find()->where(['menu_id'=>$menu_id,'categoriaPadre_id'=>$categoriaPadre_id,'categoria_id'=>$categoria_id])->one();
            $newOrdenCatHijoMas = ($modelCategoria->ordencathijo+1);

            $articulosAsc       = Articulos::find()->where(['menu_id'=>$menu_id,'categoriaPadre_id'=>$categoriaPadre_id,'ordencathijo'=>$newOrdenCatHijoMas])->all();
            $newOrdenCatHijoMenos = ($newOrdenCatHijoMas - 1);
            foreach ($articulosAsc as $articulo_asc) {
                $articulo_asc->ordencathijo = $newOrdenCatHijoMenos;
                $articulo_asc->save();
            }//end foreach

            $articulosDesc = Articulos::find()->where(['menu_id'=>$menu_id,'categoriaPadre_id'=>$categoriaPadre_id,'categoria_id'=>$categoria_id])->all();
            foreach ($articulosDesc as $articulo_desc) {
                $articulo_desc->ordencathijo = $newOrdenCatHijoMas;
                $articulo_desc->save();
            }//end foreach

            return $this->redirect(['index','tabactive'=>$tabactive]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function


    /**** Sucursales ****/
    public function actionSucursales($tabactive = NULL){
        if(Yii::$app->user->can('premiumMenus')){
            $searchModel             = new MenusSearch();
            $searchModel->cliente_id = Yii::$app->user->identity->cliente_id;
            $searchModel->empresa_id = Yii::$app->user->identity->empresa_id;
            $searchModel->flag       = "sucursal";
                        
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('sucursales', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function



}
