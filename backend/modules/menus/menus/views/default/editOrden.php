<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

use backend\modules\menus\categorias\models\Categorias;
/* @var $this yii\web\View */
/* @var $model backend\modules\menus\categorias\models\Categorias */
$categoria                     = Categorias::find()->where(['categoria_id'=>$modelCategoria->categoriaPadre_id])->one();
$this->title                   = Yii::t('app', 'Actualizar Posición para "'.$categoria->nombre.'"');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categorias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orden-edit col-md-3 col-sm-12">
	<div class="row-fluid">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
	                <h1 class="card-title"><strong><i class="fas fa-sort-amount-up-alt"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
	            </div>
	            <?php $form = ActiveForm::begin([
	                'id'      => 'editOrdenForm',
	                'action' => ['default/saveeditordencategory'],
	                'options' => [
	                    'enctype'=>'multipart/form-data',
	                    'method' => 'post',
	                ]
	            ]); ?>
	            <div class="div-form" align="center">
	            	<div class="form-group col-sm-3 mt-3">
	            		<?= Html::label('Posición', $for = 'ordencatpadre', ['class' => 'pl-1 text-primary']);?>
	            		<?= Html::input('text', $name = 'ordenCatPadre', $value = $modelCategoria->ordencatpadre, ['class' => 'form-control text-center','id'=>'ordencatpadre']); ?>
	            	</div>
	            </div>
	            <div class="clearfix"></div>
				<div class=" card-footer" align="center">
					<?= Html::input('hidden', $name = 'menu_id', $value = $modelCategoria->menu_id, ['option' => 'value']); ?>
					<?= Html::input('hidden', $name = 'parent_id', $value = $modelCategoria->categoriaPadre_id, ['option' => 'value']); ?>
					<?= Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm','data-fancybox-close'=>true]) ?>
                	<?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>