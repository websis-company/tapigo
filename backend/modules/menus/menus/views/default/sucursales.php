<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MenusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menús - Sucursales');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="text-center col-12"><div class="loading" style="display: none;"></div></div>
    <div id="divEditForm" class="col-sm-12 col-md-12 col-lg-6 offset-lg-3" style="display: none;"></div>
</div>

<div class="menus-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <!-- <p>
        <?= Html::a(Yii::t('app', 'Create Menus'), ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'menu_id',
            'cliente_id',
            'empresa_id',
            'sucursal_id',
            'nombre',
            //'descripcion:ntext',
            //'flag',
            //'estatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
