<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

use backend\modules\menus\categorias\models\Categorias;

/* @var $this yii\web\View */
/* @var $model backend\modules\menus\categorias\models\Categorias */
$this->title = Yii::t('app', 'Editar Información');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categorias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$categoria = Categorias::find()->where(['categoria_id'=>$idCategory])->one();
?>
<div class="categorias-create col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12">
	<div class="row-fluid">
	<div class="col-12">
		<div class="card card-primary">
			<div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-list-alt"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
            </div>
        	<?php $form = ActiveForm::begin([
                'id'      => 'editInformacionForm',
                'action' => ['default/saveeditform'],
                'options' => [
                    'enctype'=>'multipart/form-data',
                    'method' => 'post',
                ]
            ]); ?>
            <div class="div-form">
            	<fieldset>
            		<legend class="bg-gray-dark text-left" style="font-size: 18px;"> 
            			<div style="padding: 0.70rem 1.20rem;"><i class="fas fa-angle-double-right"></i> <?php echo $categoria->nombre; ?></div>
            		</legend>
	            	<div class="clearfix"></div>
	            	<div class="mt-3">
	            		<?php 
	            		/***** Mostrar los platillos seleccionados ******/
	            		foreach ($platillos as $platillo) {
							$platillo_nombre = $platillo->producto->nombre;
							$platillo_id     = $platillo->producto->producto_id;

							/** Busca si esta seleccionado **/
							$checked_ = "";
							if(in_array($platillo_id, $seleccionados)){
								$checked_ = true;
							}else{
								$checked_ = false;
							}//end if
	            		?>
		            		<div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-12" style="float: left;">
			            		<div class="col-2" style="float:left;">
			            			<?php echo Html::checkbox('platillos[]', $checked = $checked_, ['id'=>'platillo_'.$platillo_id,'value' => $platillo_id]); ?>
			            		</div>
			            		<div class="col-10" style="float:left;">
			            			<?php echo Html::label($platillo_nombre, $for = 'platillo_'.$platillo_id, ['class' => 'pl-1 text-primary']);?>
			            		</div>
			            	</div>	
	            		<?php
	            		}//end foreach
	            		?>
	            	</div>
	            </fieldset>
            </div>

            <div class=" card-footer" align="center">
            	<?= Html::input('hidden', $name = 'menu_id', $value = $menuId, ['option' => 'value']); ?>
            	<?= Html::input('hidden', $name = 'parent_id', $value = $idParent, ['option' => 'value']); ?>
            	<?= Html::input('hidden', $name = 'category_id', $value = $idCategory, ['option' => 'value']); ?>
            	<?= Html::input('hidden', $name = 'tabactive', $value = $tabactive, ['option' => 'value']); ?>
            	<?= Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm','data-fancybox-close'=>true]) ?>
                <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
	</div>
	<?php 
	// echo "<pre>";
	// var_dump($idParent);
	// echo "</pre>";

	// echo "<pre>";
	// var_dump($idCategory);
	// echo "</pre>";

	// echo "<pre>";
	// var_dump($platillos);
	// echo "</pre>";
	?>

</div>