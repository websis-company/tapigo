<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\menus\categorias\models\Categorias */

$this->title = Yii::t('app', 'Agregar Información');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categorias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categorias-create col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12">
    <?= $this->render('_form', [
		'modelCat' => $modelCat,
    ]) ?>

</div>