<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
?>

<div class="row-fluid">
	<div class="col-12">
		<div class="card card-primary">
			<div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-list-alt"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
            </div>
              <?php $form = ActiveForm::begin([
                    'id'      => 'informacionForm',
                    'action' => ['default/saveform'],
                    'options' => [
                        'enctype'=>'multipart/form-data',
                        'method' => 'post',
                    ]
                ]); ?>
            <div class="div-form card-body">
            	<?= Html::label('Categoría', $for = 'categoriaPadre_id', ['class' => '']); ?>
            	<?= Html::dropDownList(
            			'categoriaPadre_id', 
            			$selection = null,
            			ArrayHelper::map($modelCat,'categoria_id','nombre'), 
            			[
                            'id'       => 'categoriaPadre_id',
                            'prompt'   => 'Seleccione una cateogoría',
                            'class'    => 'form-control',
                            'onchange' => '
                                            var url_to = "'.Url::to(['/categorias/default/lists']).'";
                                            var id_    = $(this).val();
											$.get(url_to,{"id":id_},function(data){
												$("#categoria_id").html(data);
                                                $("#divPlatillos").html("");
                                                $("#categoria_id").trigger("onchange");
											});

                                            ',
							'required' => true
            			]
            		); ?>

            	<div id="divSubcategory" class="mt-2">
            		<?= Html::label('Argegar Platillos :', $for = 'categoria_id', ['class' => '']); ?>
            		<?= Html::dropDownList(
                            'categoria_id', 
                            $selection = null, 
                            $items = [], 
                            [
                                'prompt'   => 'Seleccione una opción',
                                'id'       => 'categoria_id',
                                'class'    => 'form-control',
                                'onchange' => '
                                                var url_to    = "'.Url::to(['/platillos/default/lists']).'";
                                                var parent_id = $("#categoriaPadre_id").val();
                                                var id_       = $(this).val();
                                                $.get(url_to,{"parent_id":parent_id,"id":id_},function(data){
                                                    $("#divPlatillos").html(data);
                                                });
                                            ',

                            ]
                        ); ?>
            	</div>

                <div id="divPlatillos" class="mt-3"></div>

            	<!-- Lorem ipsum dolor sit amet consectetur adipisicing, elit. Optio perferendis non quasi enim dolore dignissimos vero facilis fugit eligendi animi harum rerum repudiandae dolor, velit aliquam in, ut adipisci. Molestias? -->
            </div>
            <div class=" card-footer" align="center">
            	<?= Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm','data-fancybox-close'=>true]) ?>
                <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
            </div>
             <?php ActiveForm::end(); ?>
		</div>
	</div>
</div>