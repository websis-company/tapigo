<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use backend\modules\menus\categorias\models\Categorias;
use backend\modules\menus\platillos\models\Productos;
use backend\modules\menus\menus\models\Articulos;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\menus\platillos\models\ProductosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menú Digítal';
$this->params['breadcrumbs'][] = $this->title;
echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox-form',
    'config' => [
        'image' => [      
            // Wait for images to load before displaying
            // Requires predefined image dimensions
            // If 'auto' - will zoom in thumbnail if 'width' and 'height' attributes are found
            'preload'      => "auto",
        ],
        'clickSlide'      => false,
        'clickOutside'    => false,
        'dblclickContent' => false,
        'dblclickSlide'   => false,
        'dblclickOutside' => false,


        //'dblclickOutside' => 'false',
    ]
]);

if($modelMenu != false){
    $menuId    = $modelMenu->menu_id;
    $articulos = $modelMenu->articulos;
    $arr_menu = [];
    foreach ($articulos as $articulo) {
        $arr_menu[$articulo->categoriaPadre_id][$articulo->categoria_id][$articulo->articulo_id] = $articulo->producto_id;
    }//end foreach
}else{
    $arr_menu = [];
}//end if


?>

<style type="text/css" media="screen">
    .tableSuccess{
        background-color: #dff0d8 !important;
    }

    .tableDanger{
        background-color: #f2dede !important;   
    }
</style>

<div class="card card-teal">
    <div class="card-header bg-primary">
        <h1 class="card-title">
            <strong>Crea tu Menú Digítal ¡Ahora!</strong>
        </h1>
        <!-- <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div> -->
    </div>
    <div class="card-body">
        <div class="row-fluid">
            <div class="alert alert-light">
                Instrucciones:<br>
                1. Seleccione las categorías que conformarán las opciones del menú.<br>
                2. Agregue los platillos al menú. <br>
                3. Compartalo con sus clientes. <br>
            </div>
        </div>

        <div class="row-fluid">
            <div class="mt-5">
                <div class="col-12" style="float: left;">
                    <a class="data-fancybox-form btn btn-primary" href="javascript:;" data-type="ajax" data-touch="false" data-src="<?php echo Url::to(['addcategory']) ?>">
                        <i class="fas fa-plus-circle"></i> Agregar Categoría
                    </a>
                </div>

                <div class="clearfix"></div>

                <?php if (Yii::$app->session->hasFlash('success')): ?>
                        <div class="row-fluid mt-2" align="center">
                            <div class="col-sm-12">
                                <div class="alert bg-teal alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                   <i class="icon fa fa-check"></i> <?= Yii::$app->session->getFlash('success') ?>
                               </div>
                            </div>
                        </div>
                   <?php endif; ?>

                   <?php if (Yii::$app->session->hasFlash('error')): ?>
                        <div class="row-fluid mt-2" align="center">
                            <div class="col-sm-12">
                                <div class="alert bg-maroon alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                   <i class="icon fas fa-times"></i> <?= Yii::$app->session->getFlash('error') ?>
                               </div>
                            </div>
                        </div>
                   <?php endif; ?>

                <div class="row-fluid mt-3">
                    <div class="col-sm-12">
                        <div class="card card-teal card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <?php 
                                    /*** Categorías  ***/
                                    if(count($arr_menu) > 0){
                                        $t=0;
                                        foreach ($arr_menu as $parents => $children) {
                                                $parentCategory      = Categorias::find()->where(['categoria_id'=>$parents])->one();
                                                $ordenParentCategory = Articulos::find()->select('ordencatpadre')->where(['menu_id'=>$menuId,'categoriaPadre_id'=>$parents])->one();
                                                /** Busca que pestaña activar**/
                                                if(isset($tabactive)){
                                                    if($parents == $tabactive){
                                                        $active = "active";    
                                                    }else{
                                                        $active = "";    
                                                    }//end if
                                                }else{
                                                    if($t==0){
                                                        $active = "active";
                                                    }else{
                                                        $active = "";
                                                    }//end if
                                                }//end if

                                            ?>
                                            <li class="nav-item">
                                                <button type="button" class="btn btn-teal text-white dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="sr-only text-danger">Toggle Dropdown</span>
                                                    <div class="dropdown-menu text-danger" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-1px, 37px, 0px);">
                                                        <?= Html::a('<i class="fas fa-trash-alt"></i> Eliminar "'.$parentCategory->nombre.'"', $url = 'javascript:;', ['class' => 'dropdown-item text-danger','title'=>'Eliminar '.$parentCategory->nombre,'onclick'=>'confirmDeleteCategory('.$parents.',"'.$parentCategory->nombre.'");']); ?>
                                                        <div class="dropdown-divider"></div>
                                                        <?= Html::a(
                                                                '<i class="fas fa-sort-amount-up-alt"></i> Actualizar Posición', 
                                                                $url = 'javascript:;',
                                                                [
                                                                    'class' => 'data-fancybox-form dropdown-item text-primary',
                                                                    'data-type'=>'ajax',
                                                                    'data-touch'=>'false',
                                                                    'data-src'=>Url::to(['/menus/default/editordencategory','menu_id'=>$menuId,'categoriaPadre_id'=>$parents]),
                                                                ]
                                                            ); ?>
                                                    </div>
                                                </button>
                                                <?= Html::a(
                                                        "<span class='text-primary font-weight-bold'>".$ordenParentCategory->ordencatpadre."</span> &nbsp;&nbsp;".$parentCategory->nombre,
                                                        $url = "#tabs_".$parents, 
                                                        [
                                                            'class' => 'nav-link '.$active,'id'=>$parents."_tab",
                                                            'data-toggle'=>'pill',
                                                            'role'=>'tab',
                                                            'aria-controls'=>'tabs_'.$parents,
                                                            'aria-selected'=>'false',
                                                            'style'=>'float:right;'
                                                        ]
                                                    );?>
                                            </li>
                                            <?php 
                                            $t++;
                                        }//end foreach
                                    }//end if
                                    ?>
                                </ul>
                            </div>
                            <div class="">
                                <div class="tab-content" id="custom-tabs-one-tabContent">
                                    <?php 
                                    /*** Subcategorias ***/
                                    if(count($arr_menu) > 0){
                                        $content = 0;
                                        foreach ($arr_menu as $parents => $children) {
                                                /** Busca que contenido activar **/
                                                if(isset($tabactive)){
                                                    if($parents == $tabactive){
                                                        $active = "active show";    
                                                    }else{
                                                        $active = "";    
                                                    }//end if
                                                }else{
                                                    if($content == 0){
                                                        $active = 'active show';
                                                    }else{
                                                        $active = '';
                                                    }//end if
                                                }//end if
                                            ?> 
                                               <div class="tab-pane fade <?php echo $active; ?>" id="tabs_<?php echo $parents;?>" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                                    <div id="accordion">
                                                    <?php 
                                                    $col   = 0;
                                                    $total = count($children);
                                                    foreach ($children as $subcategory => $products){
                                                        $modelSubcategoria = Categorias::find()->where(['categoria_id'=>$subcategory])->one();
                                                        $ordenHijosCategory = Articulos::find()->select('ordencathijo')->where(['menu_id'=>$menuId,'categoriaPadre_id'=>$parents,'categoria_id'=>$subcategory])->one();
                                                    ?>
                                                        <div class="card card-gray-dark">
                                                            <div class="card-header">
                                                                <h4 class="card-title text-white">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $col; ?>" class="text-white">
                                                                       <i class="fas fa-caret-right"></i>&nbsp;&nbsp;<?php echo $modelSubcategoria->nombre; ?>
                                                                    </a>
                                                                </h4>
                                                                <?= Html::a(
                                                                        '<i class="far fa-trash-alt"></i>', 
                                                                        $url = null,
                                                                        [
                                                                            'class'   =>'float-right ml-2 btn btn-xs btn-sm btn-danger',
                                                                            'title'   =>'Eliminar '.$modelSubcategoria->nombre,
                                                                            'onclick' =>'confirmDeleteSubcategory('.$subcategory.',"'.$modelSubcategoria->nombre.'",'.$parents.')'
                                                                        ]
                                                                );?>
                                                                
                                                                <?= Html::a(
                                                                        '<i class="fas fa-edit"></i>',
                                                                        $url = null, 
                                                                        [
                                                                            'class'      =>'data-fancybox-form float-right mr-2 btn btn-xs btn-sm btn-primary',
                                                                            'title'      =>'Editar Platillos',
                                                                            'data-type'  =>'ajax',
                                                                            'data-touch' =>'false',
                                                                            'data-src' => Url::to(['/menus/default/editsubcategory','menuId'=>$menuId,'idParent'=>$parents,'id'=>$subcategory,'tabactive'=>$parents])
                                                                        ]
                                                                    );?>
                                                                
                                                                <?php 
                                                                /** Botón para Bajar Posiciones **/
                                                                /** No muestra en la última posición **/
                                                                if(($col+1) != $total){
                                                                ?>
                                                                <?= Html::a('<i class="fas fa-sort-amount-down"></i>', $url = null, ['class' => 'float-right mr-2 btn btn-xs btn-sm btn-primary','title'=>'Bajar Posición','onclick'=>'subPosition('.$menuId.','.$parents.','.$subcategory.')']); ?>
                                                                <?php 
                                                                }//end if
                                                                ?>

                                                                <?php 
                                                                /** Botón para subir Posiciones **/
                                                                /** No muestra en la primera posición **/
                                                                if($col > 0){
                                                                ?>
                                                                <?= Html::a('<i class="fas fa-sort-amount-up-alt"></i>', $url = null, ['class' => 'float-right mr-2 btn btn-xs btn-sm btn-primary','title'=>'Subir Posición','onclick'=>'sumPosition('.$menuId.','.$parents.','.$subcategory.')']); ?>
                                                                <?php 
                                                                }//end if
                                                                ?>
                                                                
                                                                <?= Html::a("<strong> Posición ".$ordenHijosCategory->ordencathijo."</strong>", $url = null, ['class' => 'float-right mr-4 text-white']); ?>

                                                            </div>
                                                            <div id="collapse<?php echo $col; ?>" class="panel-collapse collapse in  collapse show">
                                                                <div class="card-body bg-light">
                                                                    <?php 
                                                                    foreach ($products as $articulo_id => $platillo_id) {
                                                                        $modelProducto = Productos::find()->where(['producto_id'=>$platillo_id])->one();
                                                                    ?>
                                                                        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6" style="float: left; min-height: 150px;">
                                                                            <div class="col-12 bg-light" style="float: left; text-align: center; background-image: url(<?php echo Url::base()."/".$modelProducto->imagenPricipal?>); background-position: center; background-size: cover; height: 120px;"></div>
                                                                            <p>
                                                                                <?= Html::a('<i class="far fa-trash-alt"></i>', $url = null, ['class' => 'btn btn-block btn-danger btn-xs btn-sm','onclick'=>'confirmDeleteProduct('.$articulo_id.',"'.$modelProducto->nombre.'",'.$parents.')']); ?>
                                                                            </p>
                                                                            <div class="col-12" style="float: left; height: 80px;">
                                                                                <p class="text-sm-center text-md-center text-lg-left text-xl-left  font-weight-bold">
                                                                                    <?php echo $modelProducto->nombre; ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    <?php 
                                                                    }//end foreach
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php 
                                                    $col++;
                                                    }//end foreach
                                                    ?>
                                                    </div>
                                                </div>
                                            <?php 
                                            $content++;
                                        }//end foreach
                                    }//end if
                                    ?>
                                    <!-- <div class="tab-pane fade active show" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                     Mauris tincidunt mi at erat gravida, eget tristique urna bibendum. Mauris pharetra purus ut ligula tempor, et vulputate metus facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas sollicitudin, nisi a luctus interdum, nisl ligula placerat mi, quis posuere purus ligula eu lectus. Donec nunc tellus, elementum sit amet ultricies at, posuere nec nunc. Nunc euismod pellentesque diam. 
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                                     Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat augue id mi placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit sem, ac tristique nulla. Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut velit condimentum, mattis urna a, malesuada nunc. Curabitur eleifend facilisis velit finibus tristique. Nam vulputate, eros non luctus efficitur, ipsum odio volutpat massa, sit amet sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae gravida fermentum, lectus ipsum gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus tincidunt eleifend ac ornare magna. 
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="custom-tabs-one-settings-tab">
                                     Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus turpis ac, ornare sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis vulputate. Morbi euismod molestie tristique. Vestibulum consectetur dolor a vestibulum pharetra. Donec interdum placerat urna nec pharetra. Etiam eget dapibus orci, eget aliquet urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim. In hac habitasse platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis. 
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="mt-5">
                <h2>Platillos</h2>
            </div> -->
        </div>
    </div>
    <div class="card-footer bg-light" align="center">
        <h4> <i class="fas fa-share-alt"></i> Compartir</h4>
        <?php 
        if(!empty($modelEmpresa->slug)){
        ?>
        <?= Html::a('https://menu.tapigo.com/'.$modelEmpresa->slug, $url = 'https://menu.tapigo.com/'.$modelEmpresa->slug, ['target' => '_blank']); ?>
        <br>
        <div class="mt-3 mb-3">
            <?= Html::a('<i class="fab fa-whatsapp"></i>', $url = 'https://api.whatsapp.com/send?text=https://menu.tapigo.com/'.$modelEmpresa->slug, ['class' => 'btn bg-teal','style'=>'border-radius: 30px; margin-left: 5px;','target'=>'_blank']); ?>
            <?= Html::a('<i class="fab fa-facebook"></i>', $url = null, ['class' => 'btn bg-teal','style'=>'border-radius: 30px; margin-left: 5px;']); ?>
            <?= Html::a('<i class="fab fa-twitter"></i>', $url = null, ['class' => 'btn bg-teal','style'=>'border-radius: 30px; margin-left: 5px;']); ?>
        </div>
        <?php 
        }//end if
        ?>
    </div>
</div>

<script type="text/javascript">

    function confirmDeleteCategory(id,name){
        var response = confirm("¿En realidad desea eliminar la categoría '"+name+"' del menú?"); 
        if(response == true){
            var url_to = "<?php echo Url::to(['/menus/default/delcategoria'])?>";
            $.get(url_to,{"id":id},function(data){
                location.reload();
            });
            return false;
        }else{
            console.log("false")
            return false;
        }
    }//end function
    
    function confirmDeleteSubcategory(id,name,tabactive){
        var response = confirm("¿En realidad desea eliminar '"+name+"' del menú?"); 
        if(response == true){
            var url_to = "<?php echo Url::to(['/menus/default/delsubcategoria'])?>";
            $.get(url_to,{"id":id,"tabactive":tabactive},function(data){});
            return false;
        }else{
            console.log("false")
            return false;
        }//end if
    }//end function

    function confirmDeleteProduct(idArticulo,platillo,tabactive){
        var response = confirm("¿En realidad desea eliminar el platillo '"+platillo+"' del menú?"); 
        if(response == true){
            var url_to = "<?php echo Url::to(['/menus/default/delproducto'])?>";
            $.get(url_to,{"id":idArticulo,"tabactive":tabactive},function(data){});
            return true;
        }else{
            console.log("false")
            return false;
        }//end if
    }//end function

    function sumPosition(menuId,parent,subcategory){
        var url_to = "<?php echo Url::to(['/menus/default/editsumordensubcategory'])?>";
        $.get(url_to,{"menu_id":menuId,"categoriaPadre_id":parent,"categoria_id":subcategory,"tabactive":parent},function(data){return true;});
    }//end function

    function subPosition(menuId,parent,subcategory){
        var url_to = "<?php echo Url::to(['/menus/default/editsubordensubcategory'])?>";
        $.get(url_to,{"menu_id":menuId,"categoriaPadre_id":parent,"categoria_id":subcategory,"tabactive":parent},function(data){return true;});
    }//end function
</script>

