<?php

namespace backend\modules\menus\menus\models;

use Yii;
use backend\modules\menus\menus\models\Menus;
use backend\modules\menus\platillos\models\Productos;

/**
 * This is the model class for table "articulos".
 *
 * @property int $articulo_id
 * @property int $menu_id
 * @property int|null $categoriaPadre_id
 * @property int|null $categoria_id
 * @property int|null $producto_id
 * @property string|null $estatus
 *
 * @property Menus $menu
 * @property Productos $producto
 */
class Articulos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articulos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id'], 'required'],
            [['menu_id', 'categoriaPadre_id', 'categoria_id', 'producto_id','ordencatpadre','ordencathijo','ordenproducto'], 'integer'],
            [['estatus'], 'string'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menus::className(), 'targetAttribute' => ['menu_id' => 'menu_id']],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['producto_id' => 'producto_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'articulo_id' => 'Articulo ID',
            'menu_id' => 'Menu ID',
            'categoriaPadre_id' => 'Categoria Padre ID',
            'categoria_id' => 'Categoria ID',
            'producto_id' => 'Producto ID',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Menu]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menus::className(), ['menu_id' => 'menu_id']);
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Productos::className(), ['producto_id' => 'producto_id']);
    }
}
