<?php

namespace backend\modules\menus\categorias\models;

use Yii;
use backend\models\Idiomas;
use backend\modules\menus\categorias\models\Categorias;
/**
 * This is the model class for table "categoriasidiomas".
 *
 * @property int $categoriaIdioma_id
 * @property int $idioma_id
 * @property int $categoria_id
 * @property string|null $nombre
 * @property string|null $descripcion
 *
 * @property Idiomas $idioma
 * @property Categorias $categoria
 */
class Categoriasidiomas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categoriasidiomas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idioma_id', 'categoria_id','nombre'], 'required'],
            [['idioma_id', 'categoria_id'], 'integer'],
            [['descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 255],
            [['idioma_id'], 'exist', 'skipOnError' => true, 'targetClass' => Idiomas::className(), 'targetAttribute' => ['idioma_id' => 'idioma_id']],
            [['categoria_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['categoria_id' => 'categoria_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'categoriaIdioma_id' => 'Categoria Idioma ID',
            'idioma_id' => 'Idioma ID',
            'categoria_id' => 'Categoria ID',
            'nombre' => 'Categoría',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Idioma]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdioma()
    {
        return $this->hasOne(Idiomas::className(), ['idioma_id' => 'idioma_id']);
    }

    /**
     * Gets query for [[Categoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(Categorias::className(), ['categoria_id' => 'categoria_id']);
    }
}
