<?php

namespace backend\modules\menus\categorias\models;

use Yii;
use backend\modules\menus\platillos\models\Productos;
use backend\modules\menus\categorias\models\Categorias;

/**
 * This is the model class for table "categoriasproductos".
 *
 * @property int $categoriaProducto_id
 * @property int $categoria_id
 * @property int $producto_id
 *
 * @property Menus $menu
 * @property Productos $producto
 * @property Categorias $categoria
 */
class Categoriasproductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categoriasproductos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoria_id', 'producto_id'], 'required'],
            [['categoria_id', 'producto_id'], 'integer'],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['producto_id' => 'producto_id']],
            [['categoria_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['categoria_id' => 'categoria_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'categoriaProducto_id' => 'Categoria Producto ID',
            'categoria_id' => 'Categoria ID',
            'producto_id' => 'Producto ID',
        ];
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Productos::className(), ['producto_id' => 'producto_id']);
    }

    /**
     * Gets query for [[Categoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(Categorias::className(), ['categoria_id' => 'categoria_id']);
    }
}
