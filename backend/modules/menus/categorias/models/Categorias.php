<?php

namespace backend\modules\menus\categorias\models;

use Yii;
use backend\models\Clientes;
use backend\models\Empresa;

use backend\modules\menus\categorias\models\Categoriasidiomas;

/**
 * This is the model class for table "categorias".
 *
 * @property int $categoria_id
 * @property int $cliente_id
 * @property int|null $categoriaPadre_id
 * @property string $nombre
 * @property string|null $descripcion
 * @property string|null $estatus
 *
 * @property Clientes $cliente
 * @property Categorias $categoriaPadre
 * @property Categorias[] $categorias
 * @property Categoriasproductos[] $categoriasproductos
 * @property Cateogoriasidiomas[] $cateogoriasidiomas
 */
class Categorias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_id','empresa_id','nombre','estatus'], 'required'],
            [['cliente_id', 'categoriaPadre_id'], 'integer'],
            [['descripcion', 'estatus'], 'string'],
            [['nombre'], 'string', 'max' => 250],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'empresa_id']],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente_id' => 'cliente_id']],
            [['categoriaPadre_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['categoriaPadre_id' => 'categoria_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'categoria_id' => 'Categoría ID',
            'categoriaPadre_id' => 'Categoría Padre',
            'cliente_id' => 'Cliente ID',
            'empresa_id' => 'Empresa ID',
            'nombre' => 'Categoría',
            'descripcion' => 'Descripción',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['cliente_id' => 'cliente_id']);
    }

    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['empresa_id' => 'empresa_id']);
    }

    /**
     * Gets query for [[CategoriaPadre]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriaPadre()
    {
        return $this->hasOne(Categorias::className(), ['categoria_id' => 'categoriaPadre_id']);
    }

    /**
     * Gets query for [[Categorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias()
    {
        return $this->hasMany(Categorias::className(), ['categoriaPadre_id' => 'categoria_id']);
    }

    /**
     * Gets query for [[Categoriasproductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriasproductos()
    {
        return $this->hasMany(Categoriasproductos::className(), ['categoria_id' => 'categoria_id']);
    }

    /**
     * Gets query for [[Cateogoriasidiomas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriasidiomas()
    {
        return $this->hasMany(Categoriasidiomas::className(), ['categoria_id' => 'categoria_id']);
    }
}
