<?php

namespace backend\modules\menus\categorias\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\menus\categorias\models\Categorias;

/**
 * CategoriasSearch represents the model behind the search form of `backend\modules\menus\categorias\models\Categorias`.
 */
class CategoriasSearch extends Categorias
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id','categoria_id', 'cliente_id'], 'integer'],
            [['nombre', 'descripcion', 'estatus','categoriaPadre_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Categorias::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'empresa_id'        => $this->empresa_id,
            'categoria_id'      => $this->categoria_id,
            'cliente_id'        => $this->cliente_id,
            'categoriaPadre_id' => $this->categoriaPadre_id,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['=', 'estatus', $this->estatus]);

        return $dataProvider;
    }
}
