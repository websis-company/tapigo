<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\menus\categorias\models\Categorias */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css" media="screen">
    .field-categorias-empresa_id, .field-categorias-cliente_id{
        display: none;
    }    
</style>
<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-list-alt"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close text-white" onclick='closeForm("categoriasForm")'>×</button>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'categoriasForm']]); ?>
            <div class="modulos-form card-body">
                <?php 
                $catPrincipales = [];
                foreach ($categoriasPrincipales as $principales) {
                    $catPrincipales[$principales->categoria_id] = $principales->nombre;
                }//end forach

                ?>
                <!-- <?//= $form->field($model, 'categoriaPadre_id')->textInput()->label('Categoría Padre') ?> -->
                <?php
                    // Normal select with ActiveForm & model [Select2]
                    echo $form->field($model, 'categoriaPadre_id')->label('<span class="text-dark">Categoría Padre</span>')->widget(Select2::classname(), [
                        'data' => $catPrincipales,
                        'language' => 'es',
                        'theme' =>Select2::THEME_BOOTSTRAP,
                        'options' => [
                            'value' => $model->categoriaPadre_id,
                            'placeholder' => 'Seleccione una categoría'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]);
                    ?>

                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
                <!-- <?//= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?> -->
                <?= $form->field($model, 'estatus')->dropDownList([ 'Activo' => 'Activo', 'Inactivo' => 'Inactivo', ], ['prompt' => 'Seleccine una opción']) ?>
                <?= $form->field($model, 'empresa_id')->hiddenInput()->label('')?>
                <?= $form->field($model, 'cliente_id')->hiddenInput()->label('')?>
            </div>
            <div class=" card-footer" align="right">
                <?= Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm','onClick'=>'closeForm("categoriasForm")']) ?>
                <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
            </div>
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>


