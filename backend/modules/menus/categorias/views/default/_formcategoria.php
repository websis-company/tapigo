<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;

use backend\models\Idiomas;
use backend\modules\menus\categorias\models\Categorias;

$modelIdioma   = Idiomas::find()->where(['idioma_id'=>$model->idioma_id])->one();
$modelCategory = Categorias::find()->where(['categoria_id'=>$model->categoria_id])->one();


$this->title = Yii::t('app', 'Información - '.$modelIdioma->nombre);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categorias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="categorias-create col-xl-3 col-lg-7 col-md-8 col-sm-12 col-12">
	<div class="row-fluid">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
	                <h1 class="card-title">
	                	<strong>
	                		<i class="nav-icon fas fa-globe-americas"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?> <?= Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','style'=>'max-width: 40px;']); ?>
	                	</strong>
	                </h1>
	            </div>
	            <?php 
	            $form = ActiveForm::begin([
            			'id'=>"idiomascategoriasform",
            			'action' => ['default/addcategorylanguage'],
            			'options'=>[
            				'enctype'=>'multipart/form-data',
            			]
            		]); 
	            ?>
				<div class="div-form card-body">
					<h6>- Agregar información para : <strong><?php echo $modelCategory->nombre; ?></strong></h6>
					<?= $form->field($model, 'nombre',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true])->label('Nombre en '.$modelIdioma->nombre) ?>
					<?= $form->field($model, 'idioma_id')->hiddenInput(['maxlength' => true])->label(''); ?>
					<?= $form->field($model, 'categoria_id')->hiddenInput(['maxlength' => true])->label(''); ?>
					<?= $form->field($model, 'categoriaIdioma_id')->hiddenInput(['maxlength' => true])->label(''); ?>
				</div>
				<div class=" card-footer" align="center">
					<?= Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm','data-fancybox-close'=>true]) ?>
                	<?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>