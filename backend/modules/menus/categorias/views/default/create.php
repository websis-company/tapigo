<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\menus\categorias\models\Categorias */

$this->title = Yii::t('app', 'Agregar Categoría');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categorias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categorias-create">

    <?= $this->render('_form', [
		'model'                 => $model,
		'categoriasPrincipales' => $categoriasPrincipales,
    ]) ?>

</div>
