<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\menus\categorias\models\Categorias */

$this->title = Yii::t('app', 'Editar Categoría: {name}', [
    'name' => $model->nombre,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categorias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->categoria_id, 'url' => ['view', 'id' => $model->categoria_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="categorias-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
		'model'                 => $model,
		'categoriasPrincipales' => $categoriasPrincipales,
    ]) ?>

</div>
