<?php

namespace backend\modules\menus\categorias\controllers;

use Yii;
use backend\modules\menus\categorias\models\Categorias;
use backend\modules\menus\categorias\models\CategoriasSearch;
use backend\modules\menus\categorias\models\Categoriasidiomas;
use backend\models\Empresa;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;


/**
 * Default controller for the `categorias` module
 */
class DefaultController extends Controller
{
	
	 /**
     * {@inheritdoc}
     */
	public function behaviors()
    {
        return [
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index','lists','view','create','update','delete'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'lists' => ['GET','POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Categorias models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('generalCategorias')){
            $searchModel             = new CategoriasSearch();
            $searchModel->empresa_id = Yii::$app->user->identity->empresa_id;
            $searchModel->cliente_id = Yii::$app->user->identity->cliente_id;
            $dataProvider            = $searchModel->search(Yii::$app->request->queryParams);   

            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
            return $this->render('index', [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
                'idiomas'      => $idiomas
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }

    /**
     * Displays a single Categorias model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('generalCategorias')){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    public function levelCategories(){
        $model = new Categorias();
        $model->empresa_id = Yii::$app->user->identity->empresa_id;
        $model->cliente_id = Yii::$app->user->identity->cliente_id;

        /*** Levels Categories ***/
        return $categoriasPrincipales = $model::find()
                                            ->where(['empresa_id'=>$model->empresa_id])
                                            ->andWhere(['cliente_id'=>$model->cliente_id])
                                            ->andWhere(['categoriaPadre_id'=>NULL])
                                            ->all();
    }//end function

    /**
     * Creates a new Categorias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('generalCategorias')){
            $model                 = new Categorias();
            $model->empresa_id     = Yii::$app->user->identity->empresa_id;
            $model->cliente_id     = Yii::$app->user->identity->cliente_id;
            $categoriasPrincipales = $this->levelCategories();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', "Se registro correctamente la categoría :  <strong>".$model->nombre."</strong>");
                return $this->redirect(['index']);
                //return $this->redirect(['view', 'id' => $model->categoria_id]);
            }
            return $this->renderAjax('create', [
                'model'                 => $model,
                'categoriasPrincipales' => $categoriasPrincipales,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Updates an existing Categorias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('generalCategorias')){
            $model                 = $this->findModel($id);
            $categoriasPrincipales = $this->levelCategories();
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', "Se actualizo correctamente la categoría :  <strong>".$model->nombre."</strong>");
                return $this->redirect(['index']);
            }

            return $this->renderAjax('update', [
                'model'                 => $model,
                'categoriasPrincipales' => $categoriasPrincipales,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Deletes an existing Categorias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('generalCategorias')){
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Finds the Categorias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categorias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categorias::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionLists($id){
        if(Yii::$app->user->can('generalCategorias')){
            if(!empty($id)){
                $countSubcat = Categorias::find()
                                    ->where(['categoriaPadre_id' => $id])
                                    ->count();

                if($countSubcat > 0){
                    $subcategorias = Categorias::find()
                                    ->where(['categoriaPadre_id' => $id])
                                    ->all();
                    echo "<option value='all'>Todos </option>";
                    foreach ($subcategorias as $subcategoria) {
                        echo "<option value='".$subcategoria->categoria_id."'>".$subcategoria->nombre." </option>";
                    }//end foreach
                }else{
                     echo "<option value='all'>Todos</option>";
                }//end if
            }//end if
        }else{
            throw new ForbiddenHttpException;
        }
    }//end function

    public function actionCategorylanguage($id_idioma,$id_categoria){
        if(Yii::$app->user->can('interIdiomas') || Yii::$app->user->can('premiumIdiomas')){
            /*$modelCategoriaIdioma               = new Categoriasidiomas;
            $modelCategoriaIdioma->idioma_id    = $id_idioma;
            $modelCategoriaIdioma->categoria_id = $id_categoria;*/
            $model = Categoriasidiomas::find()->where(['idioma_id'=>$id_idioma,'categoria_id'=>$id_categoria])->one();
            if(empty($model)){
                $model               = new Categoriasidiomas;
                $model->idioma_id    = $id_idioma;
                $model->categoria_id = $id_categoria;
            }//end if

            return  $this->renderAjax('_formcategoria', [
                        'model'                 => $model,
                    ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    public function actionAddcategorylanguage(){
        $categoriaIdioma_id = Yii::$app->request->post()["Categoriasidiomas"]["categoriaIdioma_id"];
        if(!empty($categoriaIdioma_id)){
            $model = Categoriasidiomas::find()->where(['categoriaIdioma_id'=>$categoriaIdioma_id])->one();
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', "Se actulizo la información correctamente.");
                return $this->redirect(['index']);
            }
        }else{
            $model = new Categoriasidiomas();
            Yii::$app->session->setFlash('success', "Se guardo la información correctamente.");
            if($model->load(Yii::$app->request->post()) && $model->save()){
                return $this->redirect(['index']);
            }//end if
        };
    }//end function

}
