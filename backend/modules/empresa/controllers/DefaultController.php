<?php

namespace backend\modules\empresa\controllers;

use Yii;
use backend\models\Empresa;
use backend\models\Idiomas;


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\widgets\ActiveForm;
use yii\web\response;
use Da\QrCode\QrCode; //QrCode Generator
use yii\web\UploadedFile;

/**
 * Default controller for the `empresa` module
 */
class DefaultController extends Controller
{
     /**
     * {@inheritdoc}
     */
    public function behaviors(){
        return [
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index','edit','createcode','idiomas'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'edit'  => ['GET','POST'],
                    'createcode' => ['POST'],
                    'idiomas'=> ['GET','POST']
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex(){
        if(Yii::$app->user->can('basicEmpresa')){
            $model = $this->findModel(Yii::$app->user->identity->empresa_id);
            return $this->render('index',['model' => $model]);
        }else{
            throw new ForbiddenHttpException;
        }
    }//end function

    public function actionEdit(){
        if(Yii::$app->user->can('basicEmpresa')){
            $model           = $this->findModel(Yii::$app->user->identity->empresa_id);
            $model->scenario = "empresascenario";
            if($model->load(Yii::$app->request->post())){
                 //get the instance of the upload file
                $model->fileImage = UploadedFile::getInstance($model,'fileImage');
                if(!empty($model->fileImage)){
                    //$imageName             = $model->fileImage->name.date('Ymd_His');
                    $imageName = $model->empresa_id."-".$model->cliente_id.$model->fileImage->name;
                    $model->fileImage->saveAs('uploads/empresa/'.$imageName);
                    $model->logo = 'uploads/empresa/'.$imageName;    
                }//end if


                $diasHabiles = implode(',',Yii::$app->request->post('Empresa')["diasHabiles"]);
                $model->diasHabiles = $diasHabiles;
                $model->save(false);

                Yii::$app->session->setFlash('success', "Lo datos de empresa se guardaron correctamente.");
                return $this->redirect(['index',['model' => $model]]);
            }//end if
            return $this->render('edit',['model' => $model]);
        }else{
            throw new ForbiddenHttpException;   
        }
    }//end function

    public function actionSlug(){
        if(Yii::$app->user->can('basicEmpresa')){
            $model = $this->findModel(Yii::$app->user->identity->empresa_id);
            $model->scenario="slugscenario";
            if($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if($model->load(Yii::$app->request->post())){
                if($model->validate()){
                    $response = true;
                    return $this->render('slug',['model' => $model,'response'=>$response]);
                }else{
                    $model->getErrors();
                }//end if
            }//end if

            return $this->render('slug',['model' => $model,'response'=>false]);
        }else{
            throw new ForbiddenHttpException;      
        }
    }//end function

    public function actionCreatecode(){
        if(Yii::$app->user->can('basicEmpresa')){
            $model           = $this->findModel(Yii::$app->user->identity->empresa_id);
            $model->scenario = "slugscenario";

            if($model->load(Yii::$app->request->post())){
                if($model->save()){
                    /******* Aquí hay que generar la url del slug *****/
                    $qrCode = (new QrCode($model->slug))
                        ->setSize(500)
                        ->setMargin(5)
                        //->useForegroundColor(51, 153, 255);
                        ->useForegroundColor(0, 0, 0);


                    $nameqrbyempresa = str_replace(" ","_",$model->nombre)."_".$model->empresa_id."_".$model->cliente_id;
                    $qrCode->writeFile(Yii::$app->basePath. '/modules/empresa/codesqr/'.$nameqrbyempresa.'.png'); // writer defaults to PNG when none is specified

                    /*** Guardar uri de la imagen generada para retornal en la vista***/
                    $model->scenario = "empresascenario";
                    $model->qrimage = $qrCode->writeDataUri();
                    $model->save();

                    return $this->redirect(['slug',['model' => $model]]);
                    // display directly to the browser 
                    //header('Content-Type: '.$qrCode->getContentType());
                    //echo $qrCode->writeString();
                    /*echo '<img src="' . $qrCode->writeDataUri() . '">';
                    die();*/
                }//end if
            }//end if
        }else{
            throw new ForbiddenHttpException;
        }
    }//end function

    public function actionDownloadqr($empresa_id = NULL){
        $model    = $this->findModel($empresa_id);
        $qrImage  = $model->qrimage;
        $img      = str_replace('data:image/png;base64,', '', $qrImage);
        $fileData = base64_decode($img);
        header("Content-type: image/png");
        header("Content-Disposition: attachment; filename=codeqr.png");
        ob_clean();
        flush();
        echo $fileData;
    }//end function

    public function actionIdiomas(){
        if(Yii::$app->user->can('interIdiomas') || Yii::$app->user->can('premiumIdiomas')){
            $model   = $this->findModel(Yii::$app->user->identity->empresa_id);
            $idiomas = Idiomas::find()->where(['estatus'=>'activo'])->all();

            $model->scenario = "idiomascenario";
            if(!empty(Yii::$app->request->post())){
                $model->idiomas = Yii::$app->request->post()["idiomas"];
                $model->save();

                if(empty($model->idiomas)){
                    Yii::$app->session->setFlash('success', "Se guardaron los cambios exitosamente.");
                }else{
                    Yii::$app->session->setFlash('success', "La activación de idiomas se generó exitosamente.");
                }//end if
            }//end if

            return $this->render('idiomas',['model' => $model,'idiomas'=>$idiomas]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function


    protected function findModel($id)
    {
        if (($model = Empresa::findOne($id)) !== null) {
            return $model;
        }//end if
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
