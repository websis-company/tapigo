<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;

use backend\models\Idiomas;
/* @var $this yii\web\View */
/* @var $model backend\models\Empresa */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Actualizando información de Mi Empresa';
$this->params['breadcrumbs'][] = $this->title;

?>

<style type="text/css" media="screen">
	.field-empresa-pais, .field-empresa-slug, .field-empresa-cliente_id{
		display: none;
	}
</style>
<div class="row-fluid">
	<div class="col-sm-12 col-md-8 col-lg-8 offset-md-2 col-lg-2">
		<div class="card card-primary text-dark">
			<div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-store-alt"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>"miempresaForm"]]); ?>
			<div class="empresa-form card-body">
				<div class="row mt-2">
					<div class="col-12">
						<div class="card ">
							<div class="card-header bg-dark text-white ">
			                    <h6><strong> Datos Generales</strong></h6>
			                </div>
			                <div class="card-body">
							    <?= $form->field($model, 'nombre',['options'=>['class'=>'col-12','style'=>'float: left;']])->textInput(['maxlength' => true])->label('Nombre de la empresa') ?>
							    <?= $form->field($model, 'descripcion',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]) ?>

							    <?php 
							    /** Muestra opciones para guardar descripciones en diferentes idiomas **/
							    if(!empty($model->idiomas)){
							    	$idiomas = explode(",",$model->idiomas);
							    	foreach ($idiomas as $id_idioma) {
							    		$modelIdiomas = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
							    		if($id_idioma == 1){//Inglés
							    			echo $form->field($model, 'descripcionEn',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]);
							    		}//end if

							    		if($id_idioma == 2){//Alemán
							    			echo $form->field($model, 'descripcionDe',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]);
							    		}//end if

							    		if($id_idioma == 3){//Francés
							    			echo $form->field($model, 'descripcionFr',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]);
							    		}//end if

							    		if($id_idioma == 4){//Italiano
							    			echo $form->field($model, 'descripcionIt',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]);
							    		}//end if

							    		if($id_idioma == 5){//Portugués
							    			echo $form->field($model, 'descripcionPr',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]);
							    		}//end if
							    	}//end foreach
							    }//end if
							    ?>

							    <div class="clearfix"></div>
							    <!-- <?//= $form->field($model, 'horaApertura',['options'=>['class'=>'col-md-4 col-sm-12','style'=>'float: left;']])->textInput() ?> -->
							    <?php 
							    	//$horaApertura =  date("g:i a",strtotime($model->horaApertura));
							    	echo $form->field($model, 'horaApertura',['options'=>['class'=>'col-md-4 col-sm-12 mt-3','style'=>'float: left;']])->widget(TimePicker::classname());
							    ?>
							    <?php 
							    	//$horaCierre = date("g:i a",strtotime($model->horaCierre));
							    	echo $form->field($model, 'horaCierre',['options'=>['class'=>'col-md-4 col-sm-12 mt-3','style'=>'float: left;']])->widget(TimePicker::classname());?>
							    <!-- <?//= $form->field($model, 'horaCierre',['options'=>['class'=>'col-md-4 col-sm-12','style'=>'float: left;']])->textInput() ?> -->
							    <?php 
							    	$data = ["L"=>"Lunes","MA"=>"Martes","MI"=>"Miercoles","J"=>"Jueves","V"=>"Viernes","S"=>"Sábado","D"=>"Domingo"];
							    	if(!empty($model->diasHabiles)){
							    		$value_data = explode(',',$model->diasHabiles);
							    	}else{
							    		$value_data = "";
							    	}//end if

							    	echo $form->field($model, 'diasHabiles[]',['options'=>['class'=>'col-md-4 col-sm-12 mt-3','style'=>'float: left;']])
							    				->widget(Select2::classname(), [
							    					'data' => $data,
							    					'language' => 'es',
							    					'options' => [
				                                        'value' => $value_data,//Initial values or selected values
				                                        'placeholder' => 'Seleccione los días laborales',
				                                        'id'=>'diasHabiles',
				                                    ],
				                                    'pluginOptions' => [
				                                        'tags' => true,
				                                        'allowClear' => true,
				                                        'multiple' => true,
				                                    ],
				                            	]);
							    ?>
							    <div class="clearfix"></div>

							    <?= $form->field($model, 'fileImage',['options'=>['class'=>'col-sm-12 mt-3 bg-light']])->fileInput()->label('<div>Logo</div> <div class=" alert-primary" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 500px X 500px (Ancho x Alto)</small></div>'); ?>
							    <div class="clearfix"></div>				

							</div>
						</div>
					</div>
				</div>

				<div class="row mt-2">
					<div class="col-12">
						<div class="card">
							<div class="card-header bg-dark text-white ">
			                    <h6><strong> Dirección</strong></h6>
			                </div>
			    			<div class="card-body">
							    <?= $form->field($model, 'calle',['options'=>['class'=>'col-md-3 col-sm-12 ','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <?= $form->field($model, 'noExterior',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <?= $form->field($model, 'noInterior',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <?= $form->field($model, 'colonia',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <?= $form->field($model, 'codigoPostal',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <?= $form->field($model, 'estado',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <?= $form->field($model, 'ciudad',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <?= $form->field($model, 'telefono',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="row mt-2">
					<div class="col-12">
						<div class="card">
							<div class="card-header bg-dark text-white ">
			                    <h6><strong> Redes Sociales</strong></h6>
			                </div>
			    			<div class="card-body">
							    <?= $form->field($model, 'facebook',['options'=>['class'=>'col-md-6 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <?= $form->field($model, 'Instagram',['options'=>['class'=>'col-md-6 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <div class="clearfix"></div>
							    <?= $form->field($model, 'twitter',['options'=>['class'=>'col-md-6 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <?= $form->field($model, 'whatsapp',['options'=>['class'=>'col-md-6 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
							    <div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>

				<?= $form->field($model, 'pais')->hiddenInput(['maxlength' => true])->label('') ?>
				<?= $form->field($model, 'slug')->hiddenInput(['maxlength' => true])->label('') ?>
				<?= $form->field($model, 'cliente_id')->hiddenInput()->label('') ?>
				<?= $form->field($model, 'estatus')->hiddenInput(['value' => 'Activo'])->label(''); ?>
			</div>
			<div class=" card-footer" align="center">
				
				<?= Html::a('<i class="fas fa-times-circle"></i> Cancelar', $url = Url::to(['/miempresa/default/index']), ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm',]); ?>
                <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
            </div> 
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>
