<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Mi Empresa | Idiomas';
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="row-fluid">
	<div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 col-lg-3">
		<div class="card card-primary text-dark">
			<div class="card-header">
				<div class="col-12">
                	<h1 class="card-title">
                		<strong><i class="nav-icon fas fa-globe-americas"></i>&nbsp;&nbsp;&nbsp;Activar Idiomas </strong>
                	</h1>
            	</div>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'categoriasForm']]); ?>
            <div class="idiomas-info card-body">
            	<?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="row-fluid" align="center">
                        <div class="col-sm-12">
                            <div class="alert bg-teal alert-dismissable">
                               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                               <i class="icon fa fa-check"></i> <?= Yii::$app->session->getFlash('success') ?>
                           </div>
                        </div>
                    </div>
               <?php endif; ?>

            	<div class="row-fluid">
            		<div id="idiomaserror" class="alert bg-maroon alert-dismissable text-center" style="display: none;">
            			<i class="fas fa-exclamation-circle"></i>&nbsp;&nbsp; Lo sentimos pero no puede activar más idiomas.
            		</div>
            	</div>
            	<div class="clearfix"></div>

            	<div class="row-fluid">
	            	<?php 
	            	foreach ($idiomas as $idioma) {
						$idIdioma  = $idioma->idioma_id;
						$idiomanom = $idioma->nombre;

						$selectedIdiomas = explode(",",$model->idiomas);
						$checked = "";
						if(in_array($idIdioma, $selectedIdiomas)){
							$checked = "checked";
						}else{
							$checked = "";
						}//end if

	            	?>
	            	<div class="col-6 col-sm-6 col-md-4 float-left mt-2 mb-2">
	            		<div class="form-group">
	            			<div class="custom-control custom-switch custom-switch-off custom-switch-on-teal">
	            				<input type="checkbox" class="custom-control-input" id="customSwitch<?php echo $idIdioma?>" value="<?php echo $idIdioma; ?>" <?php echo $checked; ?>>
	            				<label class="custom-control-label" for="customSwitch<?php echo $idIdioma?>"><?php echo $idiomanom; ?></label>
	            			</div>
	            		</div>
	            	</div>
	            	<?php
	            	}//foreach
	            	?>
            	</div>
            </div>
            <div class="card-footer" align="center">
            	<?= Html::input('hidden', $name = 'idiomas', $value = $model->idiomas, ['id' => 'idiomasSelected']); ?>
            	<?= Html::input('hidden', $name = 'empresa_id', $value = $model->empresa_id, []); ?>
            	<?= Html::a('<i class="fas fa-times-circle"></i> Cancelar', $url = Url::to(['/site/index']), ['class' => 'btn btn-outline-danger']); ?>
            	<?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
            </div>
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php 
if(Yii::$app->user->can('interIdiomas')){
	$total_idiomas = 2;
}elseif(Yii::$app->user->can('premiumIdiomas')){
	$total_idiomas = 5;
}//end if

$script = <<< JS
	$(function(e){
		$(".custom-control-input").on("click",function(e){
			var selected = $("#idiomasSelected").val();
			var array    = selected.split(",");

			if(selected == ""){
				$("#idiomasSelected").val(this.value);
			}else{
				var index = array.indexOf(this.value);
				if(index == -1){
					if(array.length == {$total_idiomas}){
						$("#idiomaserror").show();
						return false;
					}else{
						$("#idiomaserror").hide();
					}//end if

					$("#idiomasSelected").val(selected+","+this.value);
				}else{
					$("#idiomaserror").hide();

					new_array = [];
					for(let i = 0; i < array.length; i++) {
						if(this.value != array[i]){
							new_array.push(array[i]);
						}//end if
					}//end for

					var new_selected = new_array.join(',');
					$("#idiomasSelected").val(new_selected);
				}//end if
			}//end if
			
		});
	});
JS;
$this->registerJs($script);
?>
