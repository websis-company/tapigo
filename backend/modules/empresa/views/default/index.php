<?php 
use yii\helpers\Html;
use yii\helpers\Url;

use backend\models\Idiomas;

$this->title = 'Mi Empresa | Información General';
$this->params['breadcrumbs'][] = $this->title;


echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox',
]);
?>


<div class="row-fluid">
	<div class="col-sm-12 col-md-8 col-lg-8 offset-md-2 col-lg-2">
		<div class="card card-primary text-dark">
			<div class="card-header">
				<div class="col-12">
					<div class="col-sm-12 col-md-6">
	                	<h1 class="card-title">
	                		<strong><i class="nav-icon fas fa-fas fa-store-alt"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?> </strong>
	                	</h1>
	                </div>
	                <div class="col-sm-12 col-md-6 float-md-right">
	                	<?= Html::a('<i class="fas fa-edit"></i>', $url = Url::to(['/miempresa/default/edit']), ['class' => 'text-white btn bg-teal col-sm-12 col-md-1 float-sm-right','title'=>'Actualizar información']); ?>
	            	</div>
            	</div>
            </div>
            <div class="miempresa-info card-body">
            	<?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="row-fluid" align="center">
                        <div class="col-sm-12">
                            <div class="alert bg-teal alert-dismissable">
                               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                               <i class="icon fa fa-check"></i> <?= Yii::$app->session->getFlash('success') ?>
                           </div>
                        </div>
                    </div>
               	<?php endif; ?>
               	
            	<div class="row mt-2">
					<div class="col-12">
						<div class="card ">
							<div class="card-header bg-gradient-dark ">
			                    <h6><strong> Datos Generales</strong></h6>
			                </div>
			                <div class="card-body">
			                	<div class="col-12 bg-gradient-dark">
			                		<strong>Logo Empresa</strong>
			                		<p align="center">
			                			<?= Html::a(Html::img(Url::base()."/".$model->logo, ['height'=>'150','style'=>'margin: 15px;']), $url = Url::base()."/".$model->logo, ['title'=>'Ver Imagen','class' => 'data-fancybox']); ?>
			                		</p>
			                	</div>
			                	<div class="col-12">
				                	<strong>Nombre</strong>
				                	<p>
				                		<?php echo $model->nombre; ?>
				                	</p>
			                	</div>
			                	<div class="col-12">
			                		<strong>Descripción</strong>
				                	<p>
				                		<?php echo nl2br($model->descripcion); ?>
				                	</p>
			                	</div>

			                	<?php 
			                	if(!empty($model->idiomas)){
			                		$idiomas = explode(",",$model->idiomas);
			                		foreach ($idiomas as $id_idioma) {
			                			$modelIdiomas = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
			                			if($id_idioma == 1){//Inglés
			                				?>
			                				<div class="col-12">
			                					<strong> Descripción en <?php echo $modelIdiomas->nombre; ?></strong>
			                					<p>
			                						<?php echo nl2br($model->descripcionEn); ?>
			                					</p>
			                				</div>
			                				<?php
			                			}//end if

			                			if($id_idioma == 2){//Alemán
			                				?>
			                				<div class="col-12">
			                					<strong>Descripción en <?php echo $modelIdiomas->nombre; ?></strong>
			                					<p>
			                						<?php echo nl2br($model->descripcionDe); ?>
			                					</p>
			                				</div>
			                				<?php
			                			}//end if

			                			if($id_idioma == 3){//Francés
			                				?>
			                				<div class="col-12">
			                					<strong>Descripción en <?php echo $modelIdiomas->nombre; ?></strong>
			                					<p>
			                						<?php echo nl2br($model->descripcionFr); ?>
			                					</p>
			                				</div>
			                				<?php
			                			}//end if

			                			if($id_idioma == 4){//Italiano
			                				?>
			                				<div class="col-12">
			                					<strong>Descripción en <?php echo $modelIdiomas->nombre; ?></strong>
			                					<p>
			                						<?php echo nl2br($model->descripcionIt); ?>
			                					</p>
			                				</div>
			                				<?php
			                			}//end if

			                			if($id_idioma == 5){//Portugués
			                				?>
			                				<div class="col-12">
			                					<strong>Descripción en <?php echo $modelIdiomas->nombre; ?></strong>
			                					<p>
			                						<?php echo nl2br($model->descripcionPr); ?>
			                					</p>
			                				</div>
			                				<?php
			                			}//end if

			                		}//end foreach
			                	}//end if
			                	?>

							    <div class="clearfix"></div>
							    <div class="col-md-4 col-sm-12" style="float: left;">
				                	<strong>Hora Apertura</strong>
				                	<p>
				                		<?php echo $model->horaApertura; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-4 col-sm-12" style="float: left;">
				                	<strong>Hora Cierra</strong>
				                	<p>
				                		<?php echo $model->horaCierre; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-4 col-sm-12" style="float: left;">
				                	<strong>Días Hábiles</strong>
				                	<?php 
				                	$data = ["L"=>"Lunes","MA"=>"Martes","MI"=>"Miercoles","J"=>"Jueves","V"=>"Viernes","S"=>"Sábado","D"=>"Domingo"];
				                	if(!is_null($model->diasHabiles)){
					                	$dias = explode(',', $model->diasHabiles);
					                	$arr_dias = [];
					                	foreach ($dias as $dia) {
					                		$arr_dias[] = $data[$dia];
					                	}//end foreach
					                	$str_dias = implode(',',$arr_dias);
				                	}else{
				                		$str_dias = "";
				                	}//end if

				                	?>
				                	<p>
				                		<?php echo $str_dias; ?>
				                	</p>
			                	</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row mt-2">
					<div class="col-12">
						<div class="card">
							<div class="card-header bg-gradient-dark ">
			                    <h6><strong> Dirección</strong></h6>
			                </div>
			                <div class="card-body">
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>Calle</strong>
				                	<p>
				                		<?php echo $model->calle; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>No. Exterior</strong>
				                	<p>
				                		<?php echo $model->noExterior; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>No. Interior</strong>
				                	<p>
				                		<?php echo $model->noInterior; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>Colonia</strong>
				                	<p>
				                		<?php echo $model->colonia; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>Código Postal</strong>
				                	<p>
				                		<?php echo $model->codigoPostal; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>Estado</strong>
				                	<p>
				                		<?php echo $model->estado; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>Ciudad</strong>
				                	<p>
				                		<?php echo $model->ciudad; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>Teléfono</strong>
				                	<p>
				                		<?php echo $model->telefono; ?>
				                	</p>
			                	</div>
			                </div>
			            </div>
			        </div>
			    </div>

			    <div class="row mt-2">
					<div class="col-12">
						<div class="card">
							<div class="card-header bg-gradient-dark ">
			                    <h6><strong> Redes Sociales</strong></h6>
			                </div>
			    			<div class="card-body">
							    <div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>Facebook</strong>
				                	<p>
				                		<?php echo $model->facebook; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>Instagram</strong>
				                	<p>
				                		<?php echo $model->Instagram; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>Twitter</strong>
				                	<p>
				                		<?php echo $model->twitter; ?>
				                	</p>
			                	</div>
			                	<div class="col-md-3 col-sm-12" style="float: left;">
				                	<strong>Whatsapp</strong>
				                	<p>
				                		<?php echo $model->whatsapp; ?>
				                	</p>
			                	</div>
							    <div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer" align="center">
				<?= Html::a('<i class="fas fa-times-circle"></i> Cancelar', $url = Url::to(['/site/index']), ['class' => 'btn btn-outline-danger','title'=>'Cancelar']); ?>
				&nbsp;&nbsp;&nbsp;
				<?= Html::a('<i class="fas fa-edit"></i> Editar Información', $url = Url::to(['/miempresa/default/edit']), ['class' => 'btn btn-outline-success','title'=>'Editar Información']); ?>
			</div>
        </div>
    </div>
</div>