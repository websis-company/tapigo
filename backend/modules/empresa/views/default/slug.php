<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Mi Empresa | Slug QR';
$this->params['breadcrumbs'][] = $this->title;
echo newerton\fancybox3\FancyBox::widget([
    'target' => '.fancybox-img',
]);
?>

<style type="text/css" media="screen">
	.invalid-feedback{
		display: block;
	}	
</style>
<div class="row-fluid">
	<div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 col-lg-3">
		<div class="card card-primary text-dark">
			<div class="card-header">
				<div class="col-12">
                	<h1 class="card-title">
                		<strong><i class="nav-icon fas fa-fas fa-store-alt"></i>&nbsp;&nbsp;&nbsp;Información sobre <?= Html::encode($this->title) ?> </strong>
                	</h1>
            	</div>
            </div>
            <div class="miempresa-info card-body">
            	<div class="row mt-2">
	                <?php $form = ActiveForm::begin([
	                		'options'=>[
								'enctype'                =>'multipart/form-data',
								'method'                 =>'post',
								'enableClientValidation' =>false,
								'enableAjaxValidation'   =>true,
								'id'                     =>"miempresaForm",
								'class'                  => 'col-sm-12'
	                		]
	                	]); ?>
	                	
	                	<?php
	                		if(!empty($model->qrimage)){
	                			?>
	                			<div align="center">
		                			<div class="col-12">
		                				<div class="alert alert-light">
		                					El <strong>Slug</strong> es el identificador único con el cual tus clientes podrán acceder a tu menú digital desde internet. <br>
		                					A continuación se muestra el link y código QR con el cual podras visualizar tu menú.
		                				</div>
		                				<div class="alert alert-light">
		                					<a href="" class="text-dark">https://menu.tapigo.com.mx/<?php echo $model->slug; ?></a>
		                					<!-- <div>Escanea el código con tu celular.</div> -->
		                				</div>
		                				<div class="row-fluid">
		                					<!-- <?//= Html::a($text, $url = null, ['option' => 'value']); ?> -->
		                					<!-- <img src="<?php echo $model->qrimage; ?>" style="width: 200px;"> -->
		                					<?= Html::a(Html::img($model->qrimage, ['style' => 'width: 200px;']),$url=$model->qrimage, ['class' => 'fancybox-img']); ?>
		                					</br></br>
		                					<?= Html::a('<i class="fas fa-download"></i> Descargar Código', $url = Url::to(['/miempresa/default/downloadqr','empresa_id'=>$model->empresa_id]), ['class' => 'btn btn-outline-primary']); ?>
		                				</div>
		                			</div>
	                			</div>
	                			<?php
	                		}else{
	                		?>
	                			<div class="col-12">
				                	<div class="alert alert-danger" align="center">
				                		IMPORTANTE.<br>
				                		El <strong>Slug</strong> es el identificador único con el cual tus clientes podrán acceder a tu menú digital desde internet. 
				                		Una vez asignado el <strong>slug</strong> se creará el código QR y no podra ser cambiado.
				                		<br><br>
				                		<div align="left">
				                			<small align="left">* El slug no debe contener carácteres especiales como: (@,%,&,etc.), solo número y letras.</small>
				                		</div>
				                	</div>
			                	</div>
			                <?php
			                	echo $form->field($model, 'slug',['enableAjaxValidation' => true,'options'=>['class'=>'input-group col-12']])->textInput(['class'=>'form-control col-12','style'=>'','placeholder'=>'Slug','maxlength' => true])->label('<span class="input-group-append">
		                			<button type="submit" class="btn btn-outline-primary btn-flat">
		                				<i class="far fa-check-circle"></i> &nbsp;&nbsp;Verificra Slug
		                			</button>
		                		</span>'); 
		                	}
	                	?>

					<?php ActiveForm::end(); ?>
				</div>
				

				<?php 
				if($response == true){
				?>
				<div class="row mt-2" align="center">
					<div class="col-sm-12">
					 	<?php $form2 = ActiveForm::begin([
					 		'action' =>['default/createcode'],
	                		'options'=>[
								'enctype' =>'multipart/form-data',
								'method'  =>'post',
								'id'      =>"slugForm",
	                		]
	                	]); ?>
	                	<div class="col-12">
	                		<?= $form2->field($model, 'slug')->hiddenInput()->label(''); ?>
	                		<div class="alert bg-teal">
	                			<i class="far fa-thumbs-up"></i> &nbsp;El Slug se encuentra disponible, ahora puede generar el código QR.
	                		</div>
	                		<?= Html::submitButton('<i class="nav-icon fas fa-fas fa-qrcode"></i> <br> Guardar y Generar QR', ['class' => 'btn btn-outline-primary']) ?>
	                	</div>
		                <?php ActiveForm::end(); ?>
		            </div>
				</div>
				<?php
				}//end if
				?>
			</div>
		</div>
    </div>
</div>


<?php 
	if(count($model->getErrors()) > 0){
		$errors = json_encode($model->getErrors());
	}else{
		$errors = "";
	}//end if

$script = <<< JS
	$(function(e){
		$(".help-block").addClass("invalid-feedback");
		var errors = '{$errors}';
		if(errors === ""){
			console.log("errors empty");
			return false;
		}else{
			var jsonObj = JSON.parse(errors);
			var slug = '{$model->slug}';
			if(slug === ""){
				console.log("entra");
				return false;
			}else{
				$(".help-block").html(jsonObj.{$model->slug});
			}//end if
		}//end if
	});
JS;
$this->registerJs($script);
?>