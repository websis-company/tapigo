<?php 
use yii\helpers\Html;

$this->title = 'Mi Empresa';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="miempresa-edit">
	<?= $this->render('_form', ['model' => $model]); ?>
</div>