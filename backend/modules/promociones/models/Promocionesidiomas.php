<?php

namespace backend\modules\promociones\models;

use Yii;
use backend\models\Idiomas;
use backend\modules\promociones\models\Promociones;

/**
 * This is the model class for table "promocionesidiomas".
 *
 * @property int $promocionIdiomas_id
 * @property int $idioma_id
 * @property int $promocion_id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $imagenPrincipal
 * @property string|null $imagenSlider1
 * @property string|null $imagenSlider2
 * @property string|null $imagenSlider3
 * @property string|null $imagenSlider4
 * @property string|null $imagenSlider5
 *
 * @property Promociones $promocion
 * @property Idiomas $idioma
 */
class Promocionesidiomas extends \yii\db\ActiveRecord
{
    public $fileMainImage;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promocionesidiomas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idioma_id', 'promocion_id','nombre','descripcion'], 'required'],
            [['idioma_id', 'promocion_id'], 'integer'],
            [['descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 255],
            [['imagenPrincipal', 'imagenSlider1', 'imagenSlider2', 'imagenSlider3', 'imagenSlider4', 'imagenSlider5'], 'string', 'max' => 200],
            [['fileMainImage'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 250,'maxWidth'=>1000,'minHeight'=>250,'maxHeight'=>1000,'maxSize'=>1024 * 1024 * 2],
            [['promocion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Promociones::className(), 'targetAttribute' => ['promocion_id' => 'promocion_id']],
            [['idioma_id'], 'exist', 'skipOnError' => true, 'targetClass' => Idiomas::className(), 'targetAttribute' => ['idioma_id' => 'idioma_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'promocionIdiomas_id' => 'Promocion Idiomas ID',
            'idioma_id' => 'Idioma ID',
            'promocion_id' => 'Promocion ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'imagenPrincipal' => 'Imagen Principal',
            'fileMainImage'   => 'Imagen Principal',
            'imagenSlider1' => 'Imagen Slider1',
            'imagenSlider2' => 'Imagen Slider2',
            'imagenSlider3' => 'Imagen Slider3',
            'imagenSlider4' => 'Imagen Slider4',
            'imagenSlider5' => 'Imagen Slider5',
        ];
    }

    /**
     * Gets query for [[Promocion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPromocion()
    {
        return $this->hasOne(Promociones::className(), ['promocion_id' => 'promocion_id']);
    }

    /**
     * Gets query for [[Idioma]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdioma()
    {
        return $this->hasOne(Idiomas::className(), ['idioma_id' => 'idioma_id']);
    }
}
