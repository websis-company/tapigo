<?php

namespace backend\modules\promociones\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\promociones\models\Promociones;

/**
 * PromocionesSearch represents the model behind the search form of `backend\modules\promociones\models\Promociones`.
 */
class PromocionesSearch extends Promociones
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promocion_id', 'cliente_id','orden'], 'integer'],
            [['nombre', 'descripcion','estatus'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Promociones::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'promocion_id' => $this->promocion_id,
            'cliente_id' => $this->cliente_id,
            'empresa_id' => $this->empresa_id,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(["=", 'orden', $this->orden])
            //->andFilterWhere(['like', 'imagenPrincipal', $this->imagenPrincipal])
            // ->andFilterWhere(['like', 'imagenSlider1', $this->imagenSlider1])
            // ->andFilterWhere(['like', 'imagenSlider2', $this->imagenSlider2])
            // ->andFilterWhere(['like', 'imagenSlider3', $this->imagenSlider3])
            // ->andFilterWhere(['like', 'imagenSlider4', $this->imagenSlider4])
            // ->andFilterWhere(['like', 'imagenSlider5', $this->imagenSlider5])
            ->andFilterWhere(['like', 'estatus', $this->estatus]);

        $query->orderBy(['orden'=>SORT_ASC]);

        return $dataProvider;
    }
}
