<?php

namespace backend\modules\promociones\models;

use Yii;
use backend\models\Clientes;
use backend\models\Empresa;

/**
 * This is the model class for table "promociones".
 *
 * @property int $promocion_id
 * @property int $cliente_id
 * @property int $empresa_id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $imagenPrincipal
 * @property string|null $imagenSlider1
 * @property string|null $imagenSlider2
 * @property string|null $imagenSlider3
 * @property string|null $imagenSlider4
 * @property string|null $imagenSlider5
 * @property string|null $estatus
 *
 * @property Clientes $cliente
 * @property Empresa $empresa
 * @property Promocionesidiomas[] $promocionesidiomas
 */
class Promociones extends \yii\db\ActiveRecord
{
    public $fileMainImage;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promociones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_id', 'empresa_id','nombre','estatus','orden'], 'required'],
            [['cliente_id', 'empresa_id','orden'], 'integer'],
            [['descripcion', 'estatus'], 'string'],
            [['nombre'], 'string', 'max' => 255],
            [['imagenPrincipal', 'imagenSlider1', 'imagenSlider2', 'imagenSlider3', 'imagenSlider4', 'imagenSlider5'], 'string', 'max' => 200],
            [['fileMainImage'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 250,'maxWidth'=>1000,'minHeight'=>250,'maxHeight'=>1000,'maxSize'=>1024 * 1024 * 2],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente_id' => 'cliente_id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'empresa_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'promocion_id'    => 'Promocion ID',
            'cliente_id'      => 'Cliente ID',
            'empresa_id'      => 'Empresa ID',
            'nombre'          => 'Nombre',
            'descripcion'     => 'Descripcion',
            'imagenPrincipal' => 'Imagen Principal',
            'fileMainImage'   => 'Imagen Principal',
            'imagenSlider1'   => 'Imagen Slider1',
            'imagenSlider2'   => 'Imagen Slider2',
            'imagenSlider3'   => 'Imagen Slider3',
            'imagenSlider4'   => 'Imagen Slider4',
            'imagenSlider5'   => 'Imagen Slider5',
            'orden'           => 'Orden de Aparición',
            'estatus'         => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Empresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['empresa_id' => 'empresa_id']);
    }

    /**
     * Gets query for [[Promocionesidiomas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPromocionesidiomas()
    {
        return $this->hasMany(Promocionesidiomas::className(), ['promocion_id' => 'promocion_id']);
    }
}
