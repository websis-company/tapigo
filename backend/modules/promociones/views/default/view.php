<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\modules\promociones\models\Promociones */
$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Promociones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox-view',
]);
?>

<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-list-alt"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
            </div>
            <div class="promociones-view card-body">
                <div class="col-12" align="right">
                    <?php echo Html::button('<i class="fas fa-edit"></i>',['value'=>Url::to(['update','id'=>$model->promocion_id]),'class' => 'btn btn-success btnUpdateView']); ?>
                    <?php echo Html::a('<i class="fas fa-trash-alt"></i>', ['delete', 'id' => $model->promocion_id], [
                        'class' => 'btn btn-danger',
                        'title' => 'Eliminar',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
                <div class="col-12 pt-3">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'promocion_id',
                            'cliente_id',
                            'nombre',
                            'descripcion:ntext',
                            [
                                'label'     => 'Imagen Principal',
                                'attribute' => 'imagenPrincipal',
                                'format'    => 'html',
                                'value'     =>function($model){
                                    return yii\bootstrap4\Html::a(yii\bootstrap4\Html::img(Url::base()."/".$model->imagenPrincipal,['height'=>'75']),Url::base()."/".$model->imagenPrincipal,['title'=>'Ver Imagen','class' => 'data-fancybox-view']);
                                }
                            ],
                            // 'imagenSlider1',
                            // 'imagenSlider2',
                            // 'imagenSlider3',
                            // 'imagenSlider4',
                            // 'imagenSlider5',
                            'estatus',
                        ],
                    ]) ?>
                </div>
            </div>
            <div align="center">
                <div class="card-footer">
                    <?= Html::button('Aceptar',['value'=>'','class'=>'btn btn-primary cancelView']) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /*** Action Button Edit View ***/
    $(".btnUpdateView").on("click",function(e){
        $("#divEditForm").hide(function(e){});
        $("#btnAddForm").show(function(e){});
        $("#divEditForm").load($(this).attr('value'),function(e){
            $("#divEditForm").slideDown('slow');
        });
    });

    /*** Action Button Cancel-Close View ***/
    $(".cancelView").on("click",function(e){
         $("#divEditForm").slideUp(function(e){});
    });
</script>
