<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\StringHelper;

use backend\models\Empresa;
use backend\models\Idiomas;


/* @var $this yii\web\View */
/* @var $searchModel backend\modules\promociones\models\PromocionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promociones';
$this->params['breadcrumbs'][] = $this->title;
echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox',
]);

echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox-idiomas',
    'config' => [
        'image' => [      
            // Wait for images to load before displaying
            // Requires predefined image dimensions
            // If 'auto' - will zoom in thumbnail if 'width' and 'height' attributes are found
            'preload'      => "auto",
        ],
        'clickSlide'      => false,
        'clickOutside'    => false,
        'dblclickContent' => false,
        'dblclickSlide'   => false,
        'dblclickOutside' => false,
    ]
]);
?>

<div class="container-fluid">
    <div class="text-center col-12"><div class="loading" style="display: none;"></div></div>
    <div id="divEditForm" class="col-sm-12 col-md-12 col-lg-6 offset-lg-3" style="display: none;"></div>
</div>

<div class="promociones-index">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-secondary card-outline">
                    <div class="card-header">
                         <div class="col-12 text-center"> 
                            <?php echo Html::button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Agregar Promoción', ['value'=>Url::to(['create']),'class' => 'btn bg-gradient-primary float-right','id'=>'btnAddForm']);
                            ?>
                        </div>
                    </div>
                    <?php if (Yii::$app->session->hasFlash('success')): ?>
                        <div class="row-fluid mt-2" align="center">
                            <div class="col-sm-12">
                                <div class="alert bg-teal alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                   <i class="icon fa fa-check"></i> <?= Yii::$app->session->getFlash('success') ?>
                               </div>
                            </div>
                        </div>
                   <?php endif; ?>

                   <div class="card-body pad table-responsive">
                        <?php 
                        /* Validación para mostrar columna idiomas **/
                        if(is_null($idiomas)){
                            $columnIdiomasVisible = false;
                            $arr_idiomas          = [];
                        }else{
                            $arr_idiomas          = explode(',',$idiomas);
                            $columnIdiomasVisible = true;
                        }//end if

                        ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            /*** Show Class in GridView ***/
                            'rowOptions' => function($model){
                                if($model->estatus == "Inactivo"){
                                    return ['class'=>'tableDanger'];
                                }elseif($model->estatus == "Activo"){
                                    return ['class'=>'tableSuccess'];
                                }
                            },
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                //'promocion_id',
                                //'cliente_id',
                                'orden',
                                'nombre',
                                //'descripcion:ntext',
                                [  
                                    'attribute' => 'descripcion',
                                    'format' => 'html',
                                    'value' => function($model){
                                        return StringHelper::truncateWords($model->descripcion,15,'...',false);
                                    }
                                ],
                                //'imagenPrincipal',
                                [
                                    'label'     => 'Imagen Principal',
                                    'attribute' => 'imagenPrincipal',
                                    'format'    => 'html',
                                    'value'     =>function($model){
                                        return yii\bootstrap4\Html::a(yii\bootstrap4\Html::img(Url::base()."/".$model->imagenPrincipal,['height'=>'50']),Url::base()."/".$model->imagenPrincipal,['title'=>'Ver Imagen','class' => 'data-fancybox']);
                                    }

                                ],
                                [
                                    'class'         =>'yii\grid\ActionColumn',
                                    'header'        =>'Idiomas',
                                    'headerOptions' =>['style'=>'text-align:center'],
                                    'template'      =>'{en} {de} {fr} {it} {pr}',
                                    'visible'       =>$columnIdiomasVisible,
                                    'buttons'=>[
                                        'en'=>function($url_,$model,$id_categoria){
                                            $id_idioma    = 1; //constante
                                            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
                                            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
                                            $arr_idiomas  = explode(',',$idiomas);
                                            if (in_array($id_idioma, $arr_idiomas)) {
                                                $modelIdioma = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
                                                return  Html::a(
                                                            Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','width'=>'25']), 
                                                            $url = 'javascript:;', 
                                                            [
                                                                'title'      => $modelIdioma->nombre,
                                                                'class'      => 'data-fancybox-idiomas',
                                                                'data-type'  => 'ajax',
                                                                'data-touch' => 'false',
                                                                'data-src'   => Url::to(['promocioneslanguage','id_idioma'=>$id_idioma,'id_promocion'=>$model->promocion_id])
                                                            ]
                                                        );
                                            }else{
                                                return false;
                                            }//end if
                                        },
                                        'de'=>function($url,$model,$id_categoria){
                                            $id_idioma    = 2; //constante
                                            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
                                            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
                                            $arr_idiomas  = explode(',',$idiomas);
                                            if (in_array($id_idioma, $arr_idiomas)) {
                                                $modelIdioma = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
                                                return  Html::a(
                                                            Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','width'=>'25']), 
                                                            $url = 'javascript:;', 
                                                            [
                                                                'title'      => $modelIdioma->nombre,
                                                                'class'      => 'data-fancybox-idiomas',
                                                                'data-type'  => 'ajax',
                                                                'data-touch' => 'false',
                                                                'data-src'   => Url::to(['promocioneslanguage','id_idioma'=>$id_idioma,'id_promocion'=>$model->promocion_id])
                                                            ]
                                                        );
                                            }else{
                                                return false;
                                            }//end if
                                        },
                                        'fr'=>function($url,$model,$id_categoria){
                                            $id_idioma    = 3; //constante
                                            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
                                            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
                                            $arr_idiomas  = explode(',',$idiomas);
                                            if (in_array($id_idioma, $arr_idiomas)) {
                                                $modelIdioma = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
                                                return  Html::a(
                                                            Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','width'=>'25']), 
                                                            $url = 'javascript:;',
                                                            [
                                                                'title'      => $modelIdioma->nombre,
                                                                'class'      => 'data-fancybox-idiomas',
                                                                'data-type'  => 'ajax',
                                                                'data-touch' => 'false',
                                                                'data-src'   => Url::to(['promocioneslanguage','id_idioma'=>$id_idioma,'id_promocion'=>$model->promocion_id])
                                                            ]
                                                        );
                                            }else{
                                                return false;
                                            }//end if
                                        },
                                        'it'=>function($url,$model,$id_categoria){
                                            $id_idioma    = 4; //constante
                                            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
                                            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
                                            $arr_idiomas  = explode(',',$idiomas);
                                            if (in_array($id_idioma, $arr_idiomas)) {
                                                $modelIdioma = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
                                                return  Html::a(
                                                            Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','width'=>'25']),
                                                            $url = 'javascript:;',
                                                            [
                                                                'title'      => $modelIdioma->nombre,
                                                                'class'      => 'data-fancybox-idiomas',
                                                                'data-type'  => 'ajax',
                                                                'data-touch' => 'false',
                                                                'data-src'   => Url::to(['promocioneslanguage','id_idioma'=>$id_idioma,'id_promocion'=>$model->promocion_id])
                                                            ]
                                                        );
                                            }else{
                                                return false;
                                            }//end if
                                        },
                                        'pr'=>function($url,$model,$id_categoria){
                                            $id_idioma    = 5; //constante
                                            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
                                            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
                                            $arr_idiomas  = explode(',',$idiomas);
                                            if (in_array($id_idioma, $arr_idiomas)) {
                                                $modelIdioma = Idiomas::find()->where(['idioma_id'=>$id_idioma])->one();
                                                return  Html::a(
                                                            Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid','width'=>'25']),
                                                            $url = 'javascript:;',
                                                            [
                                                                'title'      => $modelIdioma->nombre,
                                                                'class'      => 'data-fancybox-idiomas',
                                                                'data-type'  =>'ajax',
                                                                'data-touch' =>'false',
                                                                'data-src'   => Url::to(['promocioneslanguage','id_idioma'=>$id_idioma,'id_promocion'=>$model->promocion_id])
                                                            ]
                                                        );
                                            }else{
                                                return false;
                                            }//end if
                                        },

                                    ]
                                ],
                                [
                                    'attribute' =>'estatus',
                                    'value'=> 'estatus',
                                    'filter' => HTML::activeDropDownList(
                                        $searchModel,
                                        'estatus',
                                        ['Activo'=>'Activo','Inactivo'=>'Inactivo'],
                                        ['class'=>'form-control','prompt'=>'Todos']
                                    ),
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'header'=> 'Acciones',
                                    'headerOptions'=>['style'=>'text-align:center'],
                                    'template'=>'{view} {update} {delete}',
                                    'buttons'=>[
                                        'view'=>function($url,$model){
                                            return Html::button('<span class="glyphicon glyphicon-search"></span>',['value'=>Url::to(['view','id'=>$model->promocion_id]),'class' => 'btn btnViewForm']);
                                        },
                                        'update'=>function ($url, $model) {
                                            return Html::button('<span class="glyphicon glyphicon-pencil"></span>',['value'=>Url::to(['update','id'=>$model->promocion_id]),'class' => 'btn btnUpdateForm']);
                                        },
                                    ]
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
