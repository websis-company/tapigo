<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\promociones\models\Promociones */

$this->title = 'Agregar Promoción';
$this->params['breadcrumbs'][] = ['label' => 'Promociones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promociones-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
