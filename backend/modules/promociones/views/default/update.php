<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\promociones\models\Promociones */

$this->title = 'Editar Promoción: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Promociones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->promocion_id, 'url' => ['view', 'id' => $model->promocion_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promociones-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
