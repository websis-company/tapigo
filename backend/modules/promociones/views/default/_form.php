<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\promociones\models\Promociones */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css" media="screen">
    .field-promociones-empresa_id, .field-promociones-cliente_id{
        display: none;
    }    
</style>
<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-fas fa-percent"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close text-white" onclick='closeForm("promocionesForm")'>×</button>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'promocionesForm']]); ?>
            <div class="promociones-form card-body">
                <?= $form->field($model, 'orden')->textInput() ?>
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>
                
                <!-- <?= $form->field($model, 'imagenPrincipal')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'imagenSlider1')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'imagenSlider2')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'imagenSlider3')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'imagenSlider4')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'imagenSlider5')->textInput(['maxlength' => true]) ?> -->

                <?= $form->field($model, 'fileMainImage',['options'=>['class'=>'col-sm-12 mt-2 bg-light']])->fileInput()->label('<div>Imagen Principal</div> <div class=" alert-primary" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 1000px X 1000px (Ancho x Alto)</small></div>'); ?>

                <?= $form->field($model, 'estatus',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->dropDownList([ 'Activo' => 'Activo', 'Inactivo' => 'Inactivo', ], ['prompt' => 'Seleccine una opción']) ?>

                <?= $form->field($model, 'cliente_id')->hiddenInput()->label(''); ?>
                <?= $form->field($model, 'empresa_id')->hiddenInput()->label(''); ?>
            </div>
            <div class=" card-footer" align="right">
                <?= Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm','onClick'=>'closeForm("promocionesForm")']) ?>
                <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
            </div>
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>