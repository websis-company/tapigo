<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;

use backend\models\Idiomas;
use backend\modules\promociones\models\Promociones;

$modelIdioma      = Idiomas::find()->where(['idioma_id'=>$model->idioma_id])->one();
$modelPromociones = Promociones::find()->where(['promocion_id'=>$model->promocion_id])->one();


$this->title = Yii::t('app', 'Información - '.$modelIdioma->nombre);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Promociones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/*echo "<pre>";
print_r($modelIdioma);
print_r($modelPromociones);
echo "</pre>";
*/
?>
<style type="text/css" media="screen">
	.field-promocionesidiomas-idioma_id, .field-promocionesidiomas-promocion_id, .field-promocionesidiomas-promocionidiomas_id{
		display: none;
	}
</style>

<div class="categorias-create col-xl-4 col-lg-8 col-md-8 col-sm-12 col-12">
	<div class="row-fluid">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
					<h1 class="card-title">
	                	<strong>
	                		<i class="nav-icon fas fa-globe-americas"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?> <?= Html::img(Url::base()."/".$modelIdioma->bandera, ['class' => 'img-fluid']); ?>
	                	</strong>
	                </h1>
	            </div>
	            <?php 
	            $form = ActiveForm::begin([
            			'id'=>"idiomaspromocionesform",
            			'action' => ['default/addpromocionlanguage'],
            			'options'=>[
            				'enctype'=>'multipart/form-data',
            			]
            		]); 
	            ?>
	            <div class="div-form card-body">
	            	<h6>- Agregar información para la promoción : <strong>"<?php echo $modelPromociones->nombre; ?>"</strong></h6>
	            	<?= $form->field($model, 'nombre',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true])->label('Nombre en '.$modelIdioma->nombre." :") ?>
	            	<?= $form->field($model, 'descripcion',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textarea(['rows' =>6])->label('Descripción en '.$modelIdioma->nombre." :") ?>
	            	<div class="clearfix"></div>
	            	<?php 
	            	if(!empty($model->imagenPrincipal)){
	            		echo "<label class='mt-3'>Imagen Actual</label>";
	            		echo "<div class='col-12 text-center'>".Html::img(Url::base()."/".$model->imagenPrincipal, ['class' => 'img-thumbnail col-5'])."</div>";
	            	}//end if
	            	?>
	            	<div class="clearfix"></div>
	            	<?= $form->field($model, 'fileMainImage',['options'=>['class'=>'col-sm-12 mt-5 bg-light']])->fileInput()->label('<div>Imagen Principal en '.$modelIdioma->nombre.' : </div> <div class=" alert-primary" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 1000px X 1000px (Ancho x Alto)</small></div>'); ?>
	            	<div class="clearfix"></div>	
	            	<?= $form->field($model, 'idioma_id')->hiddenInput(['maxlength' => true])->label(''); ?>
					<?= $form->field($model, 'promocion_id')->hiddenInput(['maxlength' => true])->label(''); ?>
					<?= $form->field($model, 'promocionIdiomas_id')->hiddenInput(['maxlength' => true])->label(''); ?>

	            </div>
	            <div class=" card-footer" align="center">
					<?= Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm','data-fancybox-close'=>true]) ?>
                	<?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>