<?php

namespace backend\modules\promociones\controllers;

use Yii;
use backend\modules\promociones\models\Promociones;
use backend\modules\promociones\models\PromocionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\web\ForbiddenHttpException;

use backend\models\Empresa;
use backend\models\Idiomas;
use backend\modules\promociones\models\Promocionesidiomas;

/**
 * Default controller for the `promociones` module
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index','view','create','update','delete'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Promociones models.
     * @return mixed
     */
    public function actionIndex(){
        if(Yii::$app->user->can('interPromos')){
            $searchModel = new PromocionesSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $modelEmpresa = Empresa::find()->where(['empresa_id'=>Yii::$app->user->identity->empresa_id,'cliente_id'=>Yii::$app->user->identity->cliente_id])->one();
            $idiomas      = empty($modelEmpresa->idiomas) ? null : $modelEmpresa->idiomas;
            return $this->render('index', [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
                'idiomas'      => $idiomas
            ]);
        }else{
            throw new ForbiddenHttpException;
        }
    }//end function

    /**
     * Displays a single Promociones model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id){
        if(Yii::$app->user->can('interPromos')){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Creates a new Promociones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        if(Yii::$app->user->can('interPromos')){
            $model             = new Promociones();
            $model->empresa_id = Yii::$app->user->identity->empresa_id;
            $model->cliente_id = Yii::$app->user->identity->cliente_id;

            if ($model->load(Yii::$app->request->post())) {
                //get the instance of the upload file
                $model->fileMainImage = UploadedFile::getInstance($model,'fileMainImage');
                if(!empty($model->fileMainImage)){
                    $imageName             = $model->fileMainImage->name;
                    $model->fileMainImage->saveAs('uploads/promociones/'.$imageName);
                    $model->imagenPrincipal = 'uploads/promociones/'.$imageName;
                }else{
                    $model->imagenPrincipal = NULL;    
                }//end if
                $model->save(false);

                Yii::$app->session->setFlash('success', "Se registro correctamente la promoción :  <strong>".$model->nombre."</strong>");
                return $this->redirect(['index']);
            }//end if

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Updates an existing Promociones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id){
        if(Yii::$app->user->can('interPromos')){
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {
                //get the instance of the upload file
                $model->fileMainImage = UploadedFile::getInstance($model,'fileMainImage');
                if(!empty($model->fileMainImage)){
                    $imageName             = $model->fileMainImage->name;
                    $model->fileMainImage->saveAs('uploads/promociones/'.$imageName);
                    $model->imagenPrincipal = 'uploads/promociones/'.$imageName;    
                }//end if

                $model->save(false);

                Yii::$app->session->setFlash('success', "Se actualizo correctamente la promoción :  <strong>".$model->nombre."</strong>");
                return $this->redirect(['index']);
            }//end if


            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Deletes an existing Promociones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id){
        if(Yii::$app->user->can('interPromos')){
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Finds the Promociones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Promociones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Promociones::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPromocioneslanguage($id_idioma,$id_promocion){
        if(Yii::$app->user->can('interIdiomas')){
            $model = Promocionesidiomas::find()->where(['idioma_id'=>$id_idioma,'promocion_id'=>$id_promocion])->one();
            if(empty($model)){
                $model               = new Promocionesidiomas;
                $model->idioma_id    = $id_idioma;
                $model->promocion_id = $id_promocion;
            }//end if

            return  $this->renderAjax('_formpromocion', [
                        'model' => $model,
                    ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end fuction

    public function actionAddpromocionlanguage(){
        $promocionesIdioma_id = Yii::$app->request->post()["Promocionesidiomas"]["promocionIdiomas_id"];
        if(!empty($promocionesIdioma_id)){
            $model = Promocionesidiomas::find()->where(['promocionIdiomas_id'=>$promocionesIdioma_id])->one();
            if ($model->load(Yii::$app->request->post())) {
                 //get the instance of the upload file
                $model->fileMainImage = UploadedFile::getInstance($model,'fileMainImage');
                if(!empty($model->fileMainImage)){
                    $imageName             = $model->fileMainImage->name;
                    $model->fileMainImage->saveAs('uploads/promociones/'.$imageName);
                    $model->imagenPrincipal = 'uploads/promociones/'.$imageName;
                }//end if
                $model->save(false);

                Yii::$app->session->setFlash('success', "Se actulizo la información correctamente.");
                return $this->redirect(['index']);
            }//end if
        }else{
            $model = new Promocionesidiomas();
            if($model->load(Yii::$app->request->post())){
                 //get the instance of the upload file
                $model->fileMainImage = UploadedFile::getInstance($model,'fileMainImage');
                if(!empty($model->fileMainImage)){
                    $imageName             = $model->fileMainImage->name;
                    $model->fileMainImage->saveAs('uploads/promociones/'.$imageName);
                    $model->imagenPrincipal = 'uploads/promociones/'.$imageName;    
                }else{
                    $model->imagenPrincipal = NULL;
                }//end if

                $model->save(false);
                Yii::$app->session->setFlash('success', "Se guardo la información correctamente.");
                return $this->redirect(['index']);
            }//end if
        };
    }//end function
}