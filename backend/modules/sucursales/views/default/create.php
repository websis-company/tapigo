<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\sucursales\models\Sucursales */

$this->title = 'Agregar Sucursal';
$this->params['breadcrumbs'][] = ['label' => 'Sucursales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sucursales-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
