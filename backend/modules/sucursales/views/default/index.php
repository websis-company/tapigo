<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use backend\models\Empresa;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\sucursales\models\SucursalesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sucursales';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="text-center col-12"><div class="loading" style="display: none;"></div></div>
    <div id="divEditForm" class="col-sm-12 col-md-12 col-lg-6 offset-lg-3" style="display: none;"></div>
</div>


<div class="sucursales-index">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-secondary card-outline">
                    <div class="card-header">
                         <div class="col-12 text-center"> 
                            <?php echo Html::button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Agregar Sucursal', ['value'=>Url::to(['create']),'class' => 'btn bg-gradient-primary float-right','id'=>'btnAddForm']);
                            ?>
                        </div>
                    </div>
                    <?php if (Yii::$app->session->hasFlash('success')): ?>
                        <div class="row-fluid mt-2" align="center">
                            <div class="col-sm-12">
                                <div class="alert bg-teal alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                   <i class="icon fa fa-check"></i> <?= Yii::$app->session->getFlash('success') ?>
                               </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="card-body pad table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        /*** Show Class in GridView ***/
                        'rowOptions' => function($model){
                            if($model->estatus == "Inactivo"){
                                return ['class'=>'tableDanger'];
                            }elseif($model->estatus == "Activo"){
                                return ['class'=>'tableSuccess'];
                            }
                        },
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'sucursal_id',
                            //'empresa_id',
                            //'cliente_id',
                            'nombre',
                            //'calle',
                            //'noExterior',
                            //'noInterior',
                            //'colonia',
                            //'codigoPostal',
                            //'estado',
                            //'ciudad',
                            //'pais',
                            //'telefono',
                            //'horaApertura',
                            //'horaCierre',
                            //'diasHabiles',
                            //'facebook',
                            //'instagram',
                            //'twitter',
                            //'whatsapp',
                            [
                                'attribute' =>'estatus',
                                'value'=> 'estatus',
                                'filter' => HTML::activeDropDownList(
                                    $searchModel,
                                    'estatus',
                                    ['Activo'=>'Activo','Inactivo'=>'Inactivo'],
                                    ['class'=>'form-control','prompt'=>'Todos']
                                ),
                            ],

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'header'=> 'Acciones',
                                'headerOptions'=>['style'=>'text-align:center'],
                                'template'=>'{view} {update} {delete}',
                                'buttons'=>[
                                    'view'=>function($url,$model){
                                        return Html::button('<span class="glyphicon glyphicon-search"></span>',['value'=>Url::to(['view','id'=>$model->sucursal_id]),'class' => 'btn btnViewForm']);
                                    },
                                    'update'=>function ($url, $model) {
                                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>',['value'=>Url::to(['update','id'=>$model->sucursal_id]),'class' => 'btn btnUpdateForm']);
                                    },
                                ]
                            ],
                        ],
                    ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
