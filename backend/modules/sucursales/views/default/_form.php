<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;
/* @var $this yii\web\View */
/* @var $model backend\modules\sucursales\models\Sucursales */
/* @var $form yii\widgets\ActiveForm */
?>

<style type="text/css" media="screen">
    .field-sucursales-empresa_id, .field-sucursales-cliente_id{
        display: none;
    }    
</style>
<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-primary">
            <div class="card-header">
                <h1 class="card-title"><strong><i class="nav-icon fas fa-store"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
                <button type="button" class="btn close text-white" onclick='closeForm("sucursalesForm")'>×</button>
            </div>
            <?php $form = \yii\bootstrap4\ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'sucursalesForm']]); ?>
            <div class="sucursales-form card-body">
                <div class="row mt-2">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-header bg-dark text-white ">
                                <h6><strong> Datos Generales</strong></h6>
                            </div>
                            <div class="card-body">
                                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true,'autocomplete'=>'off']) ?>
                                <?php 
                                echo $form->field($model, 'horaApertura',['options'=>['class'=>'col-md-4 col-sm-12 mt-3','style'=>'float: left;']])->widget(TimePicker::classname());
                                ?>
                                <?php 
                                echo $form->field($model, 'horaCierre',['options'=>['class'=>'col-md-4 col-sm-12 mt-3','style'=>'float: left;']])->widget(TimePicker::classname());
                                ?>

                                <?php 
                                    $data = ["L"=>"Lunes","MA"=>"Martes","MI"=>"Miercoles","J"=>"Jueves","V"=>"Viernes","S"=>"Sábado","D"=>"Domingo"];
                                    if(!empty($model->diasHabiles)){
                                        $value_data = explode(',',$model->diasHabiles);
                                    }else{
                                        $value_data = "";
                                    }//end if

                                    echo $form->field($model, 'diasHabiles[]',['options'=>['class'=>'col-md-4 col-sm-12 mt-3','style'=>'float: left;']])
                                                ->widget(Select2::classname(), [
                                                    'data' => $data,
                                                    'language' => 'es',
                                                    'options' => [
                                                        'value' => $value_data,//Initial values or selected values
                                                        'placeholder' => 'Seleccione los días laborales',
                                                        'id'=>'diasHabiles',
                                                    ],
                                                    'pluginOptions' => [
                                                        'tags' => true,
                                                        'allowClear' => true,
                                                        'multiple' => true,
                                                    ],
                                                ]);
                                ?>

                                <!-- <?//= $form->field($model, 'horaApertura')->textInput() ?>
                                <?//= $form->field($model, 'horaCierre')->textInput() ?> -->
                                <!-- <?//= $form->field($model, 'diasHabiles')->textInput(['maxlength' => true]) ?> -->
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header bg-dark text-white ">
                                <h6><strong> Dirección</strong></h6>
                            </div>
                            <div class="card-body"> 
                                <?= $form->field($model, 'calle',['options'=>['class'=>'col-md-3 col-sm-12 ','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'noExterior',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'noInterior',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'colonia',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <div class="clearfix"></div>
                                <?= $form->field($model, 'codigoPostal',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'estado',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'ciudad',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'telefono',['options'=>['class'=>'col-md-3 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header bg-dark text-white ">
                                <h6><strong> Redes Sociales</strong></h6>
                            </div>
                            <div class="card-body">
                                <?= $form->field($model, 'facebook',['options'=>['class'=>'col-md-6 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'instagram',['options'=>['class'=>'col-md-6 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <div class="clearfix"></div>
                                <?= $form->field($model, 'twitter',['options'=>['class'=>'col-md-6 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'whatsapp',['options'=>['class'=>'col-md-6 col-sm-12','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                

                <?= $form->field($model, 'empresa_id')->hiddenInput() ?>
                <?= $form->field($model, 'cliente_id')->hiddenInput() ?>
                <?= $form->field($model, 'estatus')->hiddenInput(['value' => 'Activo'])->label(''); ?>
            </div>
            <div class=" card-footer" align="right">
               <?= Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-outline-danger','id'=>'btnCloseForm','onClick'=>'closeForm("sucursalesForm")']) ?>
               <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-outline-success']) ?>
            </div>
            <?php \yii\bootstrap4\ActiveForm::end(); ?>
        </div>
    </div>
</div>
