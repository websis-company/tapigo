<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\sucursales\models\Sucursales */

$this->title = 'Editar Sucursales: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Sucursales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sucursal_id, 'url' => ['view', 'id' => $model->sucursal_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sucursales-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
