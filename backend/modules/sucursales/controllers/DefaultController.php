<?php

namespace backend\modules\sucursales\controllers;

use Yii;
use backend\modules\sucursales\models\Sucursales;
use backend\modules\sucursales\models\SucursalesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * DefaultController implements the CRUD actions for Sucursales model.
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index','view','create','update','delete'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sucursales models.
     * @return mixed
     */
    public function actionIndex(){
        if(Yii::$app->user->can('premiumSucursales')){
            $searchModel = new SucursalesSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Displays a single Sucursales model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id){
        if(Yii::$app->user->can('premiumSucursales')){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Creates a new Sucursales model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        if(Yii::$app->user->can('premiumSucursales')){
            $model = new Sucursales();
            $model->empresa_id = Yii::$app->user->identity->empresa_id;
            $model->cliente_id = Yii::$app->user->identity->cliente_id;
            
            
            if ($model->load(Yii::$app->request->post())) {
                $diasHabiles        = implode(',',Yii::$app->request->post('Sucursales')["diasHabiles"]);
                $model->diasHabiles = $diasHabiles;
                $model->save();

                Yii::$app->session->setFlash('success', "Lo datos de la sucursal se guardaron correctamente.");
                return $this->redirect(['index',['model' => $model]]);
            }//end if

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Updates an existing Sucursales model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id){
        if(Yii::$app->user->can('premiumSucursales')){
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post())) {
                $diasHabiles = implode(',',Yii::$app->request->post('Sucursales')["diasHabiles"]);
                $model->diasHabiles = $diasHabiles;
                $model->save();
                
                Yii::$app->session->setFlash('success', "Lo datos de la sucursal <strong>".$model->nombre."</strong> se actualizaron correctamente.");
                return $this->redirect(['index',['model' => $model]]);
            }//end if

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Deletes an existing Sucursales model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id){
        if(Yii::$app->user->can('premiumSucursales')){
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Finds the Sucursales model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sucursales the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sucursales::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
