<?php

namespace backend\modules\sucursales\models;

use Yii;
use backend\models\Clientes;
use backend\models\Empresa;
/**
 * This is the model class for table "sucursales".
 *
 * @property int $sucursal_id
 * @property int $empresa_id
 * @property int $cliente_id
 * @property string $nombre
 * @property string $calle
 * @property string $noExterior
 * @property string|null $noInterior
 * @property string $colonia
 * @property string $codigoPostal
 * @property string $estado
 * @property string $ciudad
 * @property string $pais
 * @property string|null $telefono
 * @property string|null $horaApertura
 * @property string|null $horaCierre
 * @property string|null $diasHabiles
 * @property string|null $facebook
 * @property string|null $instagram
 * @property string|null $twitter
 * @property string|null $whatsapp
 * @property string|null $estatus
 *
 * @property Menus[] $menuses
 * @property Clientes $cliente
 * @property Empresa $empresa
 */
class Sucursales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sucursales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id', 'cliente_id', 'nombre', 'calle', 'noExterior', 'colonia', 'codigoPostal', 'estado', 'ciudad','diasHabiles'], 'required'],
            [['empresa_id', 'cliente_id'], 'integer'],
            [['horaApertura', 'horaCierre'], 'safe'],
            [['estatus'], 'string'],
            [['nombre', 'colonia'], 'string', 'max' => 255],
            [['calle', 'ciudad', 'facebook', 'instagram', 'twitter'], 'string', 'max' => 250],
            [['noExterior', 'noInterior', 'whatsapp'], 'string', 'max' => 50],
            [['codigoPostal'], 'string', 'max' => 10],
            [['estado', 'pais'], 'string', 'max' => 150],
            [['telefono'], 'string', 'max' => 100],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente_id' => 'cliente_id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'empresa_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sucursal_id'  => 'Sucursal ID',
            'empresa_id'   => 'Empresa ID',
            'cliente_id'   => 'Cliente ID',
            'nombre'       => 'Nombre Sucursal',
            'calle'        => 'Calle',
            'noExterior'   => 'No. Exterior',
            'noInterior'   => 'No. Interior',
            'colonia'      => 'Colonia',
            'codigoPostal' => 'Código Postal',
            'estado'       => 'Estado',
            'ciudad'       => 'Ciudad',
            'pais'         => 'Pais',
            'telefono'     => 'Teléfono',
            'horaApertura' => 'Hora Apertura',
            'horaCierre'   => 'Hora Cierre',
            'diasHabiles'  => 'Días Habiles',
            'facebook'     => 'Facebook',
            'instagram'    => 'Instagram',
            'twitter'      => 'Twitter',
            'whatsapp'     => 'Whatsapp',
            'estatus'      => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Menuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenuses()
    {
        return $this->hasMany(Menus::className(), ['sucursal_id' => 'sucursal_id']);
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Empresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['empresa_id' => 'empresa_id']);
    }
}
