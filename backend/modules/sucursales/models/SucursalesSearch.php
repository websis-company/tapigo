<?php

namespace backend\modules\sucursales\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\sucursales\models\Sucursales;

/**
 * SucursalesSearch represents the model behind the search form of `backend\modules\sucursales\models\Sucursales`.
 */
class SucursalesSearch extends Sucursales
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sucursal_id', 'empresa_id', 'cliente_id'], 'integer'],
            [['nombre', 'calle', 'noExterior', 'noInterior', 'colonia', 'codigoPostal', 'estado', 'ciudad', 'pais', 'telefono', 'horaApertura', 'horaCierre', 'diasHabiles', 'facebook', 'instagram', 'twitter', 'whatsapp', 'estatus'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sucursales::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sucursal_id' => $this->sucursal_id,
            'empresa_id' => $this->empresa_id,
            'cliente_id' => $this->cliente_id,
            'horaApertura' => $this->horaApertura,
            'horaCierre' => $this->horaCierre,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'calle', $this->calle])
            ->andFilterWhere(['like', 'noExterior', $this->noExterior])
            ->andFilterWhere(['like', 'noInterior', $this->noInterior])
            ->andFilterWhere(['like', 'colonia', $this->colonia])
            ->andFilterWhere(['like', 'codigoPostal', $this->codigoPostal])
            ->andFilterWhere(['like', 'estado', $this->estado])
            ->andFilterWhere(['like', 'ciudad', $this->ciudad])
            ->andFilterWhere(['like', 'pais', $this->pais])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'diasHabiles', $this->diasHabiles])
            ->andFilterWhere(['like', 'facebook', $this->facebook])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
            ->andFilterWhere(['like', 'twitter', $this->twitter])
            ->andFilterWhere(['like', 'whatsapp', $this->whatsapp])
            ->andFilterWhere(['like', 'estatus', $this->estatus]);

        return $dataProvider;
    }
}
