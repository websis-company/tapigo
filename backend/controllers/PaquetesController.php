<?php

namespace backend\controllers;

use Yii;
use backend\models\Paquetes;
use backend\models\PaquetesSearch;
use backend\models\Paquetesmodulos;
use backend\models\Paquetesimpresos;
use backend\models\Model; /*** Model Generric For Dynamic Forms ***/

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * PaquetesController implements the CRUD actions for Paquetes model.
 */
class PaquetesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            /*** Access control ***/
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index','create','update','delete'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Paquetes models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('controlPaquetes')){
            $searchModel = new PaquetesSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Displays a single Paquetes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('controlPaquetes')){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Creates a new Paquetes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('controlPaquetes')){
            $model                 = new Paquetes();
            $modelPaquetesModulos  = new Paquetesmodulos();
            $modelPaquetesImpresos = [new Paquetesimpresos()]; /*** Dymanic Forms ***/


            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                //return $this->redirect(['view', 'id' => $model->paquete_id]);

                /*** Save relation Table [PaqueteModulo]***/
                $modulos_arr = Yii::$app->request->post('Paquetesmodulos')["modulo_id"];
                foreach ($modulos_arr as $moduloItem) {
                    $paquetesModulos             =  new Paquetesmodulos();
                    $paquetesModulos->modulo_id  = $moduloItem;
                    $paquetesModulos->paquete_id = $model->paquete_id;
                    $paquetesModulos->save();
                }
                /***********************/

                /*** Save relation Table [PaqueteImpresos]***/
                $modelsImpresosItem = Model::createMultiple(Paquetesimpresos::classname());
                Model::loadMultiple($modelsImpresosItem, Yii::$app->request->post());
                $valid = Model::validateMultiple($modelsImpresosItem);
                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        foreach ($modelsImpresosItem as $modelImpresosItem) {
                            $modelImpresosItem->paquete_id = $model->paquete_id;
                            if (!($flag = $modelImpresosItem->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
                //return $this->redirect(['index']);
            }

            return $this->renderAjax('create', [
                'model'                 => $model,
                'modelPaquetesModulos'  => $modelPaquetesModulos,
                'modelPaquetesImpresos' => (empty($modelPaquetesImpresos)) ? [new Paquetesimpresos] : $modelPaquetesImpresos
            ]);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Updates an existing Paquetes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('controlPaquetes')){
            $model                 = $this->findModel($id);
            $modelPaquetesModulos  = new Paquetesmodulos();
            $paquetes_modulos      = $model->getPaquetesmodulos()->all();
            $modelPaquetesImpresos = $model->getPaquetesimpresos()->all();/*** Dymanic Forms ***/


            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                /*** Search Related models and delete @Table [PaqueteModulo] ***/
                $modelsDeleteModulos = Paquetesmodulos::find()->where('paquete_id = '.$id)->all();
                foreach ($modelsDeleteModulos as $modelDeleteModulo) {
                    $modelDeleteModulo->delete();
                }//end foreach
                /***************************************/
                /*** Save news relationn @Table [PaqueteModulo]***/
                $modulos_arr = Yii::$app->request->post('Paquetesmodulos')["modulo_id"];
                foreach ($modulos_arr as $moduloItem) {
                    $paquetesModulos             =  new Paquetesmodulos();
                    $paquetesModulos->modulo_id  = $moduloItem;
                    $paquetesModulos->paquete_id = $model->paquete_id;
                    $paquetesModulos->save();
                }
                /***************************************/

                /*** Search Related models and delete @Table [PaqueteImpresos] ***/
                $modelsDeleteImpresos = Paquetesimpresos::find()->where('paquete_id = '.$id)->all();
                foreach ($modelsDeleteImpresos as $modelDeleteImpreso) {
                    $modelDeleteImpreso->delete();
                }//end foreach
                /***************************************/
                /*** Save news relationn @Table [PaqueteImpresos]***/
                $modelsImpresosItem = Model::createMultiple(Paquetesimpresos::classname());
                Model::loadMultiple($modelsImpresosItem, Yii::$app->request->post());
                $valid = Model::validateMultiple($modelsImpresosItem);
                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        foreach ($modelsImpresosItem as $modelImpresosItem) {
                            $modelImpresosItem->paquete_id = $model->paquete_id;
                            if (!($flag = $modelImpresosItem->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }else{
                    echo "<pre>";
                    var_dump("entra");
                    echo "</pre>";
                    die();
                    
                }
                /***************************************/

                return $this->redirect(['index']);
                //return $this->redirect(['view', 'id' => $model->paquete_id]);
            }

            return $this->renderAjax('update', [
                'model'                => $model,
                'modelPaquetesModulos' => $modelPaquetesModulos,
                'paquetes_modulos'     => $paquetes_modulos,
                'modelPaquetesImpresos' => (empty($modelPaquetesImpresos)) ? [new Paquetesimpresos] : $modelPaquetesImpresos
            ]);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Deletes an existing Paquetes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('controlPaquetes')){
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Finds the Paquetes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Paquetes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Paquetes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public static function modulosActivos($model){
        return $model->attributes;
    }
}
