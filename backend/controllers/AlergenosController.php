<?php

namespace backend\controllers;

use Yii;
use backend\models\Alergenos;
use backend\models\AlergenosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\web\ForbiddenHttpException;

/**
 * AlergenosController implements the CRUD actions for Alergenos model.
 */
class AlergenosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index','create','update','delete'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alergenos models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('controlAlergenos')){
            $searchModel = new AlergenosSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Displays a single Alergenos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('controlAlergenos')){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Creates a new Alergenos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('controlAlergenos')){
            $model = new Alergenos();
            if ($model->load(Yii::$app->request->post())) {
                
                $model->file = UploadedFile::getInstance($model,'file');
                if(!empty($model->file)){
                    $imageName      = $model->file->name;
                    $model->file->saveAs('uploads/alergenos/'.$imageName);
                    $model->logo = 'uploads/alergenos/'.$imageName;    
                }else{
                    $model->logo = NULL;
                }//end if

                $model->save(false);
                return $this->redirect(['index']);
            }//end if

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Updates an existing Alergenos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('controlAlergenos')){
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post())) {
                $model->file = UploadedFile::getInstance($model,'file');
                if(!empty($model->file)){
                    $imageName   = $model->file->name;
                    $model->file->saveAs('uploads/alergenos/'.$imageName);
                    $model->logo = 'uploads/alergenos/'.$imageName;
                }
                $model->save(false);
                
                return $this->redirect(['index']);
            }//end if

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }//end if
    }//end function

    /**
     * Deletes an existing Alergenos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alergenos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Alergenos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alergenos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
