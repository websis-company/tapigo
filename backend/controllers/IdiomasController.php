<?php

namespace backend\controllers;

use Yii;
use backend\models\Idiomas;
use backend\models\IdiomasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\web\ForbiddenHttpException;

/**
 * IdiomasController implements the CRUD actions for Idiomas model.
 */
class IdiomasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            /*** Access control ***/
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index','create','update','delete'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Idiomas models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('controlIdiomas')){
            $searchModel = new IdiomasSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Displays a single Idiomas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('controlIdiomas')){
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Creates a new Idiomas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('controlIdiomas')){
            $model = new Idiomas();
            if ($model->load(Yii::$app->request->post())){
                //get the instance of the upload file
                $model->file = UploadedFile::getInstance($model,'file');
                if(!empty($model->file)){
                    $imageName      = $model->file->name;
                    $model->file->saveAs('uploads/idiomas/'.$imageName);
                    $model->bandera = 'uploads/idiomas/'.$imageName;    
                }else{
                    $model->bandera = NULL;    
                }//end if
                $model->save(false);
                return $this->redirect(['index']);
            }

            /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
                //return $this->redirect(['view', 'id' => $model->idioma_id]);
                return $this->redirect(['index']);
            }*/

            /**** View Ajax Mode ***/
            return $this->renderAjax('create', [
                'model' => $model,
            ]);

            /*return $this->render('create', [
                'model' => $model,
            ]);*/
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Updates an existing Idiomas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('controlIdiomas')){
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post())){
                //get the instance of the upload file
                $model->file = UploadedFile::getInstance($model,'file');
                if(!empty($model->file)){
                    $imageName   = $model->file->name;
                    $model->file->saveAs('uploads/idiomas/'.$imageName);
                    $model->bandera = 'uploads/idiomas/'.$imageName;
                }
                $model->save(false);
                //return $this->redirect(['view', 'id' => $model->idioma_id]);
                return $this->redirect(['index']);
            }


            /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idioma_id]);
            }*/

            /**** View Ajax Mode ***/
            return $this->renderAjax('update', [
                'model' => $model,
            ]);

            /*return $this->render('update', [
                'model' => $model,
            ]);*/
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Deletes an existing Idiomas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('controlIdiomas')){
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Finds the Idiomas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Idiomas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Idiomas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
