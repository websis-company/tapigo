<?php

namespace backend\controllers;

use Yii;
use backend\models\Paquetesmodulos;
use backend\models\PaquetesmodulosSearch;
use backend\models\Modulos;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PaquetesmodulosController implements the CRUD actions for Paquetesmodulos model.
 */
class PaquetesmodulosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index','create','update','delete','list'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'list'   => ['GET','POST']
                ],
            ],
        ];
    }

    /**
     * Lists all Paquetesmodulos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaquetesmodulosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Paquetesmodulos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Paquetesmodulos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Paquetesmodulos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->paqueteModulo_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Paquetesmodulos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->paqueteModulo_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Paquetesmodulos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionList($id){
        /*** Reload All "Modulos" if ID empty ***/
        if(empty($id)){
             $modulos = Modulos::find()->select('modulo_id,nombre')->all();
             foreach ($modulos as $modulo) {
                echo "<option value='".$modulo->modulo_id."'>".$modulo->nombre." </option>";
            }//end foreach
        }else{
            /*** Compare two arrays for get "Modulos" that not include in "Paquetes" ***/
            $modulosPaquetes = Paquetesmodulos::find()
                ->select('modulo_id')
                ->where(['paquete_id' => $id])
                ->all();

            $modulos = Modulos::find()->select('modulo_id,nombre')->all();

            $arrModulos = [];
            foreach ($modulos as $modulo) {
                $arrModulos[] = $modulo->modulo_id;
            }

            $arrModulosPaquetes = [];
            foreach ($modulosPaquetes as $moduloPaquetes) {
                $arrModulosPaquetes[] = $moduloPaquetes->modulo_id;
            }

            $resArray = array_diff($arrModulos, $arrModulosPaquetes);

            if(count($resArray) > 0){
                foreach ($resArray as $res) {
                    $modelModulos = Modulos::find()
                                        ->select('modulo_id,nombre')
                                        ->where(['modulo_id'=>$res])
                                        ->one();
                    echo "<option value='".$modelModulos->modulo_id."'>".$modelModulos->nombre." </option>";
                }//end foreach
            }else{
                echo "<option value=''></option>";
            }//end if
        }//end if
    }

    /**
     * Finds the Paquetesmodulos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Paquetesmodulos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Paquetesmodulos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
