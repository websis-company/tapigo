<?php

namespace backend\controllers;

use Yii;
use backend\models\Clientes;
use backend\models\ClientesSearch;
use backend\models\Adicionales;
use backend\models\Contratos;
use backend\models\Empresa;
use backend\models\Model; /*** Model Generric For Dynamic Forms ***/

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * ClientesController implements the CRUD actions for Clientes model.
 */
class ClientesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=>[
                'class' => AccessControl::className(),
                'only'  => ['index','create','update','delete'],
                'rules' => [
                    [
                        'allow'=>true,
                        'roles'=>['@'], //"@" only register user. This sign "?" is for guest users
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clientes models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('controlClientes')){
            $searchModel = new ClientesSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            throw new ForbiddenHttpException;
            //return $this->redirect(['site/index']);
        }
    }

    /**
     * Displays a single Clientes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('controlClientes')){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Creates a new Clientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('controlClientes')){
            $model              = new Clientes();
            $modulosAdicionales = [];
            $modelAdicionales   = [new Adicionales()]; /*** Dymanic Forms ***/
            $modelEmpresa       = new Empresa();


            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                /**** Save "Empresa" ****/
                $modelEmpresa->load(Yii::$app->request->post());
                $modelEmpresa->nombre     = Yii::$app->request->post()["Empresa"]["nombre"];
                $modelEmpresa->cliente_id = $model->cliente_id;
                $modelEmpresa->estatus    = "Activo";
                $modelEmpresa->save(false);

                /**** Save "Paquete" ***/
                $model->paqueteId = Yii::$app->request->post("Clientes")["paqueteId"];
                $dateNow                         = date('Y-m-d H:i:s');
                $modelContrato                   = new Contratos;
                $modelContrato->cliente_id       = $model->cliente_id;
                $modelContrato->paquete_id       = $model->paqueteId;
                $modelContrato->fechaContrato    = $dateNow;
                $modelContrato->fechaRenovacion  = date('Y-m-d H:i:s',strtotime($dateNow."+ 1 month"));
                $modelContrato->fechaCancelacion = NULL;
                $modelContrato->tipoContrato     = 'Paquete';
                $modelContrato->renovado         = 'No';
                $modelContrato->estatus          = 'Activo';
                $modelContrato->save();

                /*** Save "Modulos Adicionales" ***/
                $arrModulos = Yii::$app->request->post("Clientes")["moduloId"];
                if(!empty($arrModulos) && count($arrModulos) > 0){
                    foreach ($arrModulos as $modulo_id) {
                        $modelModulosAdicionales                  = new Adicionales;
                        $modelModulosAdicionales->contrato_id     = $modelContrato->contrato_id;
                        $modelModulosAdicionales->modulo_id       = $modulo_id;
                        $modelModulosAdicionales->fechaContrato   = $dateNow;
                        $modelModulosAdicionales->fechaRenovacion = date('Y-m-d H:i:s',strtotime($dateNow."+ 1 month"));
                        $modelModulosAdicionales->renovado        = 'No';
                        $modelModulosAdicionales->tipo            = 'Modulo';
                        $modelModulosAdicionales->estatus         = 'Activo';
                        $modelModulosAdicionales->save();
                    }//end foreach
                }//end if

                /*** Save "Impresos Adicionales" ***/
                $modelsImpresosItem = Model::createMultiple(Adicionales::classname());
                Model::loadMultiple($modelsImpresosItem, Yii::$app->request->post());
                $valid = Model::validateMultiple($modelsImpresosItem);
                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        foreach ($modelsImpresosItem as $modelImpresosItem) {
                            $modelImpresosItem->contrato_id   = $modelContrato->contrato_id;
                            $modelImpresosItem->fechaContrato = $dateNow;
                            $modelImpresosItem->renovado      = "No";
                            $modelImpresosItem->tipo          = 'Impreso';
                            $modelImpresosItem->estatus       = "Activo";
                            if (!($flag = $modelImpresosItem->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }//end if
                //return $this->redirect(['index']);
            }//end if

            return $this->renderAjax('create', [
                'model'              => $model,
                'modelEmpresa'       => $modelEmpresa,
                'modulosAdicionales' => $modulosAdicionales,
                'modelAdicionales'   => (empty($modelAdicionales)) ? [new Adicionales] : $modelAdicionales
            ]);
        }else{
             throw new ForbiddenHttpException;
        }
    }

    private function loadAttributesAdicionales($modelSearch){
        $arrModel = []; 
        if(count($modelSearch) > 0){
            foreach ($modelSearch as $model) {
                $arrModel[] = $model->modulo_id;
            }//end foreach
        }//end if
        return $arrModel;
    }

    /**
     * Updates an existing Clientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('controlClientes')){
            /*************************************************************************/
            $model         = $this->findModel($id);
            $modelEmpresa = Empresa::find()
                                ->where(['cliente_id'=>$model->cliente_id])
                                ->andWhere(['estatus'=>'Activo'])
                                ->orderBy(['empresa_id'=>SORT_DESC])
                                ->one();
            /*** Gets the record of the table "Contratos" related to "Clientes" ****/
            $modelContrato = Contratos::find()
                                ->where(['cliente_id' => $model->cliente_id])
                                ->andWhere(['estatus'=>'Activo'])
                                ->orderBy(['contrato_id'=>SORT_DESC])
                                ->one();
            $modelModulosAdicionales = Adicionales::find()
                                        ->where(['contrato_id'=>$modelContrato->contrato_id])
                                        ->andWhere(['tipo'=>'Modulo'])
                                        ->andWhere(['estatus'=>'Activo'])
                                        ->all();
            $modelImpresosAdicionales = Adicionales::find()
                                        ->where(['contrato_id'=>$modelContrato->contrato_id])
                                        ->andWhere(['tipo'=>'Impreso'])
                                        ->andWhere(['estatus'=>'Activo'])
                                        ->all();
            $modulosAdicionales  = $this->loadAttributesAdicionales($modelModulosAdicionales);
            if(!is_null($modelContrato)){
                $model->paqueteId = $modelContrato->paquete_id;
            }//end if
            $dateNow    = date('Y-m-d H:i:s');
            /*************************************************************************/

            /*************************************************************************/
            if($model->load(Yii::$app->request->post()) && $model->save()) {
                /**** Save Empresa ***/
                if(!is_null($modelEmpresa)){
                    $modelEmpresa->load(Yii::$app->request->post());
                    $modelEmpresa->nombre  = Yii::$app->request->post()["Empresa"]["nombre"];
                    $modelEmpresa->save(false);
                }else{
                    $modelEmpresa = new Empresa;
                    $modelEmpresa->load(Yii::$app->request->post());
                    $modelEmpresa->cliente_id = $model->cliente_id;
                    $modelEmpresa->estatus    = "Activo";
                    $modelEmpresa->save(false);
                }//end if


                /**** Save Paquete ***/
                $model->paqueteId = Yii::$app->request->post("Clientes")["paqueteId"];
                if(!empty($model->paqueteId)){
                    $modelContrato->paquete_id = $model->paqueteId;
                }else{
                    $modelContrato->paquete_id = NULL;
                }//end if
                $modelContrato->save();

                /*** Delete "Modulos Adicionales" before save news records***/
                foreach ($modelModulosAdicionales as $modelDeleteModulo) {
                    $modelDeleteModulo->delete();
                }//end foreach
                /************************************************************/
                /*** Save "Modulos Adicionales" after delete olds records ***/
                $arrModulos = Yii::$app->request->post("Clientes")["moduloId"];
                if(!empty($arrModulos) && count($arrModulos) > 0){
                    foreach ($arrModulos as $modulo_id) {
                        $modelModulosAdicionales                  = new Adicionales;
                        $modelModulosAdicionales->contrato_id     = $modelContrato->contrato_id;
                        $modelModulosAdicionales->modulo_id       = $modulo_id;
                        $modelModulosAdicionales->fechaContrato   = $dateNow;
                        $modelModulosAdicionales->fechaRenovacion = date('Y-m-d H:i:s',strtotime($dateNow."+ 1 month"));
                        $modelModulosAdicionales->renovado        = 'No';
                        $modelModulosAdicionales->tipo            = 'Modulo';
                        $modelModulosAdicionales->estatus         = 'Activo';
                        $modelModulosAdicionales->save();
                    }//end foreach
                }//end if
                /**************************************************************/

                foreach ($modelImpresosAdicionales as $modelDeleteImpreso) {
                    $modelDeleteImpreso->delete();
                }//end foreach
                /*** Save news relationn @Table [PaqueteImpresos]***/
                $modelsImpresosItem = Model::createMultiple(Adicionales::classname());
                Model::loadMultiple($modelsImpresosItem, Yii::$app->request->post());
                $valid = Model::validateMultiple($modelsImpresosItem);
                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        foreach ($modelsImpresosItem as $modelImpresosItem) {
                            $modelImpresosItem->contrato_id   = $modelContrato->contrato_id;
                            $modelImpresosItem->fechaContrato = $dateNow;
                            $modelImpresosItem->renovado      = "No";
                            $modelImpresosItem->tipo          = 'Impreso';
                            $modelImpresosItem->estatus       = "Activo";
                            if (!($flag = $modelImpresosItem->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }//end if
                /***************************************/
                return $this->redirect(['index']);
            }//end if
            /*************************************************************************/

            return $this->renderAjax('update', [
                'model'              => $model,
                'modelEmpresa'       => (empty($modelEmpresa)) ? new Empresa : $modelEmpresa,
                'modulosAdicionales' => $modulosAdicionales,
                'modelAdicionales'   => (empty($modelImpresosAdicionales)) ? [new Adicionales] : $modelImpresosAdicionales
            ]);
        }else{
            throw new ForbiddenHttpException;
        }
    }


    /**
     * Deletes an existing Clientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('controlClientes')){
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Finds the Clientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clientes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
