<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'language'=>'es',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'homeUrl' => '/tapigo/admin',
    'modules' => [
        'sucursales' => [
            'class' => 'backend\modules\sucursales\Module',
        ],
        'miempresa' => [
            'class' => 'backend\modules\empresa\Module',
        ],
        'categorias' => [
            'class' => 'backend\modules\menus\categorias\Module',
        ],
        'platillos' => [
            'class' => 'backend\modules\menus\platillos\Module',
        ],
        'menus' => [
            'class' => 'backend\modules\menus\menus\Module',
        ],
        'promociones' => [
            'class' => 'backend\modules\promociones\Module',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl'   => '/tapigo/admin'
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest']
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'baseUrl'   => '/tapigo/admin',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        
        /*** AdminLTE V3 ***/
        'view' => [
           'theme' => [
               'pathMap' => [
                    '@app/views' => '@vendor/hail812/yii2-adminlte3/src/views'
                ]
            ],
        ],
    ],
    'params' => $params,
];
