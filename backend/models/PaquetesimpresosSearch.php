<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Paquetesimpresos;

/**
 * PaquetesimpresosSearch represents the model behind the search form of `backend\models\Paquetesimpresos`.
 */
class PaquetesimpresosSearch extends Paquetesimpresos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paqueteImpresio', 'impreso_id', 'paquete_id', 'cantidad'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Paquetesimpresos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'paqueteImpresio' => $this->paqueteImpresio,
            'impreso_id' => $this->impreso_id,
            'paquete_id' => $this->paquete_id,
            'cantidad' => $this->cantidad,
        ]);

        return $dataProvider;
    }
}
