<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "idiomas".
 *
 * @property int $idioma_id
 * @property string $nombre
 * @property string $bandera
 * @property string $estatus
 */
class Idiomas extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'idiomas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre','estatus'], 'required'],
            [['estatus'], 'string'],
            [['file'], 'file','extensions'=>'jpg,png,gif'],
            [['nombre'], 'string', 'max' => 150],
            [['bandera'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idioma_id' => 'ID',
            'nombre' => 'Idioma',
            //'bandera' => 'Bandera',
            'file' => 'Bandera',
            'estatus' => 'Estatus',
        ];
    }
}
