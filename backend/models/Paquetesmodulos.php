<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "paquetesmodulos".
 *
 * @property int $paqueteModulo_id
 * @property int $modulo_id
 * @property int $paquete_id
 *
 * @property Paquetes $paquete
 * @property Modulos $modulo
 */
class Paquetesmodulos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquetesmodulos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modulo_id', 'paquete_id'], 'required'],
            [['modulo_id', 'paquete_id'], 'integer'],
            [['paquete_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['paquete_id' => 'paquete_id']],
            [['modulo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modulos::className(), 'targetAttribute' => ['modulo_id' => 'modulo_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paqueteModulo_id' => 'Paquete Modulo ID',
            'modulo_id' => 'Modulo ID',
            'paquete_id' => 'Paquete ID',
        ];
    }

    /**
     * Gets query for [[Paquete]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquete()
    {
        return $this->hasOne(Paquetes::className(), ['paquete_id' => 'paquete_id']);
    }

    /**
     * Gets query for [[Modulo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulo()
    {
        return $this->hasOne(Modulos::className(), ['modulo_id' => 'modulo_id']);
    }
}
