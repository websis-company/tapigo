<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "adicionales".
 *
 * @property int $adicional_id
 * @property int|null $contrato_id
 * @property int|null $modulo_id
 * @property int|null $impreso_id
 * @property string|null $fechaContrato
 * @property string|null $fechaRenovacion
 * @property string|null $fechaCancelacion
 * @property string|null $renovado
 * @property int|null $cantidad
 * @property string|null $estatus
 *
 * @property Contratos $contrato
 * @property Impresos $impreso
 * @property Modulos $modulo
 */
class Adicionales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'adicionales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contrato_id', 'modulo_id', 'impreso_id', 'cantidad'], 'integer'],
            [['fechaContrato', 'fechaRenovacion', 'fechaCancelacion'], 'safe'],
            [['renovado', 'estatus','tipo'], 'string'],
            [['contrato_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contratos::className(), 'targetAttribute' => ['contrato_id' => 'contrato_id']],
            [['impreso_id'], 'exist', 'skipOnError' => true, 'targetClass' => Impresos::className(), 'targetAttribute' => ['impreso_id' => 'impreso_id']],
            [['modulo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modulos::className(), 'targetAttribute' => ['modulo_id' => 'modulo_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'adicional_id'     => 'Adicional ID',
            'contrato_id'      => 'Contrato ID',
            'modulo_id'        => 'Modulo ID',
            'impreso_id'       => 'Impreso ID',
            'fechaContrato'    => 'Fecha Contrato',
            'fechaRenovacion'  => 'Fecha Renovacion',
            'fechaCancelacion' => 'Fecha Cancelacion',
            'renovado'         => 'Renovado',
            'cantidad'         => 'Cantidad',
            'tipo'             => 'Tipo',
            'estatus'          => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Contrato]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContrato()
    {
        return $this->hasOne(Contratos::className(), ['contrato_id' => 'contrato_id']);
    }

    /**
     * Gets query for [[Impreso]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImpreso()
    {
        return $this->hasOne(Impresos::className(), ['impreso_id' => 'impreso_id']);
    }

    /**
     * Gets query for [[Modulo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulo()
    {
        return $this->hasOne(Modulos::className(), ['modulo_id' => 'modulo_id']);
    }
}
