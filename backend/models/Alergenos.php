<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "alergenos".
 *
 * @property int $alergeno_id
 * @property string $nombre
 * @property string|null $descripcion
 * @property string $logo
 * @property string|null $estatus
 *
 * @property Productosalergenos[] $productosalergenos
 */
class Alergenos extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alergenos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['logo'],'safe'],
            [['file'], 'file','extensions'=>'jpg,png,gif'],
            [['descripcion', 'estatus'], 'string'],
            [['nombre'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'alergeno_id' => 'Alergeno ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'logo' => 'Logo',
            'file' => 'Logo',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Productosalergenos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosalergenos()
    {
        return $this->hasMany(Productosalergenos::className(), ['alergeno_id' => 'alergeno_id']);
    }
}
