<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Paquetesmodulos;

/**
 * PaquetesmodulosSearch represents the model behind the search form of `backend\models\Paquetesmodulos`.
 */
class PaquetesmodulosSearch extends Paquetesmodulos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paqueteModulo_id', 'modulo_id', 'paquete_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Paquetesmodulos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'paqueteModulo_id' => $this->paqueteModulo_id,
            'modulo_id' => $this->modulo_id,
            'paquete_id' => $this->paquete_id,
        ]);

        return $dataProvider;
    }
}
