<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Contratos;

/**
 * ContratosSearch represents the model behind the search form of `backend\models\Contratos`.
 */
class ContratosSearch extends Contratos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contrato_id', 'cliente_id', 'paquete_id'], 'integer'],
            [['fechaContrato', 'fechaRenovacion', 'fechaCancelacion', 'tipoContrato', 'renovado', 'estatus'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contratos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'contrato_id' => $this->contrato_id,
            'cliente_id' => $this->cliente_id,
            'paquete_id' => $this->paquete_id,
            'fechaContrato' => $this->fechaContrato,
            'fechaRenovacion' => $this->fechaRenovacion,
            'fechaCancelacion' => $this->fechaCancelacion,
        ]);

        $query->andFilterWhere(['like', 'tipoContrato', $this->tipoContrato])
            ->andFilterWhere(['like', 'renovado', $this->renovado])
            ->andFilterWhere(['like', 'estatus', $this->estatus]);

        return $dataProvider;
    }
}
