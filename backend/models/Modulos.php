<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "modulos".
 *
 * @property int $modulo_id
 * @property string $sku
 * @property string $nombre
 * @property string $descripcion
 * @property float $precio
 * @property string|null $estatus
 *
 * @property Adicionales[] $adicionales
 * @property Clientesmodulos[] $clientesmodulos
 * @property Paquetesmodulos[] $paquetesmodulos
 */
class Modulos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modulos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sku', 'nombre', 'descripcion', 'precio','estatus'], 'required'],
            [['descripcion', 'estatus'], 'string'],
            [['precio'], 'number'],
            [['sku'], 'string', 'max' => 15],
            [['nombre'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'modulo_id' => 'Modulo ID',
            'sku' => 'Sku',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Adicionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdicionales()
    {
        return $this->hasMany(Adicionales::className(), ['modulo_id' => 'modulo_id']);
    }

    /**
     * Gets query for [[Clientesmodulos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClientesmodulos()
    {
        return $this->hasMany(Clientesmodulos::className(), ['modulo_id' => 'modulo_id']);
    }

    /**
     * Gets query for [[Paquetesmodulos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetesmodulos()
    {
        return $this->hasMany(Paquetesmodulos::className(), ['modulo_id' => 'modulo_id']);
    }
}
