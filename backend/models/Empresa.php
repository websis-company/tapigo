<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "empresa".
 *
 * @property int $empresa_id
 * @property int $cliente_id
 * @property string $nombre
 * @property string|null $descripcion
 * @property string $calle
 * @property string $noExterior
 * @property string|null $noInterior
 * @property string $colonia
 * @property string $codigoPostal
 * @property string $estado
 * @property string $ciudad
 * @property string $pais
 * @property string|null $telefono
 * @property string|null $horaApertura
 * @property string|null $horaCierre
 * @property string|null $diasHabiles
 * @property string|null $facebook
 * @property string|null $Instagram
 * @property string|null $twitter
 * @property string|null $whatsapp
 * @property string $slug
 * @property string $estatus
 *
 * @property Clientes $cliente
 * @property Menus[] $menuses
 * @property Sucursales[] $sucursales
 */
class Empresa extends \yii\db\ActiveRecord
{
    public $fileImage;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_id', 'nombre', 'calle', 'noExterior', 'colonia', 'codigoPostal', 'estado', 'ciudad', 'estatus','diasHabiles'], 'required','on'=>['empresascenario']],
            [['cliente_id'], 'integer','on'=>['empresascenario']],
            [['descripcion', 'estatus'], 'string','on'=>['empresascenario']],
            [['descripcionEn','descripcionDe','descripcionFr','descripcionIt','descripcionPr','horaApertura', 'horaCierre','logo','qrimage'], 'safe','on'=>['empresascenario']],
            [['nombre', 'colonia', 'slug'], 'string', 'max' => 255,'on'=>['empresascenario']],
            [['calle', 'ciudad', 'facebook', 'Instagram', 'twitter'], 'string', 'max' => 250,'on'=>['empresascenario']],
            [['noExterior', 'noInterior', 'whatsapp'], 'string', 'max' => 50,'on'=>['empresascenario']],
            [['codigoPostal'], 'string', 'max' => 10,'on'=>['empresascenario']],
            [['estado', 'pais'], 'string', 'max' => 150,'on'=>['empresascenario']],
            [['telefono'], 'string', 'max' => 100,'on'=>['empresascenario']],
            [['fileImage'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 250,'maxWidth'=>500,'minHeight'=>250,'maxHeight'=>500,'maxSize'=>1024 * 1024 * 2,'on'=>['empresascenario']],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente_id' => 'cliente_id'],'on'=>['empresascenario']],

            [['idiomas'],'safe','on'=>['idiomascenario']],

            //Active only for scenario
            ['slug','slugExiste','on'=>['slugscenario']],
            ['slug','required','on'=>['slugscenario']],
            ['slug','match','pattern'=>'/^[a-zA-Z0-9_-]+$/','message'=>'El slug no debe contener carácteres especiales, solo número y letras.','on'=>['slugscenario']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'empresa_id' => 'Empresa ID',
            'cliente_id' => 'Cliente ID',
            'fileImage' => 'Logo',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'descripcionEn' => 'Descripción en Inglés',
            'descripcionDe' => 'Descripción en Alemán',
            'descripcionFr' => 'Descripción en Francés',
            'descripcionIt' => 'Descripción en Italiano',
            'descripcionPr' => 'Descripción en Portugués',
            'calle' => 'Calle',
            'noExterior' => 'No. Exterior',
            'noInterior' => 'No. Interior',
            'colonia' => 'Colonia',
            'codigoPostal' => 'Código Postal',
            'estado' => 'Estado',
            'ciudad' => 'Ciudad',
            'pais' => 'País',
            'telefono' => 'Teléfono',
            'horaApertura' => 'Hora Apertura',
            'horaCierre' => 'Hora Cierre',
            'diasHabiles' => 'Días Hábiles',
            'facebook' => 'Facebook',
            'Instagram' => 'Instagram',
            'twitter' => 'Twitter',
            'whatsapp' => 'Whatsapp',
            'slug' => 'Slug',
            'estatus' => 'Estatus',
        ];
    }

    public function slugExiste($attribute,$params){
        $slugs = Empresa::find()->select('empresa_id')->where(['slug'=>$this->slug])->one();
        if(!is_null($slugs)){
            $this->addError($this->slug,"El Slug que desea ocupar no se encuentra disponible, por favor intente con otro nombre.");
            return true;
        }//end if
    }//end function

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Menuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenuses()
    {
        return $this->hasMany(Menus::className(), ['empresa_id' => 'empresa_id']);
    }

    /**
     * Gets query for [[Sucursales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSucursales()
    {
        return $this->hasMany(Sucursales::className(), ['empresa_id' => 'empresa_id']);
    }
}
