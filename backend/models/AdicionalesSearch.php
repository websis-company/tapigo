<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Adicionales;

/**
 * AdicionalesSearch represents the model behind the search form of `backend\models\Adicionales`.
 */
class AdicionalesSearch extends Adicionales
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['adicional_id', 'contrato_id', 'modulo_id', 'impreso_id', 'cantidad'], 'integer'],
            [['fechaContrato', 'fechaRenovacion', 'fechaCancelacion', 'renovado', 'estatus'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Adicionales::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'adicional_id' => $this->adicional_id,
            'contrato_id' => $this->contrato_id,
            'modulo_id' => $this->modulo_id,
            'impreso_id' => $this->impreso_id,
            'fechaContrato' => $this->fechaContrato,
            'fechaRenovacion' => $this->fechaRenovacion,
            'fechaCancelacion' => $this->fechaCancelacion,
            'cantidad' => $this->cantidad,
        ]);

        $query->andFilterWhere(['like', 'renovado', $this->renovado])
            ->andFilterWhere(['like', 'estatus', $this->estatus]);

        return $dataProvider;
    }
}
