<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "paquetes".
 *
 * @property int $paquete_id
 * @property string $sku
 * @property string $nombre
 * @property string $descripción
 * @property float $precio
 * @property string $estatus
 *
 * @property Contratos[] $contratos
 * @property Paquetesimpresios[] $paquetesimpresios
 * @property Paquetesmodulos[] $paquetesmodulos
 */
class Paquetes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquetes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sku', 'nombre', 'descripción', 'precio', 'estatus'], 'required'],
            [['descripción', 'estatus'], 'string'],
            [['precio'], 'number'],
            [['sku'], 'string', 'max' => 15],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paquete_id' => 'Paquete ID',
            'sku' => 'Sku',
            'nombre' => 'Nombre',
            'descripción' => 'Descripción',
            'precio' => 'Precio',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Contratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratos()
    {
        return $this->hasMany(Contratos::className(), ['paquete_id' => 'paquete_id']);
    }

    /**
     * Gets query for [[Paquetesimpresios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetesimpresos()
    {
        return $this->hasMany(Paquetesimpresos::className(), ['paquete_id' => 'paquete_id']);
    }

    /**
     * Gets query for [[Paquetesmodulos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetesmodulos()
    {
        return $this->hasMany(Paquetesmodulos::className(), ['paquete_id' => 'paquete_id']);
    }
}
