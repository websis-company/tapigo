<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $cliente_id
 * @property string $nombres
 * @property string $apellidoPaterno
 * @property string $apellidoMaterno
 * @property string $email
 * @property string|null $telefono
 * @property string|null $movil
 *
 * @property Calificacionesplatillos[] $calificacionesplatillos
 * @property Categorias[] $categorias
 * @property Clientesmodulos[] $clientesmodulos
 * @property Comensales[] $comensales
 * @property Contratos[] $contratos
 * @property Empresa[] $empresas
 * @property Menus[] $menuses
 * @property Productos[] $productos
 * @property Promociones[] $promociones
 * @property User[] $users
 */
class Clientes extends \yii\db\ActiveRecord
{
    public $paqueteId;
    public $moduloId;
    public $impresosId;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombres', 'apellidoPaterno', 'apellidoMaterno', 'email'], 'required'],
            [['nombres', 'apellidoPaterno', 'apellidoMaterno'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 250],
            [['email'], 'email'],
            [['telefono', 'movil'], 'string', 'max' => 150],
            [['telefono', 'movil'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cliente_id'      => 'Cliente ID',
            'nombres'         => 'Nombre(s)',
            'apellidoPaterno' => 'Apellido Paterno',
            'apellidoMaterno' => 'Apellido Materno',
            'email'           => 'Email',
            'telefono'        => 'Telefono',
            'movil'           => 'Movil',
            'paqueteId'       => 'Paquetes',
            'moduloId'        => 'Módulos',
            'impresosId'      => 'Impresos',
        ];
    }

    /**
     * Gets query for [[Calificacionesplatillos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCalificacionesplatillos()
    {
        return $this->hasMany(Calificacionesplatillos::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Categorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias()
    {
        return $this->hasMany(Categorias::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Clientesmodulos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClientesmodulos()
    {
        return $this->hasMany(Clientesmodulos::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Comensales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComensales()
    {
        return $this->hasMany(Comensales::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Contratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratos()
    {
        return $this->hasMany(Contratos::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Empresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresas()
    {
        return $this->hasMany(Empresa::className(), ['cliente_id' => 'cliente_id']);
    }

    public function getEmpresaByClienteId(){
        return Empresa::find()
                ->where(['cliente_id'=>$this->cliente_id])
                ->andWhere(['estatus'=>'Activo'])
                ->orderBy(['empresa_id'=>SORT_DESC])
                ->one();
    }

    /**
     * Gets query for [[Menuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenuses()
    {
        return $this->hasMany(Menus::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Promociones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPromociones()
    {
        return $this->hasMany(Promociones::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['cliente_id' => 'cliente_id']);
    }
}
