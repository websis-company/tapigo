<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "menus".
 *
 * @property int $menu_id
 * @property int $cliente_id
 * @property int $empresa_id
 * @property int|null $sucursal_id
 * @property string $nombre
 * @property string|null $descripcion
 * @property string|null $flag
 * @property string|null $estatus
 *
 * @property Articulos[] $articulos
 * @property Clientes $cliente
 * @property Empresa $empresa
 * @property Sucursales $sucursal
 * @property Menusidiomas[] $menusidiomas
 */
class Menus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_id', 'empresa_id', 'nombre'], 'required'],
            [['cliente_id', 'empresa_id', 'sucursal_id'], 'integer'],
            [['descripcion', 'flag', 'estatus'], 'string'],
            [['nombre'], 'string', 'max' => 250],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente_id' => 'cliente_id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'empresa_id']],
            //[['sucursal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursales::className(), 'targetAttribute' => ['sucursal_id' => 'sucursal_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'menu_id' => 'Menu ID',
            'cliente_id' => 'Cliente ID',
            'empresa_id' => 'Empresa ID',
            'sucursal_id' => 'Sucursal ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'flag' => 'Flag',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Articulos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArticulos()
    {
        return $this->hasMany(Articulos::className(), ['menu_id' => 'menu_id']);
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['cliente_id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Empresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['empresa_id' => 'empresa_id']);
    }

    /**
     * Gets query for [[Sucursal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSucursal()
    {
        return $this->hasOne(Sucursales::className(), ['sucursal_id' => 'sucursal_id']);
    }

    /**
     * Gets query for [[Menusidiomas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenusidiomas()
    {
        return $this->hasMany(Menusidiomas::className(), ['menu_id' => 'menu_id']);
    }
}
