<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "impresos".
 *
 * @property int $impreso_id
 * @property string $sku
 * @property string $nombre
 * @property string $descripcion
 * @property float $precio
 * @property string $estatus
 *
 * @property Adicionales[] $adicionales
 * @property Paquetesimpresos[] $paquetesimpresos
 */
class Impresos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'impresos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sku', 'nombre', 'descripcion', 'precio', 'estatus'], 'required'],
            [['descripcion', 'estatus'], 'string'],
            [['precio'], 'number'],
            [['sku'], 'string', 'max' => 15],
            [['nombre'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'impreso_id' => 'Impreso ID',
            'sku' => 'Sku',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Adicionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdicionales()
    {
        return $this->hasMany(Adicionales::className(), ['impreso_id' => 'impreso_id']);
    }

    /**
     * Gets query for [[Paquetesimpresos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetesimpresos()
    {
        return $this->hasMany(Paquetesimpresos::className(), ['impreso_id' => 'impreso_id']);
    }
}
