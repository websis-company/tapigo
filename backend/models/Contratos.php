<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "contratos".
 *
 * @property int $contrato_id
 * @property int $cliente_id
 * @property int|null $paquete_id
 * @property string|null $fechaContrato
 * @property string|null $fechaRenovacion
 * @property string|null $fechaCancelacion
 * @property string|null $tipoContrato
 * @property string|null $renovado
 * @property string|null $estatus
 *
 * @property Adicionales[] $adicionales
 * @property Paquetes $paquete
 * @property Clientes $cliente
 */
class Contratos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contratos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_id'], 'required'],
            [['cliente_id', 'paquete_id'], 'integer'],
            [['fechaContrato', 'fechaRenovacion', 'fechaCancelacion'], 'safe'],
            [['tipoContrato', 'renovado', 'estatus'], 'string'],
            [['paquete_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['paquete_id' => 'paquete_id']],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente_id' => 'cliente_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'contrato_id' => 'Contrato ID',
            'cliente_id' => 'Cliente ID',
            'paquete_id' => 'Paquete ID',
            'fechaContrato' => 'Fecha Contrato',
            'fechaRenovacion' => 'Fecha Renovacion',
            'fechaCancelacion' => 'Fecha Cancelacion',
            'tipoContrato' => 'Tipo Contrato',
            'renovado' => 'Renovado',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Adicionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdicionales()
    {
        return $this->hasMany(Adicionales::className(), ['contrato_id' => 'contrato_id']);
    }

    /**
     * Gets query for [[Paquete]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquete()
    {
        return $this->hasOne(Paquetes::className(), ['paquete_id' => 'paquete_id']);
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['cliente_id' => 'cliente_id']);
    }
}
