<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "paquetesimpresos".
 *
 * @property int $paqueteImpresio
 * @property int $impreso_id
 * @property int $paquete_id
 * @property int|null $cantidad
 *
 * @property Paquetes $paquete
 * @property Impresos $impreso
 */
class Paquetesimpresos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquetesimpresos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['impreso_id','cantidad'], 'required'],
            [['impreso_id', 'paquete_id', 'cantidad'], 'integer'],
            [['paquete_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['paquete_id' => 'paquete_id']],
            [['impreso_id'], 'exist', 'skipOnError' => true, 'targetClass' => Impresos::className(), 'targetAttribute' => ['impreso_id' => 'impreso_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paqueteImpresio' => 'Paquete Impresio',
            'impreso_id' => 'Impreso ID',
            'paquete_id' => 'Paquete ID',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[Paquete]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquete()
    {
        return $this->hasOne(Paquetes::className(), ['paquete_id' => 'paquete_id']);
    }

    /**
     * Gets query for [[Impreso]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImpreso()
    {
        return $this->hasOne(Impresos::className(), ['impreso_id' => 'impreso_id']);
    }
}
