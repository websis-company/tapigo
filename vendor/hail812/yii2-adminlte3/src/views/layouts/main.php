<?php

/* @var $this \yii\web\View */
/* @var $content string */
use backend\assets\AppAsset;
use yii\helpers\Html;
\hail812\adminlte3\assets\FontAwesomeAsset::register($this);
\hail812\adminlte3\assets\AdminLteAsset::register($this);

use backend\models\Empresa;
use backend\models\Clientes;
use backend\models\Contratos;

AppAsset::register($this);

$this->registerCssFile('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700');

$assetDir = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

/*** Valid Empresa by User login ***/
$empresa_id = Yii::$app->user->identity->empresa_id;
$cliente_id = Yii::$app->user->identity->cliente_id;
if(!empty($empresa_id) && !empty($cliente_id)){
    $modelEmpresa =  Empresa::find()
                    ->where(['empresa_id'=>$empresa_id])
                    ->andWhere(['estatus'=>'Activo'])
                    ->orderBy(['empresa_id'=>SORT_DESC])
                    ->one();

    $modelCliente = Clientes::find()
                        ->where(['cliente_id'=>$cliente_id])
                        ->one();

    $modelContratos = $modelCliente->contratos;
    foreach ($modelContratos as $contrato) {
        //get modulos "Paquetes"
        $modulosContratos       = $contrato->paquete->paquetesmodulos;
        $adicionalesContratados = $contrato->adicionales;
    }//end foreach
}else{
    $modelEmpresa           = new Empresa;
    $modelCliente           = new Clientes;
    $modelContratos         = new Contratos;
    $modulosContratos       = [];
    $adicionalesContratados = [];
}//end if
/********************************/
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition sidebar-mini accent-primary">
<?php $this->beginBody() ?>

<div class="wrapper">
    <!-- Navbar -->
    <?= $this->render('navbar', ['assetDir' => $assetDir,'modelEmpresa'=>$modelEmpresa]) ?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?= $this->render('sidebar', ['assetDir' => $assetDir,'modulos'=>$modulosContratos]) ?>

    <!-- Content Wrapper. Contains page content -->
    <?= $this->render('content', ['content' => $content, 'assetDir' => $assetDir]) ?>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <?= $this->render('control-sidebar') ?>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <?= $this->render('footer') ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
