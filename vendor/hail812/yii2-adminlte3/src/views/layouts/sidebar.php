<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>
<aside class="main-sidebar sidebar-dark-olive elevation-4">
    <!-- Brand Logo -->
    <a href="<?=\yii\helpers\Url::home()?>" class="brand-link navbar-primary logo-switch">
        <?= Html::img(Url::base().'/images/logoTapigoXL.png', ['style' => 'margin:0; left: 20%;','class'=>'brand-image-xs logo-xl','alt'=>'Tapi Go!']); ?>
        <?= Html::img(Url::base().'/images/logoTapigoXS.png', ['style' => '','class'=>'brand-image-xl logo-xs','alt'=>'Tapi Go!']); ?>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex"> -->
            <!-- <div class="image">
                <img src="<?//=$assetDir?>/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div> -->
            <!-- <div class="info" style="text-align: center;">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div> -->
        <!-- </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?php
            $items   = [];
            $items[] = ['label' => 'Home', 'icon' => 'fas fa-home', 'url' => [Url::to('/site/index')], 'target' => ''];
            
            if(Yii::$app->user->can('premiumIdiomas')){
                $items[] = ['label' => 'Idiomas', 'icon' => 'fas fa-globe-americas', 'url' => [Url::to('/miempresa/default/idiomas')], 'target' => ''];
            }//end if
            if(Yii::$app->user->can('interIdiomas')){
                $items[] = ['label' => 'Idiomas', 'icon' => 'fas fa-globe-americas', 'url' => [Url::to('/miempresa/default/idiomas')], 'target' => ''];
            }//end if
            if(Yii::$app->user->can('basicEmpresa')){
                $items[] = ['label' => 'Mi Empresa', 'icon' => 'fas fa-store-alt', 'url' => [Url::to('/miempresa/default/index')], 'target' => ''];
            }//end if
            if(Yii::$app->user->can('premiumSucursales')){
                $items[] = ['label' => 'Sucursales', 'icon' => 'fas fa-store', 'url' => [Url::to('/sucursales/default/index')], 'target' => ''];
            }//end if
            if(Yii::$app->user->can('basicEmpresa')){
                $items[] = ['label' => 'Slug QR', 'icon' => 'fas fa-qrcode', 'url' => [Url::to('/miempresa/default/slug')], 'target' => ''];
            }//end if


            if(Yii::$app->user->can('basicMenu')){
                //$items[] = ['label' => 'Menús', 'icon' => 'fas fa-utensils', 'url' => [Url::to('/menus/default/index')], 'target' => ''];   
                $items[]= 
                    [
                        'label' => 'Configura Menús',
                        'icon'  => 'fas fa-cogs',
                        'items' => [
                            ['label' => 'Categorías', 'icon'=>'fas fa-list-alt','url' => [Url::to('/categorias/default/index')]],
                            ['label' => 'Platillos', 'icon'=>'fas fa-pizza-slice','url' => [Url::to('/platillos/default/index')]],
                            ['label' => 'Menú Digítal', 'icon'=>'fas fa-utensils','url' => [Url::to('/menus/default/index')]],
                        ]
                    ];
            }//end if

            if(Yii::$app->user->can('premiumMenus')){
                $items[]= 
                    [
                        'label' => 'Configura Menús',
                        'icon'  => 'fas fa-cogs',
                        'items' => [
                            ['label' => 'Categorías', 'icon'=>'fas fa-list-alt','url' => [Url::to('/categorias/default/index')]],
                            ['label' => 'Platillos', 'icon'=>'fas fa-pizza-slice','url' => [Url::to('/platillos/default/index')]],
                            ['label' => 'Menú - Principal', 'icon'=>'fas fa-utensils','url' => [Url::to('/menus/default/index')]],
                            ['label' => 'Menus - Sucursales', 'icon'=>'fas fa-utensils','url' => [Url::to('/menus/default/sucursales')]],
                        ]
                    ];
            }//end if


            if(Yii::$app->user->can('interPromos')){
                $items[] = ['label' => 'Promociones', 'icon' => 'fas fa-percent', 'url' => [Url::to('/promociones/default/index')], 'target' => ''];
            }//end if

            /**** Opciones admin ****/
            if(Yii::$app->user->can('controlClientes')){
                $items[] = ['label' => 'Clientes', 'icon' => 'fas fa-users', 'url' => [Url::to('/clientes/index')], 'target' => ''];
            }//end if
            if(Yii::$app->user->can('controlModulos')){
                $items[] = ['label' => 'Módulos', 'icon' => 'fas fa-puzzle-piece', 'url' => [Url::to('/modulos/index')], 'target' => ''];
            }//end if
            if(Yii::$app->user->can('controlImpresos')){
                $items[] = ['label' => 'Impresos', 'icon' => 'fa fa-print', 'url' => [Url::to('/impresos/index')], 'target' => ''];
            }//end if
            if(Yii::$app->user->can('controlPaquetes')){
                $items[] = ['label' => 'Paquetes', 'icon' => 'fad fa-cubes', 'url' => [Url::to('/paquetes/index')], 'target' => ''];
            }//end if
            if(Yii::$app->user->can('controlIdiomas')){
                $items[] = ['label' => 'Idiomas', 'icon' => 'globe-americas', 'url' => [Url::to('/idiomas/index')], 'target' => ''];
            }//end if
            if(Yii::$app->user->can('controlAlergenos')){
                $items[] = ['label' => 'Alérgenos', 'icon' => 'fas fa-skull-crossbones', 'url' => [Url::to('/alergenos/index')], 'target' => ''];
            }//end if
            /**** Opciones admin ****/

            echo \hail812\adminlte3\widgets\Menu::widget([
                'items' => //[
                    /*[
                        'label' => 'Starter Pages',
                        'icon' => 'tachometer-alt',
                        'badge' => '<span class="right badge badge-info">2</span>',
                        'items' => [
                            ['label' => 'Active Page', 'url' => ['site/index'], 'iconStyle' => 'far'],
                            ['label' => 'Inactive Page', 'url' => ['idiomas/index'], 'iconStyle' => 'far'],
                        ]
                    ],*/
                    $items,
                    /*['label' => 'Home', 'icon' => 'fas fa-home', 'url' => ['/site/index'], 'target' => ''],
                    ['label' => 'Clientes', 'icon' => 'fas fa-users', 'url' => ['/clientes/index'], 'target' => ''],
                    ['label' => 'Módulos', 'icon' => 'fas fa-puzzle-piece', 'url' => ['/modulos/index'], 'target' => ''],
                    ['label' => 'Impresos', 'icon' => 'fa fa-print', 'url' => ['/impresos/index'], 'target' => ''],
                    ['label' => 'Paquetes', 'icon' => 'fad fa-cubes', 'url' => ['/paquetes/index'], 'target' => ''],
                    ['label' => 'Idiomas', 'icon' => 'globe-americas', 'url' => ['/idiomas/index'], 'target' => ''],*/

                    /*['label' => 'Empresa', 'icon' => 'globe-americas', 'url' => ['/empresa/default'], 'target' => ''],                    */
                    /*['label' => 'Simple Link', 'icon' => 'th', 'badge' => '<span class="right badge badge-danger">New</span>'],
                    ['label' => 'Yii2 PROVIDED', 'header' => true],
                    ['label' => 'Login', 'url' => ['site/login'], 'icon' => 'sign-in-alt', 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Gii',  'icon' => 'file-code', 'url' => ['/gii'], 'target' => '_blank'],
                    ['label' => 'Debug', 'icon' => 'bug', 'url' => ['/debug'], 'target' => '_blank'],
                    ['label' => 'MULTI LEVEL EXAMPLE', 'header' => true],
                    ['label' => 'Level1'],
                    [
                        'label' => 'Level1',
                        'items' => [
                            ['label' => 'Level2', 'iconStyle' => 'far'],
                            [
                                'label' => 'Level2',
                                'iconStyle' => 'far',
                                'items' => [
                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle'],
                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle'],
                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle']
                                ]
                            ],
                            ['label' => 'Level2', 'iconStyle' => 'far']
                        ]
                    ],
                    ['label' => 'Level1'],
                    ['label' => 'LABELS', 'header' => true],
                    ['label' => 'Important', 'iconStyle' => 'far', 'iconClassAdded' => 'text-danger'],
                    ['label' => 'Warning', 'iconClass' => 'nav-icon far fa-circle text-warning'],
                    ['label' => 'Informational', 'iconStyle' => 'far', 'iconClassAdded' => 'text-info'],*/
                //],
            ]);
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>