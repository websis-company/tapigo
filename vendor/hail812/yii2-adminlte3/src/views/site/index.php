<?php
use yii\helpers\Url;

$this->title = 'Tapi Go! | Tu Menú Digital';
$this->params['breadcrumbs'] = [['label' => $this->title]];
?>
<div class="container-fluid">
    <div class="row">
        <?php 
        if(Yii::$app->user->can('interIdiomas') || Yii::$app->user->can('premiumIdiomas')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Idiomas</strong></h4>
                        <p>Configura los idiomas de tu menú</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-fas fa-globe-americas"></i>
                    </div>
                    <a href="<?php echo Url::to(['/miempresa/default/idiomas'])?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('basicEmpresa')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Mi Empresa</strong></h4>
                        <p>Editar información de mi empresa</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-store-alt"></i>
                    </div>
                    <a href="<?php echo Url::to(['/miempresa/default/index']) ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('premiumSucursales')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Sucursales</strong></h4>
                        <p>Administra sucursales de tu empresa.</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-store"></i>
                    </div>
                    <a href="<?php echo Url::to(['/sucursales/default/index']) ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('basicEmpresa')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Slug QR</strong></h4>
                        <p>Información sobre código QR</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-qrcode"></i>
                    </div>
                    <a href="<?php echo Url::to(['/miempresa/default/slug']) ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('generalCategorias')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Menú | Categorías</strong></h4>
                        <p>Categorías para tu menú digítal.</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-list-alt"></i>
                    </div>
                    <a href="<?php echo Url::to(['/categorias/default/index']); ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('generalProducto')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Menú | Platillos</strong></h4>
                        <p>Platillos para conformar tu menú digital</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-pizza-slice"></i>
                    </div>
                    <a href="<?php echo Url::to(['/platillos/default/index']) ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('basicMenu')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Menú Digítal</strong></h4>
                        <p>Arma tu menú digítal</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-utensils"></i>
                    </div>
                    <a href="<?php echo Url::to(['menus/default/index']) ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end
        ?>

        <?php 
        if(Yii::$app->user->can('premiumMenus')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Menú - Principal</strong></h4>
                        <p>Arma tu menú digítal</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-utensils"></i>
                    </div>
                    <a href="<?php echo Url::to(['menus/default/index']) ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('interPromos')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Promociones</strong></h4>
                        <p>Configura promociones para tus clientes.</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-percent"></i>
                    </div>
                    <a href="<?php echo Url::to(['/promociones/default/index'])?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php 
        }//end if
        ?>

        
        <?php 
        if(Yii::$app->user->can('controlClientes')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Clientes</strong></h4>
                        <p>Información</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-fas fa-users"></i>
                    </div>
                    <a href="<?php echo Url::to('../clientes/index'); ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>
        
        <?php 
        if(Yii::$app->user->can('controlModulos')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Módulos</strong></h4>
                        <p>Información</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-fas fa-puzzle-piece"></i>
                    </div>
                    <a href="<?php echo Url::to('../modulos/index'); ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('controlImpresos')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Impresos</strong></h4>
                        <p>Información</p>
                    </div>
                    <div class="icon">
                        <i class="nav-icon fas fa-fa fa-print"></i>
                    </div>
                    <a href="<?php echo Url::to('../impresos/index'); ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('controlPaquetes')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Paquetes</strong></h4>
                        <p>Información</p>
                    </div>
                    <div class="icon">
                        <i class="nav-icon fas fa-fad fa-cubes"></i>
                    </div>
                    <a href="<?php echo Url::to('../paquetes/index'); ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('controlIdiomas')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Idiomas</strong></h4>
                        <p>Información</p>
                    </div>
                    <div class="icon">
                        <i class="nav-icon fas fa-globe-americas"></i>
                    </div>
                    <a href="<?php echo Url::to('../idiomas/index'); ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php 
        }//end if
        ?>

        <?php 
        if(Yii::$app->user->can('controlAlergenos')){
        ?>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h4><strong>Alérgenos</strong></h4>
                        <p>Información</p>
                    </div>
                    <div class="icon">
                        <i class="nav-icon fas fa-skull-crossbones"></i>
                    </div>
                    <a href="<?php echo Url::to('../alergenos/index'); ?>" class="small-box-footer text-white">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        <?php
        }//end if
        ?>

        <!-- <div class="col-lg-6">
        <?= \hail812\adminlte3\widgets\Alert::widget([
            'type' => 'success',
            'body' => '<h3>Congratulations!</h3>',
        ]) ?>
        <?= \hail812\adminlte3\widgets\Callout::widget([
            'type' => 'danger',
            'head' => 'I am a danger callout!',
            'body' => 'There is a problem that we need to fix. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.'
        ]) ?>
        </div> -->
    </div>
</div>