<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use backend\models\AuthAssignment;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $firstName;
    public $lastName;
    public $username;
    public $email;
    public $password;
    public $cliente_id;
    public $empresa_id;



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Este nombre de usuario ya ha sido asignado a otro usuario.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Esta dirección de correo electrónico ya existe.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 12],
            [['cliente_id','empresa_id'],'required']
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }//end if

        $user = new User();
        $user->firstName = $this->firstName;
        $user->lastName = $this->lastName;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->status = User::STATUS_ACTIVE;
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->cliente_id = $this->cliente_id;
        $user->empresa_id = $this->empresa_id;
        $user->save();

        /** Save role user **/
        $userRole = new AuthAssignment;
        switch (Yii::$app->request->post()["skuPaquete"]) {
            case 'TGPAQ001':
                $role = "adminbasic";
                break;
            case 'TGPAQ002':
                $role = "adminInter";
                break;
            case 'TGPAQ003':
                $role = "adminPremium";
                break;
            default:
                $role = "adminbasic";
                break;
        }//end switch

        $userRole->item_name = $role;
        $userRole->user_id   = $user->id;
        $userRole->save(false);
        return $user; /*&& $this->sendEmail($user);*/

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
