<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\promociones\models\PromocionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promociones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promociones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Promociones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'promocion_id',
            'cliente_id',
            'nombre',
            'descripcion:ntext',
            'imagenPrincipal',
            //'imagenSlider1',
            //'imagenSlider2',
            //'imagenSlider3',
            //'imagenSlider4',
            //'imagenSlider5',
            //'estatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
