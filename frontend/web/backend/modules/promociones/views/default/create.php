<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\promociones\models\Promociones */

$this->title = 'Create Promociones';
$this->params['breadcrumbs'][] = ['label' => 'Promociones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promociones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
