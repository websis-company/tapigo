<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\promociones\models\Promociones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promociones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cliente_id')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'imagenPrincipal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imagenSlider1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imagenSlider2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imagenSlider3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imagenSlider4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imagenSlider5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estatus')->dropDownList([ 'Activo' => 'Activo', 'Inactivo' => 'Inactivo', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
