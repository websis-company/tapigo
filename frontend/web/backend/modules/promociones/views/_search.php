<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\promociones\models\PromocionesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promociones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'promocion_id') ?>

    <?= $form->field($model, 'cliente_id') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?= $form->field($model, 'imagenPrincipal') ?>

    <?php // echo $form->field($model, 'imagenSlider1') ?>

    <?php // echo $form->field($model, 'imagenSlider2') ?>

    <?php // echo $form->field($model, 'imagenSlider3') ?>

    <?php // echo $form->field($model, 'imagenSlider4') ?>

    <?php // echo $form->field($model, 'imagenSlider5') ?>

    <?php // echo $form->field($model, 'estatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
