<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\promociones\models\Promociones */

$this->title = 'Update Promociones: ' . $model->promocion_id;
$this->params['breadcrumbs'][] = ['label' => 'Promociones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->promocion_id, 'url' => ['view', 'id' => $model->promocion_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promociones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
